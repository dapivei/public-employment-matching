import psycopg2
import logging

from collections import defaultdict
from datetime import date

from src.utils.general import get_db_conn
from src.utils import constants

TODAY = date.today()


def get_vocabulary_functions():
    """
    Gets the vocabulary knowledge built with word2vec
    :return: Dictionary with vocabulary knowledge
    """
    q = """
        select 
            vocabulary,
            grupo
        from
            {}.{}
    """.format(constants.SCHEMA_GROUPS, constants.FUNCTIONS_VOCABULARY_TABLE)

    db_conn = get_db_conn("../../conf/local/credentials.yaml")
    cursor = db_conn.cursor()

    try:
        cursor.execute(q)
        vocabulary_table = cursor.fetchall()
        db_conn.commit()
    except (Exception, psycopg2.DatabaseError) as error:
        logging.error(error)

    # cambiar a diccionario
    return dict(vocabulary_table)


def get_vocabulary_knowledge():
    """

    :return:
    """
    q = """
            select 
                vocabulary,
                grupo
            from
                {}.{}
        """.format(constants.SCHEMA_GROUPS, constants.KNOWLEDGE_VOCABULARY_TABLE)

    db_conn = get_db_conn("../../conf/local/credentials.yaml")
    cursor = db_conn.cursor()

    try:
        cursor.execute(q)
        vocabulary_table = cursor.fetchall()
        db_conn.commit()
    except (Exception, psycopg2.DatabaseError) as error:
        logging.error(error)

    # cambiar a diccionario
    return dict(vocabulary_table)


def get_catalogue_functions_group_names():
    """
    Gets the catalogue for knowledge group names
    :return: Dictionary with the catalogue for knowledge group names
    """
    q = """
        select 
            grupo,
            group_name
        from
            {}.{}
    """.format(constants.SCHEMA_GROUPS, constants.STPS_CATALOGO_NOMBRES_GRUPOS_FUNCIONES)

    db_conn = get_db_conn("../../conf/local/credentials.yaml")
    cursor = db_conn.cursor()

    try:
        cursor.execute(q)
        functions_group_names = cursor.fetchall()
        db_conn.commit()
    except (Exception, psycopg2.DatabaseError) as error:
        logging.error(error)

    # cambiar a diccionario
    return dict(functions_group_names)


def sort_groups(groups):
    """
    Sort groups of job offers based on the number of words belonging to each group
    :param groups: List of groups and number of words on each group
    :return: sorted list of groups
    """
    counts = defaultdict(int)
    for group in groups:
        counts[group] += 1

    sorted_groups = sorted(counts.items(), reverse=True, key=lambda tup: tup[1])

    return [sort_group[0] for sort_group in sorted_groups]


def how_many_elements_per_group(num_groups):
    """
    Calculates how many offers do we have to retrieve for each group: 50% first group rest 50% among rest of groups
    :param num_groups: List of groups from where we have to retrieve job offers
    :return: List with number of offers to retrieve for each group
    """
    num_grupos_restantes = len(num_groups) - 1
    logging.info("num grupos totales: " + str(len(num_groups)))

    if len(num_groups) == 1:
        return [20]
    elif 10 % num_grupos_restantes == 0:
        return [10] + [int(10 / num_grupos_restantes)] * num_grupos_restantes
    else:
        reminder = 10 % num_grupos_restantes
        num_offers_per_group = [int(10 / num_grupos_restantes)] * num_grupos_restantes
        for i in range(reminder):
            num_offers_per_group[i] += 1
        return [10] + num_offers_per_group


def format_rows(id_candidato, group, top_20):
    """

    :param id_candidato:
    :param group:
    :param top_20:
    :return:
    """
    rows = [(id_candidato, job_offer[0], group, TODAY) for job_offer in top_20]
    # eliminar duplicados
    return list(set(rows))
