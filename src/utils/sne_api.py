# -*- coding: utf-8 -*-

import requests

def job_recommendations(id_candidato):
    """
    Api call to retrieve top 20 job recommendations per jobseeker.
    """
    URL_BASE = 'http://ec2-3-238-245-185.compute-1.amazonaws.com:5000/search/employment_fair/knowledge/{}'.format(id_candidato)
    response = requests.get(url = URL_BASE)

    return(response.json())


def candidate_recommendations(id_oferta):
    """
    Api call to retrieve most compatible job seekers for job vacancy.
    """
    URL_BASE = 'http://ec2-3-238-245-185.compute-1.amazonaws.com:5000/search/employment_fair/company/{}'.format(id_oferta)
    response = requests.get(url = URL_BASE)

    return(response.json())
