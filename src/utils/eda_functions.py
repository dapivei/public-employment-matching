"""
FUNCTIONS FOR EDA
"""
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt 
import seaborn as sns
import os
import matplotlib.cm as cm
import pickle

from sklearn.metrics import silhouette_samples, silhouette_score
from PIL import Image
from sklearn.cluster import KMeans
from yellowbrick.cluster import KElbowVisualizer, SilhouetteVisualizer
from gensim.models import Word2Vec, FastText, KeyedVectors
from collections import Counter
from wordcloud import WordCloud
from matplotlib.ticker import FuncFormatter
from yellowbrick.cluster import KElbowVisualizer, SilhouetteVisualizer

from src.utils.general import *

def generate_word_counter(data, n_most_common=50):
    """Generates unigrams from text data. Defaults to top 50 unigrams"""
    print('TOP '+ str(n_most_common) + ' UNIGRAMS IN DATA')

    return(Counter(" ".join(data).split()).most_common(n_most_common))

def generate_wordcloud(data, column, normalization=None, color='Blues_r', mask=None, n=50, save=True):
    """Generates WordCloud with bigrams, defaults to top 50 bigrams"""
    
    all_words = '' 
    #looping and joining text to extract most common words
    for arg in data[column]: 
        tokens = arg.split()  
        all_words += " ".join(tokens)+" "
    wordcloud = WordCloud(
        width = 700, 
        height = 700, 
        background_color='white', 
        min_font_size = 10, 
        colormap=color,
        mask=mask,
        stopwords=['ninguno'],
        collocations=True,
        max_words = n
    ).generate(all_words) 
    plt.figure(
        figsize = (10, 8), 
        facecolor = None
    ) 
    plt.imshow(wordcloud) 
    plt.axis("off") 
    plt.tight_layout(pad = 0) 
    print('TOP '+ str(n) + ' BIGRAMS IN DATA')

    if save:
        path = '../images/'
        if not os.path.exists(path):
            os.makedirs(path)
        plt.savefig(
            path+column+'_'+normalization + '_wordcloud.pdf',
            dpi=1200
        )
       
    else:
        pass
    plt.show()
    
def plot_n_word_frecuency(df, column, normalization=None, color="#81A0BA", save=True, n=10):
    """
    Plot frequency in length of entries 
    """
    df['word_count'] = df[column].str.split().map(len)
    sns.set(rc={'figure.figsize':(10,8)})
    sns.set_style("whitegrid", {'axes.grid' : False})
    sns.set_context("paper")   
    sns.despine()
    d = sns.countplot(
        x='word_count', 
        data=df, 
        color=color
    )
    d.yaxis.set_major_formatter(FuncFormatter(number_formatter))
    print('WORD COUNT FOR '+ column.upper())
    d.set(xlabel="Number of words", ylabel = "Count")
    d.set_xticklabels(d.get_xticklabels(), rotation=0)
    if save:
        path = '../images/'
        if not os.path.exists(path):
            os.makedirs(path)
        d.figure.savefig(
            path+ column + '_' + normalization + '_wordcount.pdf', 
            dpi=1200, 
            transparent=True
        )
    else:
        pass

def run_silhouette_analysis(vec_features, type_text, save=True, range_n_clusters=range(2,30)):
    """
    Runs silhouette_analysis to select
    the number of optimal clusters formed with
    KMeans clustering.
    :param vec_features: Features formed with vetorizer model.
    :param type_text: Type of text vectorized.
    :param save: To save dataframe with silhouette scores and plots.
    :param range_n_clusters: List of numbers.
    :return:
    """
    clusters = []
    avg_silhouette_score = []
    for n_clusters in range_n_clusters:
        print(f"Processing n_clusers = {n_clusters}")
        sns.set_style("whitegrid", {'axes.grid' : False})
        sns.set_context("paper")
        sns.despine()
        # Create a subplot with 1 row and 2 columns
        fig, (ax1, ax2) = plt.subplots(1, 2)
        fig.set_size_inches(18, 7)
        # The 1st subplot is the silhouette plot
        # The silhouette coefficient can range from -1
        ax1.set_xlim([-0.6, 1])
        # The (n_clusters+1)*10 is for inserting blank space between silhouette
        # plots of individual clusters, to demarcate them clearly.
        ax1.set_ylim([0, len(vec_features) + (n_clusters + 1) * 10])
        # Initialize the clusterer with n_clusters value
        clusterer = KMeans(
            n_clusters=n_clusters,
            max_iter=constants.KMEANS_MAX_ITER,
            random_state=constants.KMEANS_SEED
        )
        cluster_labels = clusterer.fit_predict(
            vec_features
        )
        # The silhouette_score gives the average value for all the samples.
        # This gives a perspective into the density and separation of the formed
        # clusters
        silhouette_avg = silhouette_score(
            vec_features,
            cluster_labels
        )
        clusters.append(n_clusters)
        avg_silhouette_score.append(silhouette_avg)

        # Compute the silhouette scores for each sample
        sample_silhouette_values = silhouette_samples(
            vec_features,
            cluster_labels
        )
        y_lower = 10
        for i in range(n_clusters):
            # Aggregate the silhouette scores for samples belonging to
            # cluster i, and sort them
            ith_cluster_silhouette_values = \
                sample_silhouette_values[cluster_labels == i]

            ith_cluster_silhouette_values.sort()

            size_cluster_i = ith_cluster_silhouette_values.shape[0]
            y_upper = y_lower + size_cluster_i

            color = cm.nipy_spectral(
                float(i) / n_clusters
            )
            ax1.fill_betweenx(
                np.arange(y_lower, y_upper),
                0,
                ith_cluster_silhouette_values,
                facecolor=color,
                edgecolor=color,
                alpha=0.7
            )
            # Label the silhouette plots with their cluster numbers at the middle
            ax1.text(-0.05, y_lower + 0.5 * size_cluster_i, str(i))
            # Compute the new y_lower for next plot
            y_lower = y_upper + 10  # 10 for the 0 samples

        ax1.set_title("The silhouette plot for the various clusters.")
        ax1.set_xlabel("The silhouette coefficient values")
        ax1.set_ylabel("Cluster label")

        # The vertical line for average silhouette score of all the values
        ax1.axvline(
            x=silhouette_avg,
            color="gray",
            linestyle="--"
        )

        ax1.set_yticks([])  # Clear the yaxis labels / ticks
        ax1.set_xticks([-0.6, -0.4, -0.2, 0, 0.2, 0.4, 0.6, 0.8, 1])

        # 2nd Plot showing the actual clusters formed
        colors = cm.nipy_spectral(
            cluster_labels.astype(float) / n_clusters
        )
        ax2.scatter(
            vec_features[:, 0],
            vec_features[:, 1],
            marker='.',
            s=30,
            lw=0,
            alpha=0.7,
            c=colors,
            edgecolor='k'
        )

        # Labeling the clusters
        centers = clusterer.cluster_centers_
        # Draw white circles at cluster centers
        ax2.scatter(
            centers[:, 0],
            centers[:, 1],
            marker='o',
            c="white",
            alpha=1,
            s=200,
            edgecolor='k'
        )

        for i, c in enumerate(centers):
            ax2.scatter(
                c[0],
                c[1],
                marker='$%d$' % i,
                alpha=1,
                s=50,
                edgecolor='k'
            )

        ax2.set_title("The visualization of the clustered data.")
        ax2.set_xlabel("Feature space for the 1st feature")
        ax2.set_ylabel("Feature space for the 2nd feature")
        title = f"Silhouette analysis for KMeans clustering on {type_text} with n_clusters = {n_clusters}"
        plt.suptitle(
            title.upper(),
            fontsize=14,
            fontweight='bold'
        )
        plt.show()
        name_file = 'silhouette_analysis_' + type_text + '_nclusters' + str(n_clusters)
        if save:
            save_plot(name_file, plt)
        plt.close()

    intermediate_dictionary = {
        'Number of Clusters':n_clusters,
        'Average Silhouette Score':avg_silhouette_score
    }
    silhouette_results = pd.DataFrame(intermediate_dictionary)
    if save:
        save_dataframes('silhouette_analysis_' + type_text, silhouette_results)
    return(silhouette_results)

def run_elbow_analysis(vec_features, type_text):
    model = KMeans()
    visualizer = KElbowVisualizer(
        model,
        k=(2,30),
	timing=False
    )
    visualizer.fit(
        vec_features
    )
    path = '../../images/'
    check_path(path)
    visualizer.show(
        outpath= path + 'elbow_analysis_' + type_text + ".pdf"
    )
