"""
Functions to visualize results
"""
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt 
import seaborn as sns
import os
import boto3
import logging
import psycopg2


from gensim.models import Word2Vec, KeyedVectors
from sklearn.manifold import TSNE
from matplotlib.ticker import FuncFormatter

from src.utils.general import *

def get_vocabulary_groupings(text, embedding, normalization):
    print('GROUPINGS IN '+ text.upper() + '---' + embedding.upper()  + '+' +  normalization.upper())
    q = """
        select 
            *
        from 
            groups.vocabulary_{}_{}_{}
    """.format(text, embedding, normalization)
    db_conn = get_db_conn("../conf/local/credentials.yaml")
    cursor = db_conn.cursor()
    try:
        cursor.execute(q)
        vocab_groups = cursor.fetchall()
        db_conn.commit()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
        logging.error(error)
    return pd.DataFrame(vocab_groups,columns=['vocabulary', 'group'])

def get_elements_per_group(df, save=False, name_file=None, color="#939597"):
    sns = set_default_sns()
    d = sns.countplot(
        x='group',
        data=df,
        color=color
    )
    d.set(xlabel="Group", ylabel = "Count")
    for p in d.patches:
        d.annotate('{:,}'.format(p.get_height()), (p.get_x()+0.1, p.get_height()+50))
    if save:
        save_images(d, name_file)
    else:
        pass
    
def print_vocabulary_per_group(df):
    for i in range(0, len(df.group.unique())):
        print("####"*28, 
              "\n", 
              "GROUP Nº:", i,
              '\n',
             "####"*28)
        print(list(df[df.group==i].vocabulary))
        
def display_closestwords_tsnescatterplot(model, word, save=False, name_file=None):
    sns = set_default_sns()
    arr = np.empty((0,100), dtype='f')
    word_labels = [word]
    close_words = model.similar_by_word(word)
    arr = np.append(arr, np.array([model[word]]), axis=0)
    for wrd_score in close_words:
        wrd_vector = model[wrd_score[0]]
        word_labels.append(wrd_score[0])
        arr = np.append(arr, np.array([wrd_vector]), axis=0)
    tsne = TSNE(n_components=2, random_state=0)
    np.set_printoptions(suppress=True)
    Y = tsne.fit_transform(arr)
    x_coords = Y[:, 0]
    y_coords = Y[:, 1]
    plt.scatter(x_coords, y_coords, marker="o", s=50, color="#373e4b")
    for label, x, y in zip(word_labels, x_coords, y_coords):
        plt.annotate(
            label, 
            xy=(x, y), 
            xytext=(0.3, 3), 
            color='#373e4b',
            textcoords='offset points'
        )
    plt.xlim(x_coords.min()+10, x_coords.max()+20)
    plt.ylim(y_coords.min()+10, y_coords.max()+10)
    plt.show() 
    if save:
        save_images(plt, name_file)
    else:
        pass

def columns_groups_dataframe(type_text):
  if type_text=='knowledge':
    columns= ['0', 'group', 'id_jobseeker', 'skill', 'registration_date']
  else:
    columns= ['0', 'group', 'id_job_offer', 'job_description', 'job_title',  'closure_date', 'opening_date' ]
  return(columns)

def retrieve_groups_dataframe(grupo, type_text, root_form, model_embedding, path='../'):
    if type_text=='functions':
        groups_table = constants.FUNCTIONS_OFFERS
        id_left = 'id_oferta_empleo'
        original_table = 'oferta_empleo'
        id_right ='id_oe'
    elif type_text=='title':
        groups_table = constants.TITLE_OFFERS    
        original_table = 'oferta_empleo'
        id_left = 'id_oferta_empleo'
        id_right ='id_oe'
    else: 
        groups_table = constants.KNOWLEDGE_CANDIDATE  
        original_table = 'bt_conocimiento_habilidad'
        id_left = 'id_candidato'
        id_right ='id_bt'


    q = """
      with groups_created as(
      select
        *
      from
        groups.{}_{}_{}
      where
          grupo = {}
      ),
      id_groups as(
      select
        *
      from 
        stps.{}
      )
      select 
        distinct on (groups_created.{})
        *
      from
        groups_created
      left join
        id_groups
      on cast(groups_created.{} as int)=id_groups.{}
      
      
    """.format(
        groups_table,
        model_embedding,
        root_form,       
        grupo,
        original_table,
        id_left,
        id_left,
        id_right
    )
    db_conn = get_db_conn(path + "../conf/local/credentials.yaml")
    cursor = db_conn.cursor()
    try:
        cursor.execute(q)
        df = cursor.fetchall()
        db_conn.commit()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
        logging.error(error)
    df = pd.DataFrame(df, columns=columns_groups_dataframe(type_text))
    df = df.drop('0', axis=1)
    df = df.reset_index(drop=True)
    return df
