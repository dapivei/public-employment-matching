"""
General functions used throughout the project
"""
import matplotlib.pyplot as plt
import yaml
import psycopg2
import boto3
import os
import seaborn as sns
import pandas as pd

from gensim.models import Word2Vec, KeyedVectors

from src.utils import constants

def get_s3_credentials(creds_file):
    """ Get credentials for accessing AWS S3 buckets from the credentials file"""
    creds = read_yaml_file(creds_file)

    s3_creds = creds['s3']

    return s3_creds


def read_yaml_file(yaml_file):
    """ Load yaml cofigurations """

    config = None
    try:
        with open(yaml_file, 'r') as f:
            config = yaml.safe_load(f)
    except:
        raise FileNotFoundError('Couldnt load the file')

    return config


def get_db_conn(creds_file):
    """ Get an authenticated psycopg db connection, given a credentials file"""
    creds = read_yaml_file(creds_file)['db']

    connection = psycopg2.connect(
        user=creds['user'],
        password=creds['pass'],
        host=creds['host'],
        port=creds['port'],
        database=creds['db']
    )

    return connection


def number_formatter(number, pos=None):
    """Convert a number into a human readable format."""
    magnitude = 0
    while abs(number) >= 1000:
        magnitude += 1
        number /= 1000.0
        
    return '%.1f%s' % (number, ['', 'K', 'M', 'B', 'T', 'Q'][magnitude])


def get_db_conn_sql_alchemy(creds_file):
    """Get credentials for db connection"""
    creds = read_yaml_file(creds_file)['db']

    connection = "postgresql://{}:{}@{}:{}/{}".format(creds['user'], creds['pass'], creds['host'], creds['port'],
                                                      creds['db'])

    return connection


def check_path(path):
    if not os.path.exists(path):
        os.makedirs(path)


def save_plot(name_file, plt):
    """
    Save plots to images folder
    :param name_file: Name of the file
    :return:
    """
    path = '../../images/'
    check_path(path)
    plt.savefig(
        path + name_file + '.pdf',
        dpi=1200,
        transparent=True
    )
    print(f"Plot {name_file} saved")


def save_dataframes(name_file, df):
    """
    Save df json
    :param name_file: Name of the file
    :return:
    """
    path = '../../results/'
    check_path(path)
    df.to_json(path + name_file + '.json')


def set_default_sns():
    sns.set(rc={'figure.figsize':(14,8)})
    sns.set_style("whitegrid", {'axes.grid' : False})
    sns.set_context("paper")
    sns.despine()
    return(sns)

def load_vec_model(type_text, embedding, lemmatization=True, path="../"):

    """
    Loads the word2vec model from S3
    :param type_text:
    :param embedding:
    :return:
    """

    s3_creds = get_s3_credentials(path+ "../conf/local/credentials.yaml")

    session = boto3.Session(
        aws_access_key_id=s3_creds['aws_access_key_id'],
        aws_secret_access_key=s3_creds['aws_secret_access_key']
    )
    s3 = session.resource('s3')
    if lemmatization:
        root_form = "lemmatization"
    else:
        root_form = "stemming"
    if type_text == "function":
        name = "funciones"
    elif type_text == "knowledge":
        name = "conocimientos"
    else:
        name = "titulos"


    # file_name = constants.WORD2VEC_FUNCTIONS_MODEL
    s3_path = "s3://" + constants.S3_BUCKET + "/" + "groups/" + type_text + "/" + root_form + "/" + \
              embedding + "_"+ name + "_" + root_form + ".model"

    # load the model
    model = KeyedVectors.load_word2vec_format(s3_path)

    return model, lemmatization, root_form


def save_images(d, name_file):
    path="../images/"
    if not os.path.exists(path):
        os.makedirs(path)
    try:
        d.figure.savefig(path+name_file + '.pdf', dpi=1200, transparent=True)
    except:
        d.savefig(path+name_file + '.pdf', dpi=1200, transparent=True)

