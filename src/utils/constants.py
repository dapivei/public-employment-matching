"""
CONSTANTS EMPLOYED THROUGHOUT THE MATCHING MOTOR PROJECT
"""

S3_BUCKET = "sne-match"
S3_PATH_TITLE_MODEL = "groups/title/"
S3_PATH_FUNCTION_MODEL = "groups/function/"
S3_PATH_KNOWLEDGE_MODEL = "groups/knowledge/"

KMEANS_MAX_ITER = 199
KMEANS_SEED = 1234

# groups table
TITLE_VOCABULARY_TABLE = 'vocabulary_title'
KNOWLEDGE_VOCABULARY_TABLE = 'vocabulary_knowledge'
FUNCTIONS_VOCABULARY_TABLE = "vocabulary_functions"
TITLE_OFFERS = "titulos_ofertas"
FUNCTIONS_OFFERS = "funciones_ofertas"
KNOWLEDGE_CANDIDATE = "conocimientos_candidatos"
