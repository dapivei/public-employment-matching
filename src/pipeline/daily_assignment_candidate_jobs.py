# -*- coding: utf-8 -*-

"""
RETRIEVES JOB OFFERINGS WHICH FIT BETTER TO
SKILLS OR KNOWLEDGES POSSED BY JOB SEEKERS
"""

import psycopg2
import logging
import time
import spacy

from psycopg2 import extras
from datetime import date
from concurrent.futures import ProcessPoolExecutor
from collections import defaultdict

from src.utils.general import get_db_conn
from src.utils import constants
from src.pipeline import knowledge_search


TODAY = date.today()
print(str(TODAY))
logging.basicConfig(filename='daily_assignment_candidate_jobs_' + str(TODAY) + '.log', level=logging.INFO)


def get_vocabulary_functions(root_form, model_embedding):
    """
    Gets the functions vocabulary, built with the vectorizer model of choice.
    :param root_form: Type of text normalization performed.
    :param model embedding: Type of vectorizer.
    :return: Dictionary with the vocabulary built from functions listed by the hiring companies.
    """
    ending = '_' + model_embedding + '_' + root_form
    q = """
        select 
            vocabulary,
            grupo
        from
            {}.{}
    """.format(
        constants.SCHEMA_GROUPS,
        constants.FUNCTIONS_VOCABULARY_TABLE + ending
        )
    db_conn = get_db_conn("../../conf/local/credentials.yaml")
    cursor = db_conn.cursor()

    try:
        cursor.execute(q)
        vocabulary_table = cursor.fetchall()
        db_conn.commit()
    except (Exception, psycopg2.DatabaseError) as error:
        logging.error(error)

    # cambiar a diccionario
    return dict(vocabulary_table)


def get_catalogue_functions_group_names(root_form, model_embedding):
    """
    Retrieves the name of different groups or clusters form from the vocabulary listed as functions.
    :param root_form: Type of text normalization performed.
    :param model embedding: Type of vectorizer.
    :return: Dictionary with the groups names.
    """
    ending = '_' + model_embedding + '_' + root_form
    q = """
        select 
            grupo,
            group_name
        from
            {}.{}
    """.format(
        constants.SCHEMA_GROUPS,
        constants.STPS_CATALOGO_NOMBRES_GRUPOS_FUNCIONES + ending
        )

    db_conn = get_db_conn("../../conf/local/credentials.yaml")
    cursor = db_conn.cursor()

    try:
        cursor.execute(q)
        functions_group_names = cursor.fetchall()
        db_conn.commit()
    except (Exception, psycopg2.DatabaseError) as error:
        logging.error(error)

    # cambiar a diccionario
    return dict(functions_group_names)


def get_conocimientos_candidatos():
    """
    Retrieves the skills specified by all job seekers,
    restricted to those that have been active users the employment portal
    in the last 3 months.
    :return: List of tuples with id_candidato and knowledge/ability.
    """
    db_conn = get_db_conn("../../conf/local/credentials.yaml")
    q = """
        with max_fecha as(
            select 
                a.id_bt,
                max(fecha_hora_registro_bt) as fecha_hora_registro_bt
            from 
                stps.bt_conocimiento_habilidad a
            join 
                stps.inicio_sesion_bt b
            on 
                a.id_bt = b.id_bt::int
            where 
                fecha_ultimo_acceso::date between '{}'::date - interval '3 months' and '{}' 
            and 
                a.id_bt != 0 --tiene todos los conocimientos! es de prueba del sne
            group by 
                a.id_bt
        )
        
        select 
            id_bt,
            string_agg(distinct conocimientoherramienta_bt, ' ') as conocimientoherramienta_bt
        from 
            stps.bt_conocimiento_habilidad
        join
            max_fecha 
        using(id_bt, fecha_hora_registro_bt)
        where 
            conocimientoherramienta_bt != ''
        group by    
            id_bt
    """.format(
        knowledge_search.TODAY,
        knowledge_search.TODAY
        )

    cursor = db_conn.cursor()

    try:
        cursor.execute(q)
        conocimientos_candidatos = cursor.fetchall()
        db_conn.commit()
    except (Exception, psycopg2.DatabaseError) as error:
        logging.error(error)

    return conocimientos_candidatos


def get_offers_to_assign_group(root_form, model_embedding):
    """
    Finds the vacancies that don't have a group assigned.
    :param root_form: Type of text normalization performed.
    :param model embedding: Type of vectorizer.
    :return: List of tuples with id_oe and functions of offers that need a group to be assigned to-
    """
    ending = "_" + model_embedding + "_" + root_form

    q = """
        with ofertas_con_grupo as (
            select 
                id_oferta_empleo,
                grupo
            from 
                {}.{}  
        ),
        
        ofertas_empleo as(
            select 
                id_oe,
                grupo,
                funciones_oe
            from 
                {}.{} a
            left join
                ofertas_con_grupo b
            on cast(a.id_oe as int) = cast(b.id_oferta_empleo as int)
            where funciones_oe != ''
        )    
        select 
            id_oe, 
            funciones_oe
        from 
            ofertas_empleo
        where 
            grupo is null
    """.format(
        constants.SCHEMA_GROUPS,
        constants.FUNCTIONS_OFFERS + ending,
        constants.SCHEMA_STPS,
        constants.STPS_OFERTA_EMPLEO_FUNCIONES
        )

    db_conn = get_db_conn("../../conf/local/credentials.yaml")
    cursor = db_conn.cursor()

    try:
        cursor.execute(q)
        ofertas_asociar_grupos = cursor.fetchall()
        db_conn.commit()
    except (Exception, psycopg2.DatabaseError) as error:
        logging.error(error)

    return ofertas_asociar_grupos


def save_ofertas_grupos(ofertas_grupos, root_form, model_embedding):
    """
    Persist offers with its respective group on BD
    :param ofertas_grupos: List of tuples with id of the offer an group where it belongs
    :param root_form: Type of text normalization performed.
    :param model embedding: Type of vectorizer.
    :return: Nothing.
    """
    q = "insert into {}.{} values({})".format(
        constants.SCHEMA_GROUPS,
        constants.FUNCTIONS_OFFERS + "_" + model_embedding + "_" + root_form,
        ", ".join(["%s"] * 3)
        )

    db_conn = get_db_conn("../../conf/local/credentials.yaml")
    cursor = db_conn.cursor()

    try:
        extras.execute_batch(cursor, q, ofertas_grupos)
        db_conn.commit()
    except (Exception, psycopg2.DatabaseError) as error:
        logging.error(error)


def assign_group_to_offer(model, vocabulary, root_form, model_embedding, nlp):
    """
    Assign a group to a new or recent offer that didn't have a group associated yet.
    :param model: Vectorizer trainned model.
    :param vocabulary: Dictionary of knowledge vocabulary.
    :param root_form: Type of text normalization performed.
    :param model embedding: Type of vectorizer.
    :param nlp: Spanish language model from spacy.
    :return: Nothing.
    """
    # obtener ofertas que no tienen grupo asociado
    ofertas_asociar_grupo = get_offers_to_assign_group(root_form, model_embedding)
    logging.info("Offers that don't have a group assigned: " + str(len(ofertas_asociar_grupo)))
    ofertas_grupos = []
    for offer in ofertas_asociar_grupo:
        id_oferta = offer[0]
        funciones_oferta = offer[1]
        cleaned_text = knowledge_search.clean_text(funciones_oferta, root_form, nlp)

        functions = " ".join(list(set(cleaned_text.split())))

        if cleaned_text != '':
            most_similar_word = knowledge_search.get_most_similar(functions, model)
            logging.info(most_similar_word)
            if most_similar_word is not None:
                # find unique groups and sort them
                groups = [vocabulary[word_tuple[0]] for word_tuple in most_similar_word]
                group_selected = sort_groups(groups)[0]
                logging.info("Group for new offer: " + str(id_oferta) + " " + str(group_selected))
                ofertas_grupos.append((
                    id_oferta,
                    group_selected,
                    knowledge_search.TODAY))
            else:
                logging.info("No hay palabras similares a: " + str(functions))

    logging.info("Total number of offers assigned: " + str(len(ofertas_grupos)))
    save_ofertas_grupos(ofertas_grupos, root_form, model_embedding)


def persist_offers(rows, root_form, model_embedding):
    """
    Persists offers - candidates - groups on DB.
    :param rows: Data to persist in DB.
    :param root_form: Type of text normalization performed.
    :param model embedding: Type of vectorizer.
    :return: Nothing.
    """
    logging.info("Registro en bd")
    db_conn = get_db_conn("../../conf/local/credentials.yaml")

    ending = "_" + model_embedding + "_" + root_form

    q = "insert into {}.{}(idbt, idoferta, grupo, concepto, fechacontrol) values({})".format(
        constants.SCHEMA_STPS,
        constants.STPS_RESULTADO_OFERTAS_PARABT,
        ", ".join(['%s'] * 5)
        )
    try:
        cursor = db_conn.cursor()
        try:
            extras.execute_batch(cursor, q, rows)
            db_conn.commit()
        except(Exception, psycopg2.IntegrityError) as error:
            logging.info("Llave duplicada: " + rows)
            logging.error(error)
            db_conn.rollback()
        else:
            db_conn.commit()
    except (Exception, psycopg2.DatabaseError) as error:
        logging.error(error)


def format_rows(id_candidato, group_number, group_name, top_n):
    """
    Transform top offers into a list of of unique offers.
    :param id_candidato: Unique identifier of candidate.
    :param group: Groupings of knowledges assigned.
    :param top_n: Number of top offers to return.
    :return: List of of unique offers.
    """
    rows = [(id_candidato, job_offer[0], group_number, group_name, knowledge_search.TODAY) for job_offer
            in top_n]
    # eliminar duplicados
    return list(set(rows))


def sort_groups(groups):
    """
    Sort groups of job offers based on the number of words belonging to each group.
    :param groups: List of groups and number of words on each group.
    :return: Sorted list of groups.
    """
    counts = defaultdict(int)
    for group in groups:
        counts[group] += 1

    sorted_groups = sorted(counts.items(), reverse=True, key=lambda tup: tup[1])

    return [sort_group[0] for sort_group in sorted_groups]


def how_many_elements_per_group(num_groups):
    """
    Calculates how many offers do we have to retrieve for each group: 50% first group rest 50% among rest of groups.
    :param num_groups: List of groups from where we have to retrieve job offers.
    :return: List with number of offers to retrieve for each group.
    """
    num_grupos_restantes = len(num_groups) - 1
    logging.info("Número de grupos totales: " + str(num_groups))

    if len(num_groups) == 1:
        return [20]
    elif 10 % num_grupos_restantes == 0:
        return [10] + [int(10 / num_grupos_restantes)] * num_grupos_restantes
    else:
        reminder = 10 % num_grupos_restantes
        num_offers_per_group = [int(10 / num_grupos_restantes)] * num_grupos_restantes
        for i in range(reminder):
            num_offers_per_group[i] += 1
        return [10] + num_offers_per_group


def group_process(cleaned_text, model, vocabulary, function_groups_names, root_form, model_embedding, n_offerings):

    """
    For a given set of unique skills specified in the job seekers profile, we
    retrieve the most compatible groupings of job offerings available (based on the functions determined in
    the different job offerings).
    :param cleaned_text: Candidate knowledge.
    :param model: Vectorizer trainned model.
    :param vocabulary: Dictionary of knowledge vocabulary.
    :param function_groups_names: Catalogue of group names for job functions descriptions.
    :param root_form: Type of text normalization performed.
    :param model embedding: Type of vectorizer.
    :return: Nothing.
    """
    id_candidato = cleaned_text[0]
    # unique words
    knowledge = " ".join(list(set(cleaned_text[1].split())))

    if cleaned_text != '':
        most_similar_word = knowledge_search.get_most_similar(knowledge, model)
        logging.info("Most similar words: " + str(most_similar_word))
        if most_similar_word is not None:
            # find unique groups and sort them
            groups = [vocabulary[word_tuple[0]] for word_tuple in most_similar_word]
            groups_selected = sort_groups(groups)
            logging.info("Groups for: " + str(id_candidato) + " " + str(groups_selected))

            # how many from each group
            how_many_per_group = how_many_elements_per_group(groups_selected)
            logging.info("How many per group: " + str(how_many_per_group))
            for i, group_number in enumerate(groups_selected):
                group_name = function_groups_names[group_number]
                offers = knowledge_search.get_offers(group_number, root_form, model_embedding, n_offerings)
                if len(offers) > 0:
                    top_n = sorted(offers, key=lambda x: x[1], reverse=True)[:how_many_per_group[i]]
                    rows = format_rows(id_candidato, group_number, group_name, top_n)
                    persist_offers(rows, root_form, model_embedding)
                else:
                    logging.info("The is no associated offers to the following word: " + knowledge)
        else:
            logging.info("There is no similar word to: " + knowledge)
    else:
        logging.info("Empty clean space: " + knowledge)


def populate_stps_offers_for_candidates(lemmatization=True, fasttext=True, n_offerings=20):
    """
    Main funciton to populate database with most compatible job offerings to the knowledge and skills possed by the job seeker.
    :param lemmatization: Type of normalization performed to text. Defaults to lemmatization, though can be changed to stemming.
    :param fasttext: Type of vectorization performed to text. Defaults to fasttext, though can be changed to word2vec.
    :param n_offerings: Number of job offerings to retrieve per job seeker. Defaults to 20.
    :return: Nothing.
    """
    start_time = time.time()
    # defining name of variable to save the output (depending on the type of text normalization specified)
    if lemmatization:
        root_form = "lemmatization"
        nlp = spacy.load('es_core_news_lg')
    else:
        root_form = "stemming"
        nlp = None
    if fasttext:
        model_embedding = "fasttext"
    else:
        model_embedding = "word2vec"

    # load model
    model = knowledge_search.load_vec_model(root_form, model_embedding)

    # obtener vocabulario de funciones
    vocabulary = get_vocabulary_functions(root_form, model_embedding)

    # obtener catalogo de nombres de grupos de funciones
    function_groups_names = get_catalogue_functions_group_names(root_form, model_embedding)

    # asociar ofertas a grupos
    assign_group_to_offer(model, vocabulary, root_form, model_embedding, nlp)

    # obtener los conocimientos de los candidatos
    conocimientos_candidatos = get_conocimientos_candidatos()
    logging.info("Total knowledges retrieved: " + str(len(conocimientos_candidatos)))
    # limpiar texto
    cleaned_text = []
    start_time = time.time()
    executor = ProcessPoolExecutor(max_workers=6)
    for element in conocimientos_candidatos:
        id_candidato = element[0]
        conocimientos = element[1]
        executor.submit(cleaned_text.append((id_candidato, knowledge_search.clean_text(conocimientos, root_form, nlp))))
    logging.info("Time cleaning text: " + str(time.time() - start_time))

    for element in cleaned_text:
        executor.submit(group_process(element, model, vocabulary, function_groups_names, root_form, model_embedding, n_offerings))
    logging.info("Total time assigning job offers with similar skills requirements: " + str(time.time() - start_time))


print("in main")
start_time = time.time()
populate_stps_offers_for_candidates()
logging.info("Total time of execution: " + str(time.time() - start_time))
