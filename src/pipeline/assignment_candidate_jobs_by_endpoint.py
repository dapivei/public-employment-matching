import time
import psycopg2
import logging

from datetime import date
from psycopg2 import extras

from src.utils.general import get_db_conn
from src.utils import constants
from src.pipeline import knowledge_search
from src.utils import search_process_functions

TODAY = date.today()
logging.basicConfig(filename='assignment_candidato_jobs_by_endpoint_' + str(TODAY) + '.log', level=logging.INFO)


def get_conocimientos_candidato(id_candidato):
    """
        Get the knowledge and abilities from all candidates in the DB

        :return: List of tuples with id_candidato and knowledge/ability
        """
    db_conn = get_db_conn("../../conf/local/credentials.yaml")

    q = """
            with max_fecha as(
                select 
                    id_bt,
                    max(fecha_hora_registro_bt) as fecha_hora_registro_bt
                from 
                    stps.bt_conocimiento_habilidad
                where 
                    id_bt = {}
                group by 
                    id_bt
            )

            select 
                id_bt,
                string_agg(conocimientoherramienta_bt, ' ') as conocimientoherramienta_bt
            from 
                stps.bt_conocimiento_habilidad
            join
                max_fecha 
            using(id_bt, fecha_hora_registro_bt)
            where 
                conocimientoherramienta_bt != ''
            group by id_bt
        """.format(id_candidato)

    cursor = db_conn.cursor()

    try:
        cursor.execute(q)
        conocimientos_candidato = cursor.fetchall()
        db_conn.commit()
        logging.info("got knowledges from db")
    except (Exception, psycopg2.DatabaseError) as error:
        logging.error(error)

    return conocimientos_candidato


def persist_offers(rows):
    """
    Persists offers - candidates - groups on DB on _copy table and on treemap
    :param rows: Data to persist in DB
    :return:
    """
    logging.info("registro en bd")
    db_conn = get_db_conn("../../conf/local/credentials.yaml")

    # en _copy
    q = "insert into {}.{}(idbt, idoferta, concepto, fechacontrol) values({})".format(constants.SCHEMA_STPS,
                                              constants.STPS_RESULTADO_OFERTAS_PARABT, ", ".join(['%s'] * 4))

    try:
        cursor = db_conn.cursor()
        logging.info('insertando en stps resultados _copy candidato')
        try:
            extras.execute_batch(cursor, q, rows)
            db_conn.commit()
        except (Exception, psycopg2.IntegrityError) as error:
            logging.info("llave duplicada: ", rows)
            logging.error(error)
            db_conn.rollback()
        else:
            db_conn.commit()
    except (Exception, psycopg2.DatabaseError) as error:
        logging.error(error)



    # en treemap
    q = """insert into {}.{}(idbt, idoferta, concepto, fechacontrol) 
          values({})""".format(constants.SCHEMA_STPS, constants.STPS_RESULTADO_OFERTAS_PARABT_TREEMAP,
                               ", ".join(['%s'] * 4))
    try:
        cursor = db_conn.cursor()
        try:
            extras.execute_batch(cursor, q, rows)
            db_conn.commit()
        except(Exception, psycopg2.IntegrityError) as error:
            logging.info("llave duplicada: ", rows)
            logging.error(error)
            db_conn.rollback()
        else:
            db_conn.commit()
    except (Exception, psycopg2.DatabaseError) as error:
        logging.error(error)


def group_process(cleaned_text, model, vocabulary, function_groups_names):
    """

    :param conocimiento_candidato: candidate knowledge
    :param model: word2vec trainned model
    :param vocabulary: Dictionary of knowledge vocabulary
    :param function_groups_names: Catalogue of group names for job functions descriptions
    :return:
    """
    id_candidato = cleaned_text[0]
    knowledge = " ".join(list(set(cleaned_text[1].split())))

    if cleaned_text != '':
        most_similar_word = knowledge_search.get_most_similar(knowledge, model)
        logging.info(most_similar_word)
        if most_similar_word is not None:
            # find unique groups and sort them
            groups = [vocabulary[word_tuple[0]] for word_tuple in most_similar_word]
            groups_selected = search_process_functions.sort_groups(groups)
            logging.info("groups for: " + str(id_candidato) + " " + str(groups_selected))
            # how many from each group
            how_many_per_group = search_process_functions.how_many_elements_per_group(groups_selected)
            logging.info("how many per group: " + str(how_many_per_group))
            for i, group_number in enumerate(groups_selected):
                group_name = function_groups_names[group_number]
                offers = knowledge_search.get_offers(group_number)
                if len(offers) > 0:
                    top_20 = sorted(offers, key=lambda x: x[1], reverse=True)[:how_many_per_group[i]]
                    rows = search_process_functions.format_rows(id_candidato, group_name, top_20)
                    persist_offers(rows)
                else:
                    logging.info("no hay ofertas asociadas " + knowledge)
        else:
            logging.info("no hay palabra similar: " + knowledge)
    else:
        logging.info("texto limpio vacio: " + knowledge)


def populate_stps_offers_for_candidate(id_candidato):
    start_time = time.time()
    model = knowledge_search.load_word2vec_model()
    # obtener vocabulario de conocimientos
    vocabulary = search_process_functions.get_vocabulary_functions()

    # obtener catalogo de nombres de grupos de funciones
    function_groups_names = search_process_functions.get_catalogue_functions_group_names()

    # obtener los conocimientos del candidato
    conocimientos_candidato = get_conocimientos_candidato(id_candidato)

    # limpiar texto
    if len(conocimientos_candidato) > 0:
        #executor = ProcessPoolExecutor(max_workers=6)
        for element in conocimientos_candidato:
            id_candidato = element[0]
            conocimientos = element[1]
            cleaned_text = (id_candidato, knowledge_search.clean_text(conocimientos))
            logging.info("cleaned text for candidate " + str(id_candidato) + " " + str(cleaned_text))
        logging.info("tiempo clean text " + str(time.time() - start_time))

        group_process(cleaned_text, model, vocabulary, function_groups_names)
        logging.info("total time: " + str(time.time() - start_time))

        return 'OK'
    else:
        return 'El candidato {} no tiene conocimientos registrados'.format(id_candidato)

'''
start_time = time.time()
populate_stps_offers_for_candidate(11913)
logging.info("Tiempo total: " + str(time.time() - start_time))
'''