#!/bin/bash
PATH=/home/azurepass/.pyenv/plugins/pyenv-virtualenv/shims:/home/azurepass/.pyenv/shims:/home/azurepass/.pyenv/plugins/pyenv-virtualenv/shims:/home/azurepass/.pyenv/shims:/home/azurepass/.pyenv/bin:/usr/local/bin:/usr/bin:/bin:/usr/local/games:/usr/games

eval "$(pyenv init -)"
eval "$(pyenv virtualenv-init -)"

pyenv activate sne

python --version

cd /home/azurepass/sne/src/pipeline
echo "$PWD"

export PYTHONPATH=/home/azurepass/sne

echo "#### starting daily assignment candidate jobs"
python /home/azurepass/sne/src/pipeline/daily_assignment_candidate_jobs.py > output_candidate.txt &&
echo "#### daily assignment candidate job finished"
echo "#### starting daily assignment company candidates"
python /home/azurepass/sne/src/pipeline/daily_assignment_company_candidates.py > output_company.txt &&
echo "#### daily assignment company candidates finished"
echo "#### starting daily assignment pruning of expired offers"
python /home/azurepass/sne/src/pipeline/daily_pruning_candidate_jobs.py > output_pruning.txt &&
echo "#### daily assignment pruning of expired offers finished"

### compress and send to aws
today=$(date "+%Y-%m-%d")

# daily assignments candidates
prefix="daily_assignment_candidate_jobs_"
suffix=".zip"
zip "$prefix$today$suffix" "$prefix$today.log"

aws s3 cp "$prefix$today$suffix" s3://tec-sne/logs/agrupaciones_funciones/ --profile tec

rm "$prefix$today$suffix"
rm "$prefix$today.log"

# daily assignments companies
prefix="daily_assignment_company_candidates_"
zip "$prefix$today$suffix" "$prefix$today.log"

aws s3 cp "$prefix$today$suffix" s3://tec-sne/logs/agrupaciones_conocimientos/ --profile tec

rm "$prefix$today$suffix"
rm "$prefix$today.log"

# daily pruning
prefix="daily_pruning_candidate_jobs_"
zip "$prefix$today$suffix" "$prefix$today.log"

aws s3 cp "$prefix$today$suffix" s3://tec-sne/logs/pruning/ --profile tec

rm "$prefix$today$suffix"
rm "$prefix$today.log"