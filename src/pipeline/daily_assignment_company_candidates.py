# -*- coding: utf-8 -*-

"""
RETRIEVES JOB CANDIDATES WHICH FIT BETTER TO
DIFFERENT JOB OFFERINGS POSTED BY HIRING ENTITIES
"""

import psycopg2
import logging
import time
import spacy

from decimal import Decimal
from psycopg2 import extras
from datetime import date
from concurrent.futures import ProcessPoolExecutor
from collections import defaultdict

from src.utils.general import get_db_conn
from src.utils import constants
from src.pipeline import functions_search

TODAY = date.today()
print(str(TODAY))
logging.basicConfig(filename='daily_assignment_company_candidates_' + str(TODAY) + '.log', level=logging.INFO)


def get_vocabulary_knowledge(root_form, model_embedding):
    """
    Gets the knowledge vocabulary, built with the vectorizer model of choice.
    :param root_form: Type of text normalization performed.
    :param model embedding: Type of vectorizer.
    :return: Dictionary with the vocabulary built from the skills listed by the job seekers.
    """
    ending = '_' + model_embedding + '_' + root_form
    q = """
            select 
                vocabulary,
                grupo
            from
                {}.{}
        """.format(
            constants.SCHEMA_GROUPS,
            constants.KNOWLEDGE_VOCABULARY_TABLE + ending
            )

    db_conn = get_db_conn("../../conf/local/credentials.yaml")
    cursor = db_conn.cursor()

    try:
        cursor.execute(q)
        vocabulary_table = cursor.fetchall()
        db_conn.commit()
    except (Exception, psycopg2.DatabaseError) as error:
        logging.error(error)

    # cambiar a diccionario
    return dict(vocabulary_table)


def get_catalogue_knowledge_group_names(root_form, model_embedding):
    """
    Retrieves the name of different groups or clusters form from the vocabulary listed as skills or knowledges.
    :param root_form: Type of text normalization performed.
    :param model embedding: Type of vectorizer.
    :return: Dictionary with the groups names.
    """
    ending = '_' + model_embedding + '_' + root_form
    q = """
            select 
                grupo,
                group_name
            from
                {}.{}
        """.format(
        constants.SCHEMA_GROUPS,
        constants.STPS_CATALOGO_NOMBRES_GRUPOS_CONOCIMIENTOS + ending
        )

    db_conn = get_db_conn("../../conf/local/credentials.yaml")
    cursor = db_conn.cursor()

    try:
        cursor.execute(q)
        knowledges_group_names = cursor.fetchall()
        db_conn.commit()
    except (Exception, psycopg2.DatabaseError) as error:
        logging.error(error)

    # cambiar a diccionario
    return dict(knowledges_group_names)


def get_funciones_ofertas():
    """
    Retrieves the functions specified by all job offers.
    :return: List of tuples with id_oe & functions+titles listed in job offers.
    """
    db_conn = get_db_conn("../../conf/local/credentials.yaml")
    q = """
        select 
            id_oe,
            a.funciones_oe,
            a.titulo_oe
        from
            {}.{} a
        join 
            {}.{} b
        using (id_oe)
        where 
            a.funciones_oe != ''
        and 
            fecha_vigencia >= '{}'
    """.format(
        constants.SCHEMA_STPS,
        constants.STPS_OFERTA_EMPLEO_FUNCIONES,
        constants.SCHEMA_STPS,
        constants.STPS_OFERTA_EMPLEO,
        functions_search.TODAY
        )

    cursor = db_conn.cursor()

    try:
        cursor.execute(q)
        funciones_oferta = cursor.fetchall()
        db_conn.commit()
    except (Exception, psycopg2.DatabaseError) as error:
        logging.error(error)

    return funciones_oferta


def get_candidates_to_assign_group(root_form, model_embedding):
    """
    Finds the job seekers that don't have a group assigned.
    :param root_form: Type of text normalization performed.
    :param model embedding: Type of vectorizer.
    :return: List of tuples with id_bt and conocimientoherramienta_bt of job seekers
    that need a group to be assigned to.
    """
    ending = "_" + model_embedding + "_" + root_form

    q = """
        with candidatos_con_grupo as (
            select 
                id_candidato,
                grupo
            from 
                {}.{}  
        ),
        
        candidatos as(
            select 
                id_bt,
                grupo,
                conocimientoherramienta_bt
            from 
                {}.{} a
            left join
                candidatos_con_grupo b
            on 
                cast(b.id_candidato as int) = cast(a.id_bt as int)
            where 
                conocimientoherramienta_bt != ''
        )    
        select 
            id_bt, 
            conocimientoherramienta_bt
        from 
            candidatos
        where 
            grupo is null
    """.format(
        constants.SCHEMA_GROUPS,
        constants.KNOWLEDGE_CANDIDATE + ending,
        constants.SCHEMA_STPS,
        constants.STPS_CANDIDATO_CONOCIMIENTO_HABILIDAD
        )

    db_conn = get_db_conn("../../conf/local/credentials.yaml")
    cursor = db_conn.cursor()

    try:
        cursor.execute(q)
        candidatos_asociar_grupos = cursor.fetchall()
        db_conn.commit()
    except (Exception, psycopg2.DatabaseError) as error:
        logging.error(error)

    return candidatos_asociar_grupos


def save_candidatos_grupos(candidatos_grupos, root_form, model_embedding):
    """
    Persist candidates with their respective group on BD
    :param candidatos_grupos: List of tuples with id of the job seeker and group where it belongs
    :param root_form: Type of text normalization performed.
    :param model embedding: Type of vectorizer.
    :return: Nothing.
    """
    q = "insert into {}.{} values({})".format(
        constants.SCHEMA_GROUPS,
        constants.KNOWLEDGE_CANDIDATE + "_" + model_embedding + "_" + root_form,
        ", ".join(["%s"] * 3)
        )

    db_conn = get_db_conn("../../conf/local/credentials.yaml")
    cursor = db_conn.cursor()

    try:
        extras.execute_batch(cursor, q, candidatos_grupos)
        db_conn.commit()
    except (Exception, psycopg2.DatabaseError) as error:
        logging.error(error)


def sort_groups(groups):
    """
    Sort groups of job offers based on the number of words belonging to each group.
    :param groups: List of groups and number of words on each group.
    :return: Sorted list of groups.
    """
    counts = defaultdict(int)
    for group in groups:
        counts[group] += 1

    sorted_groups = sorted(counts.items(), reverse=True, key=lambda tup: tup[1])

    return [sort_group[0] for sort_group in sorted_groups]


def assign_group_to_candidate(model, vocabulary, root_form, model_embedding, nlp):
    """
    Assign a group to a job seeker that recently registered and does not have a group associated yet
    :param model: Vectorizer trainned model.
    :param vocabulary: Dictionary of knowledge vocabulary.
    :param root_form: Type of text normalization performed.
    :param model embedding: Type of vectorizer.
    :param nlp: Spanish language model from spacy.
    :return: Nothing.
    """
    # obtener ofertas que no tienen grupo asociado
    candidatos_asociar_grupo = get_candidates_to_assign_group(root_form, model_embedding)
    logging.info("Job seekers that don't have a group assigned: " + str(len(candidatos_asociar_grupo)))
    candidatos_grupos = []
    for candidato in candidatos_asociar_grupo:
        id_candidato = candidato[0]
        conocimientos = candidato[1]
        cleaned_text = functions_search.clean_text(conocimientos, root_form, nlp)
        knowledge = " ".join(list(set(cleaned_text.split())))

        if cleaned_text != '':
            most_similar_word = functions_search.get_most_similar(knowledge, model)
            logging.info(most_similar_word)
            if most_similar_word is not None:
                # find unique groups and sort them
                groups = [vocabulary[word_tuple[0]] for word_tuple in most_similar_word]
                group_selected = sort_groups(groups)[0]
                #logging.info("Group for new job seeker: " + str(id_candidato) + " " + str(group_selected))
                candidatos_grupos.append((
                    id_candidato,
                    group_selected,
                    functions_search.TODAY))
            else:
                logging.info("No hay palabras similares a: " + str(knowledge))

    logging.info("Total number of job seekers assigned: " + str(len(candidatos_grupos)))
    save_candidatos_grupos(candidatos_grupos, root_form, model_embedding)


def persist_offers(rows, root_form, model_embedding):
    """
    Persists offers - candidates - groups on DB.
    :param rows: Data to persist in DB
    :param root_form: Type of text normalization performed.
    :param model embedding: Type of vectorizer.
    :return: Nothing.
    """
    logging.info("Registro en bd")
    db_conn = get_db_conn("../../conf/local/credentials.yaml")
    ending = "_" + model_embedding + "_" + root_form

    q = "insert into {}.{}(idbt, idoferta, grupo, concepto, fechacontrol) values({})".format(
        constants.SCHEMA_STPS,
        constants.STPS_RESULTADO_CANDIDATOS_PARAEMP,
        ", ".join(['%s'] * 5))
    try:
        cursor = db_conn.cursor()
        try:
            extras.execute_batch(cursor, q, rows)
        except (Exception, psycopg2.IntegrityError) as error:
            logging.info("Llave duplicada: " + str(rows))
            logging.error(error)
            db_conn.rollback()
        else:
            db_conn.commit()
    except (Exception, psycopg2.DatabaseError) as error:
        logging.error(error)


def format_rows(id_oferta, group, group_name, new_rows):
    """
    Formats the row to be inserted in BD.
    :param id_oferta: Id of a job offer.
    :param title: Job title.
    :param top_n: List of top n ideal candidates
    :return: List of unique candidates.
    """
    rows = [(element[0], id_oferta, group, group_name, functions_search.TODAY) for element in new_rows]

    # eliminar duplicados
    return list(set(rows))


def format_rows_comparison(id_oferta, top_n):
    """
    Format tuples to make a correct comparison between rows wanted to be inserted and rows that are already inserted
    :param id_oferta: Id of a job offer.
    :param top_n: List of top n ideal candidates
    :return: List of tuples with rows in the correct format to compare
    """
    rows = [(Decimal(candidato[0]), id_oferta, functions_search.TODAY) for candidato in top_n]

    # eliminar duplicados
    return list(set(rows))


def eliminate_duplicate_keys(rows, existing_results):
    """
    Eliminates rows that already exist on the results company table by checking differences between sets.
    :param rows: List of tuples of recommendations to be inserted in the result table.
    :param existing_results: List of tuples
    :return: List of new rows to insert into results table.
    """
    if len(existing_results) > 0:
        set_rows = set(rows)
        set_existing_results = set(existing_results)

        rows_to_insert = list(set_rows.difference(set_existing_results))
    else:
        rows_to_insert = rows

    return rows_to_insert


def group_process(cleaned_text, model, vocabulary, knowledge_groups_names, root_form, model_embedding,
                  n_candidates):
    """
    For a given set of unique specified functions in the job vacancy, we
    retrieve the most compatible groupings of job seekers available (based on the skills determined in
    the candidates profile).
    :param cleaned_text: Cleaned text of functions.
    :param model: Vectorizer trainned model.
    :param vocabulary: Dictionary of knowledge vocabulary.
    :param knowledge_groups_names: Catalogue of group names for skills listed by candidates.
    :param root_form: Type of text normalization performed.
    :param model embedding: Type of vectorizer.
    :param n_candidates: Number of job seekers to retrieve.
    :param existing_results: List of tuples with rows already registered in db.
    :return:  Nothing.
    """
    id_oferta = cleaned_text[0]
    functions = cleaned_text[1]
    title = cleaned_text[2]

    if functions != '':
        start_time = time.time()
        most_similar_word = functions_search.get_most_similar(functions, model)
        logging.info("TIME-> get most similar (group process): " + str(time.time() - start_time))
        if most_similar_word is not None:
            logging.info("Most similar word: " + str(most_similar_word))
            group = vocabulary[most_similar_word[0][0]]
            group_name = knowledge_groups_names[group]
            logging.info("group name: " + str(group_name))
            start_time = time.time()
            candidates = functions_search.get_candidates(group, root_form, model_embedding)
            logging.info("TIME-> get candidates (group process): " + str(time.time() - start_time))
            if len(candidates) > 0:
                top_n = sorted(candidates, key=lambda x: x[1], reverse=True)[:n_candidates]
                rows = format_rows(id_oferta, group, group_name, top_n)
                start_time = time.time()
                persist_offers(rows, root_form, model_embedding)
                logging.info("TIME-> persist offers (group process): " + str(time.time() - start_time))
            else:
                logging.info("The is no job seeker with skills associated to the following word: " + str(functions))
        else:
            logging.info("There is no similar word to: " + str(functions))
    else:
        logging.info("Empty clean space: " + str(functions))


def populate_stps_candidates_for_offers(lemmatization=True, fasttext=True, n_candidates=20):

    """
    Main funciton to populate database with most compatible job seekers to the functions listed by the job offerings.
    :param lemmatization: Type of normalization performed to text. Defaults to lemmatization, though can be changed to stemming.
    :param fasttext: Type of vectorization performed to text. Defaults to fasttext, though can be changed to word2vec.
    :param n_candidates: Number of job candidates to retrieve per job oferring. Defaults to 20.
    :return: Nothing.
    """
    start_time = time.time()
    # defining name of variable to save the output (depending on the type of text normalization specified)
    if lemmatization:
        root_form = "lemmatization"
        nlp = spacy.load('es_core_news_lg')
    else:
        root_form = "stemming"
        nlp = None
    if fasttext:
        model_embedding = "fasttext"
    else:
        model_embedding = "word2vec"
    # obtener vocabulario de knowledge
    vocabulary = get_vocabulary_knowledge(root_form, model_embedding)

    # obtener catalogo de conocimientos
    knowledge_groups_names = get_catalogue_knowledge_group_names(root_form, model_embedding)

    # obtener modelo
    model = functions_search.load_vec_model(root_form, model_embedding)

    # asociar candidatos nuevos con grupos
    assign_group_to_candidate(model, vocabulary, root_form, model_embedding, nlp)

    # obtener funciones de ofertas
    funciones_oferta = get_funciones_ofertas()
    logging.info("Total functions retrieved: " + str(len(funciones_oferta)))
    # limpieza de texto en funciones
    cleaned_text = []
    start_time = time.time()
    executor = ProcessPoolExecutor(max_workers=3)
    for element in funciones_oferta:
        id_oferta = element[0]
        funciones = element[1]
        titulo = element[2]
        executor.submit(cleaned_text.append((id_oferta, functions_search.clean_text(funciones, root_form, nlp), titulo)))
    logging.info("Time cleaning text: " + str(time.time() - start_time))

    for element in cleaned_text:
        executor.submit(group_process(element, model, vocabulary, knowledge_groups_names, root_form, model_embedding,
                                      n_candidates))

    logging.info("Total time assigning job candidates with similar offeres: " + str(time.time() - start_time))

start_time = time.time()
print("start")
populate_stps_candidates_for_offers()
logging.info("Total time of execution: " + str(time.time() - start_time))
