import psycopg2
import boto3
import logging

from gensim.models import Word2Vec, KeyedVectors
from datetime import date

from src.pipeline.text_preprocessing import preprocess_text
from src.utils import constants
from src.utils.general import get_s3_credentials, get_db_conn

#TODAY = date.today()
TODAY = '2019-06-12'

def load_word2vec_model():
    """
    Loads the word2vec model from S3
    :param type_model:
    :return:
    """
    s3_creds = get_s3_credentials("../../conf/local/credentials.yaml")

    session = boto3.Session(
        aws_access_key_id=s3_creds['aws_access_key_id'],
        aws_secret_access_key=s3_creds['aws_secret_access_key']
    )
    s3 = session.resource('s3')

    file_name = constants.WORD2VEC_TITLE_MODEL
    s3_path = "s3://" + constants.S3_BUCKET + "/" + constants.S3_PATH_TITLE_MODEL + file_name

    # load the model
    model = KeyedVectors.load_word2vec_format(s3_path)

    return model


def get_most_similar(cleaned_text, model):
    """
    Get the most similar word from the word2vec model
    :param cleaned_text: cleaned text
    :param model: which model to look for similarity
    :return:
    """
    similar = model.most_similar(positive=cleaned_text.split())
    # top 1
    logging.info("most similar: ", similar[0])
    # get the word
    most_similar_word = similar[0][0]

    return most_similar_word


def get_group(most_similar_word):
    """

    :param most_similar_word:
    :param title:
    :return:
    """
    q = """
        select 
            group_name as grupo
        from 
            {}.{}
        join
            {}.{}
        using(grupo)
        where 
            vocabulary = '{}'
    """.format(constants.SCHEMA_GROUPS, constants.TITLE_VOCABULARY_TABLE,
               constants.SCHEMA_GROUPS, constants.STPS_CATALOGO_NOMBRES_GRUPOS_TITULOS, most_similar_word)

    db_conn = get_db_conn("../../conf/local/credentials.yaml")
    cursor = db_conn.cursor()

    try:
        cursor.execute(q)
        grupo = cursor.fetchall()
        db_conn.commit()
    except (Exception, psycopg2.DatabaseError) as error:
        logging.error(error)

    return grupo[0][0]


def get_offers(group):
    """
    Get related offers
    :param group: group to look for
    :return:
    """
    q = """
        with ofertas_grupos as(
            select
                id_oferta_empleo 
            from 
                {}.{}
            where grupo = {}
        ),
        
        ofertas as(
            select 
                id_oferta_empleo
            from 
                clean.oferta_empleo
            where fecha_fin >= '{}'
        ) 
        
        select 
            id_oferta_empleo
        from 
            ofertas_grupos
        join 
            ofertas
        using (id_oferta_empleo)
    """.format(constants.SCHEMA_GROUPS, constants.TITLE_OFFERS, group, TODAY)

    db_conn = get_db_conn("../../conf/local/credentials.yaml")
    cursor = db_conn.cursor()

    try:
        cursor.execute(q)
        ofertas = cursor.fetchall()
        db_conn.commit()
    except (Exception, psycopg2.DatabaseError) as error:
        logging.error(error)

    offers = [ele for element in ofertas for ele in element]

    return offers


def clean_text(text_to_look):
    steps = ['basic_cleaning', 'remove_accents', 'remove_punctuation', 'remove_numbers',
             'remove_stopwords', 'stemming']

    cleaned_text = preprocess_text(text_to_look,steps)

    return cleaned_text


def sort_offers(offers):
    """
    Gets top 20 job offers Sorted by the publish date from recent to older
    :param offers: List of offers found by the model
    :return: List of 20 offers sorted by publish date
    """
    q = """
        select 
            id_oferta_empleo
        from 
            {}.{}
        where id_oferta_empleo in {}
        order by fecha_inicio desc
        limit 20
    """.format(constants.SCHEMA_CLEAN, constants.OFERTA_EMPLEO, tuple(offers))

    db_conn = get_db_conn("../../conf/local/credentials.yaml")
    cursor = db_conn.cursor()

    try:
        cursor.execute(q)
        top_20 = cursor.fetchall()
        db_conn.commit()
    except (Exception, psycopg2.DatabaseError) as error:
        logging.error(error)

    return top_20



def search_group_title(text_to_look, title=True):
    """

    :param text_to_look:
    :param title:
    :return:
    """

    cleaned_text = clean_text(text_to_look)
    model = load_word2vec_model()
    most_similar_word = get_most_similar(cleaned_text, model)
    group = get_group(most_similar_word)
    offers = get_offers(group)
    top_20 = sort_offers(offers)
    # convert list of tuples into list
    output = [element for elem in top_20 for element in elem]

    return output
