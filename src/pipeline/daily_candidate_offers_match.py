import psycopg2
import logging

from datetime import date

from src.utils.general import get_db_conn
from src.utils import constants


TODAY = date.today()
print(str(TODAY))
logging.basicConfig(filename='daily_candidate_jobs_match_' + str(TODAY) + '.log', level=logging.INFO)


def get_offers_from_group(group, root_form, model_embedding):
    """
    Get all offers in a specific functions group
    :param group: Interested group obtained from clustering
    :param root_form: How does the cleaning of the text was made, lemmatization or stemming
    :param model_embedding: Which embedding was used, word2vec or fasttext
    :return: List of tuples with id_oferta_empleo and group
    """
    q = """
        select 
           id_oferta_empleo,
           grupo
        from 
           {}.{}
        where grupo = {}
    """.format(
        constants.SCHEMA_GROUPS,
        constants.FUNCTIONS_OFFERS + root_form + model_embedding,
        grupo
    )

    db_conn = get_db_conn("../../conf/local/credentials.yaml")
    cursor = db_conn.cursor()

    try:
        cursor.execute(q)
        ofertas_grupo = cursor.fetchall()
        db_conn.commit()
    except (Exception, psycopg2.DatabaseError) as error:
        logging.error(error)

    return ofertas_grupo

