"""
FINDING OPTIMAL NUMBER OF GROUPINGS WITH SILHOUETTE AND ELBOW ANALYSIS
"""

from src.utils.general import *
from src.utils.eda_functions import *

type_text=["function", "knowledge", "title"]
type_embedding = ['fasttext', 'word2vec']
lemmatization = [True, False]


# function + fasttext + lemmatization==True
text = type_text[0]
embedding = type_embedding[0]
normalization = lemmatization[0]
if normalization==True:
    norm ='lemmatization'
else:
    norm ='stemming'

model = load_vec_model(
    text, 
    embedding,
    normalization
    )[0]

vec_features = model[model.wv.vocab]

run_elbow_analysis(
    vec_features,
    text +'_'+ embedding + '_' + norm
)

run_silhouette_analysis(
    vec_features, 
    text + '_'+ embedding + '_' + norm
)


