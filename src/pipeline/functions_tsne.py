"""
GENERATES TSNE FOR FUNCTIONS POSTED IN JOB OFFERS
"""
import boto3
import os
import pandas as pd
import matplotlib.pyplot as plt

from gensim.models import Word2Vec, KeyedVectors
from sklearn.manifold import TSNE

from src.utils import constants
from src.utils.general import get_s3_credentials, get_db_conn


def load_vec_model(type_text, embedding, lemmatization=True):
    """
    Loads the word2vec model from S3
    :param type_text:
    :param embedding:
    :return:
    """
    s3_creds = get_s3_credentials("../../conf/local/credentials.yaml")

    session = boto3.Session(
        aws_access_key_id=s3_creds['aws_access_key_id'],
        aws_secret_access_key=s3_creds['aws_secret_access_key']
    )
    s3 = session.resource('s3')
    if lemmatization:
        root_form = "lemmatization"
    else:
        root_form = "stemming"

    # file_name = constants.WORD2VEC_FUNCTIONS_MODEL
    s3_path = "s3://" + constants.S3_BUCKET + "/" + "groups/" + type_text + "/" + root_form + "/" + \
              embedding + "_funciones_" + root_form + ".model"

    # load the model
    model = KeyedVectors.load_word2vec_format(s3_path)

    return model, lemmatization, root_form


def get_vocab(grupo, type_text, embedding, root_form):
    conn = get_db_conn("../../conf/local/credentials.yaml")
    if type_text == "function":
        type_text = "functions"

    q = """
        select
            vocabulary

        from
            groups.{}

        where
            grupo={};
    
    """.format("vocabulary_" + type_text + "_" + embedding + "_" + root_form, grupo)

    vocab = pd.read_sql(q, conn)
    words = vocab['vocabulary'].tolist()

    return words


def tsne_plot(model, vectors):
    """
    Creates TSNE model and plots it
    :param model:
    :param vectors:
    :return
    """
    labels = []
    tokens = []

    for word in vectors:
        tokens.append(model[word])
        labels.append(word)

    tsne_model = TSNE(n_components=2, init='random', n_iter=1500, random_state=27)
    new_values = tsne_model.fit_transform(tokens)

    x = []
    y = []
    for value in new_values:
        x.append(value[0])
        y.append(value[1])

    plt.figure(figsize=(16, 16))
    for i in range(len(x)):
        plt.scatter(x[i], y[i])
        plt.annotate(labels[i],
                     xy=(x[i], y[i]),
                     xytext=(5, 2),
                     textcoords='offset points',
                     ha='right',
                     va='bottom')
    return (plt)


def run_tsne():
    """
    Runs TSNE for functions listed in job description and saves plots in "results" file
    """
    normalizations = [True, False]
    for normalization in normalizations:
        type_text = "function"
        embeddings = ["fasttext", "word2vec"]
        for embedding in embeddings:
            if embedding == 'fasttext' and normalization == True:
                clusters=constants.KMEANS_FUNCIONES_N_GROUPS_FASTTEXT_LEMMATIZATION
            elif embedding == 'fasttext' and normalization == False:
                clusters=constants.KMEANS_FUNCIONES_N_GROUPS_FASTTEXT_STEMMING
            elif embedding == 'word2vec' and  normalization == True:
                clusters=constants.KMEANS_FUNCIONES_N_GROUPS_WORD2VEC_LEMMATIZATION
            else:
                clusters=constants.KMEANS_FUNCIONES_N_GROUPS_WORD2VEC_STEMMING

            model, lemmatization, root_form = load_vec_model(
                type_text,
                embedding,
                lemmatization=normalization
            )
            word_vectors = model.vocab
            dirname = '../../results/' + root_form + "/"
            if not os.path.exists(dirname):
                os.makedirs(dirname)
            else:
                pass
            for i in range(0, clusters):
                vocab = get_vocab(
                    i,
                    type_text,
                    embedding,
                    root_form
                )
                title = type_text + "_" + embedding + "_" + root_form + \
                        "_group" + str(i)
                vectors = {k: word_vectors[k] for k in vocab}
                plt = tsne_plot(model, vectors)
                plt.title(title.upper().replace("_", " "))
                plt.savefig(dirname + title + ".png")
                print('saved', title)

if __name__ == '__main__':
    run_tsne()
