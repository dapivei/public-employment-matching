import pickle

from src.pipeline.precision_baselines import precision_recall_baseline_as_of_date
from src.utils.constants import BASELINE_FUISTE_CONTRATADO, BASELINE_LINEA_TIEMPO

from_dates_ana = ['2017-01-01', '2017-01-01', '2017-02-01', '2017-04-01', '2017-06-01', '2017-08-01', '2017-10-01',
                  '2017-12-01', '2018-02-01', '2018-04-01']
to_dates_ana = ['2017-05-31', '2017-07-31', '2017-09-30', '2017-11-30', '2018-01-31', '2018-03-31', '2018-05-31',
                '2018-07-31', '2018-09-30', '2018-11-30']

from_dates_caro = ['2017-01-01', '2017-02-01', '2017-03-01', '2017-04-01', '2017-05-01', '2017-06-01', '2017-07-01',
                   '2017-08-01', '2017-09-01', '2017-10-01', '2017-11-01', '2017-12-01', '2018-01-01', '2018-02-01',
                   '2018-03-01', '2018-04-01', '2018-05-01', '2018-06-01', '2018-07-01', '2018-08-01', '2018-09-01',
                   '2018-10-01']
to_dates_caro = ['2017-05-31', '2017-06-30', '2017-07-31', '2017-08-31', '2017-09-30', '2017-10-31', '2017-11-30',
                 '2017-12-31', '2018-01-31', '2018-02-28', '2018-03-31', '2018-04-30', '2018-06-30', '2018-07-31',
                 '2018-08-31', '2018-09-30', '2018-10-31', '2018-11-30', '2018-12-31', '2019-01-31', '2019-02-28',
                 '2019-03-31']

from_dates_dira = ['2017-01-01', '2017-01-01', '2017-01-01', '2017-01-01', '2017-01-01', '2017-01-01', '2017-01-01',
                   '2017-01-01']
to_dates_dira = ['2017-03-31', '2017-06-30', '2017-09-30', '2017-12-31', '2018-03-31', '2018-06-30', '2018-09-30',
                 '2018-12-31']

to_run_conf = {'ana': [from_dates_ana, to_dates_ana],
               'caro': [from_dates_caro, to_dates_caro],
               'dira': [from_dates_dira, to_dates_dira]}

metrics_per_timechop = {}
metrics = []

for element in to_run_conf.items():
    from_dates = element[1][0]
    to_dates = element[1][1]
    for j, from_date in enumerate(from_dates):
        to_date = to_dates[j]
        precision, recall = precision_recall_baseline_as_of_date(from_date, to_date, BASELINE_FUISTE_CONTRATADO)
        print(BASELINE_FUISTE_CONTRATADO, element[0], j, precision, recall)
        metrics.append((element[0], BASELINE_FUISTE_CONTRATADO, "timechop_" + str(j + 1), from_date, to_date,
                        precision, recall))
        metrics_per_timechop[element[0] + "_timechop_" + str(j + 1)] = {'baseline': BASELINE_FUISTE_CONTRATADO,
                                                                      'timechop_' + str(j +1): {'from_date': from_date,
                                                                                                'to_date': to_date,
                                                                                                'precision': precision,
                                                                                                'recall': recall}}

        precision, recall = precision_recall_baseline_as_of_date(from_date, to_date, BASELINE_LINEA_TIEMPO)
        metrics.append((element[0], BASELINE_LINEA_TIEMPO, "timechop_" + str(j + 1), from_date, to_date,
                        precision, recall))
        metrics_per_timechop[element[0] + "_timechop_" + str(j + 1)] = {'baseline': BASELINE_LINEA_TIEMPO,
                                            'timechop_' + str(j + 1): {'from_date': from_date,
                                                                       'to_date': to_date,
                                                                       'precision': precision,
                                                                       'recall': recall}}
        print(BASELINE_LINEA_TIEMPO, element[0], j, precision, recall)

with open("../../data/metrics_per_timechop.pkl", "wb") as f:
    pickle.dump(metrics, f)
