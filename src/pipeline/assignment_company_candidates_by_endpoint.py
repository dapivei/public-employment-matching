import psycopg2
import logging
import time

from psycopg2 import extras
from datetime import date

from src.utils.general import get_db_conn
from src.utils import constants
from src.pipeline import functions_search
from src.utils import search_process_functions

TODAY = date.today()
logging.basicConfig(filename='assignment_company_candidates_by_endpoint_' + str(TODAY) + '.log', level=logging.INFO)





def get_funciones_oferta(id_oferta):
    """
    Get the knowledge and abilities from all candidates in the DB

    :return: List of tuples with id_candidato and knowledge/ability
    """
    db_conn = get_db_conn("../../conf/local/credentials.yaml")

    q = """
        select 
            id_oe,
            a.funciones_oe,
            b.titulo_oe
        from 
            {}.{} a
        join 
            {}.{} b 
        using (id_oe)
        where 
            id_oe = {}
        and
            a.funciones_oe != ''
    """.format(constants.SCHEMA_STPS, constants.STPS_OFERTA_EMPLEO_FUNCIONES,
               constants.SCHEMA_STPS, constants.STPS_OFERTA_EMPLEO, id_oferta)

    cursor = db_conn.cursor()

    try:
        cursor.execute(q)
        funciones_oferta = cursor.fetchall()
        db_conn.commit()
    except (Exception, psycopg2.DatabaseError) as error:
        logging.error(error)

    return funciones_oferta


def persist_offers(rows):
    """
    Persists offers - candidates - groups on DB
    :param rows: Data to persist in DB
    :return:
    """
    logging.info("registro en bd")
    db_conn = get_db_conn("../../conf/local/credentials.yaml")

    # insertar en _copy
    q = """
        insert into {}.{}(idbt, idoferta, concepto, fechacontrol) values({})
    """.format(constants.SCHEMA_STPS, constants.STPS_RESULTADO_CANDIDATOS_PARAEMP, ", ".join(['%s'] * 4))

    try:
        cursor = db_conn.cursor()
        try:
            extras.execute_batch(cursor, q, rows)
        except (Exception, psycopg2.IntegrityError) as error:
            logging.info("llave duplicada: ", rows)
            logging.error(error)
            db_conn.rollback()
        else:
            db_conn.commit()
    except (Exception, psycopg2.DatabaseError) as error:
        logging.error(error)

    # insertar en treemap
    q = """
        insert into {}.{}(idbt, idoferta, concepto, fechacontrol) values({})
    """.format(constants.SCHEMA_STPS, constants.STPS_RESULTADO_CANDIDATOS_PARAEMP_TREEMAP, ", ".join(['%s'] * 4))

    try:
        extras.execute_batch(cursor, q, rows)
        db_conn.commit()
    except (Exception, psycopg2.DatabaseError) as error:
        logging.error(error)


def format_rows(id_oferta, title, top_20):
    """
    Formats the row to be inserted in BD
    :param id_oferta: Id of a job offer
    :param title: Job title
    :param top_20: List of top 20 ideal candidates
    :return:
    """
    rows = [(candidato[0], id_oferta, title, TODAY) for candidato in top_20]

    # eliminar duplicados
    return list(set(rows))


def group_process(cleaned_text, model, vocabulary):
    """

    :param funciones_oferta:
    :param model:
    :param vocabulary:
    :return: Tuple of id_candidato and group
    """
    id_oferta = cleaned_text[0]
    functions = cleaned_text[1]
    title = cleaned_text[2]

    if functions != '':
        most_similar_word = functions_search.get_most_similar(functions, model)
        if most_similar_word is not None:
            logging.info("most similar word: " + most_similar_word)
            # group = functions_search.get_group(most_similar_word)
            group = vocabulary[most_similar_word]
            candidates = functions_search.get_candidates(group)
            if len(candidates) > 0:
                top_20 = sorted(candidates, key=lambda x: x[1], reverse=True)[:20]
                rows = format_rows(id_oferta, title, top_20)
                persist_offers(rows)


def populate_stps_candidates_for_offer(id_oferta):
    start_time = time.time()
    # obtener modelo
    model = functions_search.load_word2vec_model()

    # obtener vocabulario de knowledge
    vocabulary = search_process_functions.get_vocabulary_knowledge()

    # obtener funciones de ofertas
    funciones_oferta = get_funciones_oferta(id_oferta)

    if len(funciones_oferta) > 0:
        # limpieza de texto en funciones
        for element in funciones_oferta:
            id_oferta = element[0]
            funciones = element[1]
            titulo = element[2]
            cleaned_text = (id_oferta, functions_search.clean_text(funciones), titulo)
        logging.info("tiempo clean text " + str(time.time() - start_time))

        group_process(cleaned_text, model, vocabulary)

        logging.info("tiempo total: " + str(time.time() - start_time))

        return 'OK'
    else:
        return 'La oferta {} no tiene funciones descritas'.format(id_oferta)

'''
populate_stps_candidates_for_offer(452650)
'''