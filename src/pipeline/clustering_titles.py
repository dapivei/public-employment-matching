"""
MODELS TO GENERATE VOCABULARY AND GROUPS OF
EXISTING JOB TITLES IN DIFFERENT JOB OPENINGS
"""
import pandas as pd
import numpy as np
import psycopg2
import logging
import boto3
import pickle

from sklearn.cluster import KMeans
from gensim.models import Word2Vec, FastText
from psycopg2 import extras
from datetime import date

from src.utils.general import get_db_conn, get_s3_credentials
from src.utils import constants


def get_clean_text_titulo(root_form):
    """
    Retrieve existing job titles specified in
    job openings.
    :param root_form: Type of normalization performed on titles of offerings.
    :return: Clean titles specified in job offerings.
    """
    # get clean text for titulo oferta
    q = """

        select 
            id_oe,
            titulo_oe 
        from 
            {}.{}
        where 
            titulo_oe != ''
    """.format(constants.SCHEMA_STPS, constants.OFERTA_EMPLEO + "_text_" + root_form)

    db_conn = get_db_conn("../../conf/local/credentials.yaml")
    cursor = db_conn.cursor()

    try:
        cursor.execute(q)
        titulos_ofertas = cursor.fetchall()
        db_conn.commit()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
        logging.error(error)

    return titulos_ofertas


def format_titulos_for_vec(titulos_ofertas):
    """
    Changes the offer titles format into a list of elements, one element per word.
    :param titulos_ofertas: List of tuples of offer titles
    :return: List of elements.
    """
    return [element[1].split() for element in titulos_ofertas]


def save_model(model, model_embedding, root_form):
    """
    Save model into S3
    :param model: Model to be persisted in S3
    :param root_form: Type of normalization.
    :param model_embedding: Type of embedding.
    :return: Nothing.
    """
    s3_creds = get_s3_credentials("../../conf/local/credentials.yaml")

    session = boto3.Session(
        aws_access_key_id=s3_creds['aws_access_key_id'],
        aws_secret_access_key=s3_creds['aws_secret_access_key']
    )
    s3 = session.client('s3')
    if model_embedding == 'fasttext':
        embedding = constants.FASTTEXT_TITLE_MODEL
    else:
        embedding = constants.WORD2VEC_TITLE_MODEL

    file_name = embedding + "_" + root_form + '.model'
    s3_path = "s3://" + constants.S3_BUCKET + "/" + constants.S3_PATH_TITLE_MODEL + root_form + "/" + file_name
    # store in s3 bucket
    model.wv.save_word2vec_format(s3_path)


def run_model(texto_titulos, model_embedding, root_form):
    """
    Runs the vectorizer algorithm.
    :param texto_titulos: List of offer titles, one cleaned word as an element in the list.
    :param root_form: Type of normalization.
    :param model_embedding: Type of embedding.
    :return: Vocabulary and features extracted by vectorizer model.
    """
    np.random.seed(constants.NUMPY_SEED)

    if model_embedding == "fasttext":
        model = FastText(
            sentences=texto_titulos, size=constants.VEC_SIZE, window=constants.VEC_WINDOW,
            min_count=constants.VEC_MIN_COUNT, workers=constants.VEC_WORKERS
        )
    else:
        model = Word2Vec(
            sentences=texto_titulos, size=constants.VEC_SIZE, window=constants.VEC_WINDOW,
            min_count=constants.VEC_MIN_COUNT, workers=constants.VEC_WORKERS
        )

    vec_keys = list(model.wv.vocab.keys())

    # extract features from word2vec
    vec_features = model[model.wv.vocab]

    # save the model in S3
    save_model(model, model_embedding, root_form)

    return vec_keys, vec_features


def save_kmeans(kmeans_vec, model_embedding, root_form):
    """
    Stores kmeans generated from vectorizer model.
    :param kmeans_vec:
    :param root_form: Type of normalization.
    :param model_embedding: Type of embedding.
    :return: Nothing.
    """
    s3_creds = get_s3_credentials("../../conf/local/credentials.yaml")

    session = boto3.Session(
        aws_access_key_id=s3_creds['aws_access_key_id'],
        aws_secret_access_key=s3_creds['aws_secret_access_key']
    )
    s3 = session.client('s3')

    if model_embedding == "fasttext":
        name_model = constants.FASTTEXT_TITLE_MODEL
    else:
        name_model = constants.WORD2VEC_TITLE_MODEL
    
    file_name = "kmeans_" + name_model + "_" + root_form + ".pkl"
    s3_path = constants.S3_PATH_TITLE_MODEL + root_form + "/" + file_name
    # store in s3 bucket
    kmeans_pickled = pickle.dumps(kmeans_vec)
    try:
        s3.put_object(Bucket=constants.S3_BUCKET, Key=s3_path, Body=kmeans_pickled)
    except(Exception) as error:
        print(error)


def generate_groups_kmeans(vec_features, model_embedding, root_form, clusters_n):
    """
    Generate the groups for the offer titles with KMeans and saves model.
    :param vec_features: Features extracted by vectorizer model.
    :param root_form: Type of normalization.
    :param model_embedding: Type of embedding.
    :param clusters_n: Number of clusters formed.
    :return: Nothing.
    """
    kmeans_titulo = KMeans(
        n_clusters=clusters_n, 
        max_iter=constants.KMEANS_MAX_ITER,
        random_state=constants.KMEANS_SEED
        )
    kmeans_ = kmeans_titulo.fit(vec_features)

    save_kmeans(kmeans_, model_embedding, root_form)

    return kmeans_


def get_groups(titulo_oferta_list, labels_dict):
    """
    Each offer title can be part of different groups, we only stores the top 3 groups that each title belongs to.
    :param titulo_oferta_list: List of offer titles.
    :param labels_dict: Dictionary with the vocabulary extracted by Word2Vec and the group where ir belongs.
    :return: Groups for titles defined in job offerings.
    """
    aux = [labels_dict[element] for element in titulo_oferta_list]
    if len(aux) > 1:
        groups_dict = {element: aux.count(element) for element in aux}
    else:
        groups_dict = {aux[0]: 1}
    groups_sorted = sorted(groups_dict.items(), key=lambda x: x[1], reverse=True)
    groups = [element[0] for element in groups_sorted]

    try:
        grupo = groups[0]
    except (Exception) as error:
        logging.error(error)

    return grupo


def save_vocabulary_in_db(vocabulary, model_embedding, root_form):
    """
    Save vocabulary in DB
    :param vocabulary: Data frame of vocabulary to persist
    :param root_form: Type of normalization.
    :param model_embedding: Type of word embedding.
    :return: Nothing, except when an error occurs.
    """
    q = "insert into {}.{} values({})".format(
        constants.SCHEMA_GROUPS,
        constants.TITLE_VOCABULARY_TABLE+ "_" + model_embedding + "_" + root_form, 
        ", ".join(['%s'] * 3))

    db_conn = get_db_conn("../../conf/local/credentials.yaml")
    cursor = db_conn.cursor()

    labels = [list(row) for row in vocabulary.itertuples(index=False)]

    try:
        extras.execute_batch(cursor, q, labels)
        db_conn.commit()
    except (Exception, psycopg2.DatabaseError) as error:
        logging.error(error)


def assign_offer_to_group(kmeans_vec, vec_keys, texto_titulos, titulos_ofertas, model_embedding, root_form):
    """
    Assigns each offer title to a group generated with KMeans.
    :param kmeans_vec: Groups generated by KMeans.
    :param vec_keys: Vocabulary from vectorizer model.
    :param texto_titulos: Titles of offers.
    :param titulos_ofertas: Cleaned offer titles.
    :param model_embedding: Type of embedding.
    :param root_form: Type of normalization.
    :return: Assigment of groups for each title of job offer.
    """
    labels = pd.DataFrame(
        {'vocabulary': vec_keys, 
        'grupo': kmeans_vec.labels_,
        'date_assigned': np.repeat(date.today(),len(vec_keys))}
        )    
    # save vocabulary in DB
    save_vocabulary_in_db(labels, model_embedding, root_form)
    labels_dict = dict(zip(labels.vocabulary, labels.grupo))
    text_groups = [get_groups(ele, labels_dict) for ele in texto_titulos]
    df_grupos = pd.DataFrame(text_groups, columns=['grupo'])
    titulos = pd.DataFrame(titulos_ofertas, columns=["id_oferta_empleo", "titulo"])
    ofertas_grupos = titulos.join(df_grupos)

    # convert ot int
    ofertas_grupos['grupo'] = ofertas_grupos.grupo.astype(int)
    ofertas_grupos['date_assigned'] = pd.to_datetime('today').date()

    return ofertas_grupos


def save_ofertas_grupo(ofertas_con_grupo, model_embedding, root_form):
    """
    Persists each existing offer in DB with their corresponding group.
    :param ofertas_con_grupo: Data frame with titles of the offers and its corresponding group.
    :param model_embedding: Type of embedding.
    :param root_form: Type of normalization.  
    :return: Nothing.
    """
    # convert title offers into list of tuples
    ofertas_ = ofertas_con_grupo.drop(['titulo'], axis=1)
    ofertas = [list(row) for row in ofertas_.itertuples(index=False)]

    # get clean text for titulo oferta
    q = "insert into {}.{} values({});".format(
        constants.SCHEMA_GROUPS, 
        constants.TITLE_OFFERS + "_" + model_embedding + "_" + root_form,
        ", ".join(["%s"] * 3))

    db_conn = get_db_conn("../../conf/local/credentials.yaml")
    cursor = db_conn.cursor()
    try:
        extras.execute_batch(cursor, q, ofertas)
        db_conn.commit()
    except (Exception, psycopg2.DatabaseError) as error:
        logging.error(error)


def groups_from_embedding(lemmatization=True, fasttext=True):
    """
    Pipeline for generating and persisting the groups for offer titles.
    :param lemmatization: Type of text normalization. Defaults to lemmatization, but could be changed to stemming
    :param fasttext: Type fo embedding. Defaults for fasttext, but can be changed to word2vec
    :return: Nothing.
    """
    # defining name of variable to save the output (depending on the type of text normalization specified)
    if lemmatization and fasttext:
        root_form = "lemmatization"
        model_embedding = "fasttext"
        clusters_n = constants.KMEANS_TITULOS_N_GROUPS_FASTTEXT_LEMMATIZATION
    elif lemmatization == False and fasttext == False:
        model_embedding = "word2vec"
        root_form = "stemming"
        clusters_n = constants.KMEANS_TITULOS_N_GROUPS_WORD2VEC_STEMMING
    elif lemmatization == False and fasttext == True:
        root_form = "stemming"
        model_embedding = "fasttext"
        clusters_n = constants.KMEANS_TITULOS_N_GROUPS_FASTTEXT_STEMMING
    else:
        model_embedding = "word2vec"
        root_form = "lemmatization"
        clusters_n = constants.KMEANS_TITULOS_N_GROUPS_WORD2VEC_LEMMATIZATION
    
    # job titles from offers   
    titulos_ofertas = get_clean_text_titulo(root_form)
    lista_titulos = format_titulos_for_vec(titulos_ofertas)
    vec_keys, vec_features = run_model(lista_titulos, model_embedding, root_form)
    kmeans_vec = generate_groups_kmeans(vec_features, model_embedding,  root_form, clusters_n)
    ofertas_con_grupo = assign_offer_to_group(kmeans_vec, vec_keys, lista_titulos, titulos_ofertas, model_embedding, root_form)
    save_ofertas_grupo(ofertas_con_grupo, model_embedding, root_form)
    print('Done!')

if __name__ == '__main__':
    groups_from_embedding()
