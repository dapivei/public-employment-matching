"""
CLEANING TEXT IN TABLE OF INTEREST.
----------------------------------
 :param schema: Schema of origin
 :param table: Table of origin
 :param text_cols: List of text columns
 :param id_column_table: Column identifier
 :param schema_dest: Schema where output will be saved
 :param table_dest: Table where output will be saved
 :param lemmatization: Type of text normalization. Defaults to lemmatization=True,
 though could be changed to lemmatization=False to perform stemming
 """

from src.pipeline.text_preprocessing import clean_table_text
from src.utils import constants


# Cleaning 'oferta_empleo'

clean_table_text(
    schema = constants.SCHEMA_STPS,
    table = constants.STPS_OFERTA_EMPLEO,
    text_cols = constants.TEXTO_OFERTA_EMPLEOS,
    id_column_table = constants.TEXTO_OFERTA_EMPLEOS_ID_COL,
    schema_dest = constants.SCHEMA_STPS,
    table_dest = constants.OFERTA_EMPLEO
)

# Cleaning 'candidato_conocimiento_habilidad'

clean_table_text(
    schema = constants.SCHEMA_STPS,
    table = constants.STPS_CANDIDATO_CONOCIMIENTO_HABILIDAD,
    text_cols = constants.TEXTO_CANDIDATOS_CONOCIMIENTO_HABILIDAD,
    id_column_table = constants.TEXTO_CANDIDATOS_CONOCIMIENTO_HABILIDAD_ID_COL,
    schema_dest = constants.SCHEMA_CLEAN,
    table_dest = constants.CANDIDATO_CONOCIMIENTO_HABILIDAD
)
