"""
FOR EACH JOB SEEKER, RETRIEVES THE TOP N JOB OFFERINGS
THAT FIT BETTER TO THE SKILLS OR KNOWLEDGES SHE OR HE POSSESSES
"""
import psycopg2
import boto3
import logging
import spacy

from gensim.models import KeyedVectors
from datetime import date

from src.pipeline.text_preprocessing import preprocess_text
from src.utils import constants
from src.utils.general import get_s3_credentials, get_db_conn


TODAY = date.today()


#logging.basicConfig(filename='daily_knowledge_search_' + str(TODAY) + '.log', level=logging.INFO)


def load_vec_model(root_form, model_embedding):
    """
    Loads the vectorizer model from S3.
    :param root_form: Type of normalization performed to text.
    :param model_embedding: Type of vectorizer.
    :return: Loaded model.
    """
    s3_creds = get_s3_credentials("../../conf/local/credentials.yaml")

    session = boto3.Session(
        aws_access_key_id=s3_creds['aws_access_key_id'],
        aws_secret_access_key=s3_creds['aws_secret_access_key'],
        region_name=constants.S3_REGION
    )
    s3 = session.resource('s3')

    if model_embedding == 'fasttext':
        embedding = constants.FASTTEXT_FUNCTIONS_MODEL
    else:
        embedding = constants.WORD2VEC_FUNCTIONS_MODEL

    file_name = embedding + "_" + root_form + '.model'
    s3_path = "s3://" + constants.S3_BUCKET + "/" + constants.S3_PATH_FUNCTION_MODEL + root_form + "/" + file_name
    # load the model
    model = KeyedVectors.load_word2vec_format(s3_path)

    return model


def get_most_similar(cleaned_text, model):
    """
    Get the most similar word from the vectorizer model.
    :param cleaned_text: Cleaned text.
    :param model: Which model to look for similarity.
    :return: Most similar word.
    """
    similar = None
    # check for similarity only with keywords that are in the vocabulary
    word_vectors = model.wv
    valid_vocabulary = [element for element in cleaned_text.split() if element in word_vectors.vocab]
    if len(valid_vocabulary) > 0:
        similar = model.most_similar(positive=valid_vocabulary)
        # top 1
        logging.info("most similar: " + str(similar[0]))

    return similar


def get_group(most_similar_word, root_form, model_embedding):
    """
    Retrieves group name that contains most similar word.
    :param most_similar_word: Most similar word.
    :param root_form: Type of normalization performed to text.
    :param model_embedding: Type of vectorizer employed.
    :return: Group containing the most similar word.
    """
    ending = '_' +  model_embedding + '_' + root_form
    q = """
        select 
            grupo,
            group_name
        from 
            {}.{}
        join
            {}.{}
        using(grupo)
        where 
            vocabulary = '{}'
    """.format(
        constants.SCHEMA_GROUPS,
        constants.FUNCTIONS_VOCABULARY_TABLE + ending,
        constants.SCHEMA_GROUPS,
        constants.STPS_CATALOGO_NOMBRES_GRUPOS_FUNCIONES + ending,
        most_similar_word
        )

    db_conn = get_db_conn("../../conf/local/credentials.yaml")
    cursor = db_conn.cursor()

    try:
        cursor.execute(q)
        grupo = cursor.fetchall()
        db_conn.commit()
    except (Exception, psycopg2.DatabaseError) as error:
        logging.error(error)

    return grupo[0]


def get_offers(group, root_form, model_embedding, n_offerings):
    """
    Get related offers.
    :param group: Group to look for.
    :param root_form: Type of normalization performed to text.
    :param model_embedding: Type of vectorizer employed.
    :return: List of offers belonging to a specific group.
    """
    ending = '_' +  model_embedding + '_' + root_form

    q = """
        with ofertas_grupos as(
            select
                distinct id_oferta_empleo::INTEGER
            from 
                {}.{}
            where grupo = {}
        ),
        ofertas as(
            select 
                distinct id_oe::INTEGER as id_oferta_empleo,
                fecha_ini_vigencia
            from 
                {}.{}
            where fecha_vigencia >= '{}'
        ) 
        select 
            id_oferta_empleo,
            fecha_ini_vigencia
        from 
            ofertas_grupos
        join 
            ofertas
        using (id_oferta_empleo)
        order by 
            fecha_ini_vigencia desc
        limit {}

    """.format(
        constants.SCHEMA_GROUPS,
        constants.FUNCTIONS_OFFERS + ending,
        group,
        constants.SCHEMA_STPS,
        constants.STPS_OFERTA_EMPLEO,
        TODAY,
        n_offerings
        )

    db_conn = get_db_conn("../../conf/local/credentials.yaml")
    cursor = db_conn.cursor()

    try:
        cursor.execute(q)
        ofertas = cursor.fetchall()
        db_conn.commit()
    except (Exception, psycopg2.DatabaseError) as error:
        logging.error(error)

    return ofertas


def get_knowledge(id_candidato):
    """
    Gets the knowledge and skills that this candidate has.
    :param id_candidato: Id from the candidate.
    :return: Knowledge and skills as string.
    """
    q = """
        select 
            conocimiento_habilidad
        from 
            {}.{}
        where 
            id_candidato = {}
        order by 
            fecha_alta desc
        limit 1
    """.format(
        constants.SCHEMA_STPS,
        constants.STPS_CANDIDATO_CONOCIMIENTO_HABILIDAD,
        id_candidato
        )

    db_conn = get_db_conn("../../conf/local/credentials.yaml")
    cursor = db_conn.cursor()

    try:
        cursor.execute(q)
        conocimiento_habilidad = cursor.fetchall()
        db_conn.commit()
    except (Exception, psycopg2.DatabaseError) as error:
        logging.error(error)

    text = " ".join([element for element in conocimiento_habilidad[0]])

    return text


def clean_text(text_to_look, root_form, nlp):
    """Preprocess text.
    :param text_to_look: Text to search.
    :param root_form: Type of text normalization performed.
    :param nlp: Spanish language model from spacy.
    :return: Cleaned text.
    """
    steps = ["basic_cleaning", "remove_accents", "remove_punctuation", "remove_numbers",
    "remove_stopwords", root_form, "remove_morewords"]

    cleaned_text = preprocess_text(text_to_look, steps, nlp)

    return cleaned_text


def sort_offers(offers, n_offerings):
    """
    Gets top job offerings. Sorted by the date most recent job offer published.
    :param offers: List of offers found by the model.
    :param n_offerings: Number of top offerings to retrieve.
    :return: List of top offers sorted by published date.
    """
    q = """
        select 
            id_oe::INTEGER as id_oferta_empleo
        from 
            {}.{}
        where 
            id_oe in {}
        order by fecha_ini_vigencia desc
        limit {}
    """.format(
        constants.SCHEMA_STPS,
        constants.STPS_OFERTA_EMPLEO,
        tuple(offers),
        n_offerings
        )

    db_conn = get_db_conn("../../conf/local/credentials.yaml")
    cursor = db_conn.cursor()

    try:
        cursor.execute(q)
        top_n = cursor.fetchall()
        db_conn.commit()
    except (Exception, psycopg2.DatabaseError) as error:
        logging.error(error)

    return top_n


def search_group_knowledge(id_candidato, n_offerings=20, lemmatization=True, fasttext=True):
    """
    Gets the job offers that are the most similiar to the knowledge and abilities of the candidate.
    :param id_candidato: Id form the candidate.
    :param n_offerings: Number of job offerings to retrieve per job seeker. Defaults to 20.
    :param lemmatization: Type of text normalization. Defaults to lemmatization, but could be changed to stemming.
    :param fasttext: Type fo embedding. Defaults for fasttext, but can be changed to word2vec.
    :return: List of top n job offers
    """
    if lemmatization:
        root_form = "lemmatization"
        nlp = spacy.load('es_core_news_lg')
    else:
        root_form = "stemming"
        nlp = None
    if fasttext:
        model_embedding = "fasttext"
    else:
        model_embedding = "word2vec"

    knowledge_abilities = get_knowledge(id_candidato)
    cleaned_text = clean_text(knowledge_abilities, root_form, nlp)
    model = load_vec_model(root_form, model_embedding)
    most_similar_word = get_most_similar(cleaned_text, model)
    group = get_group(most_similar_word, root_form, model_embedding)
    offers = get_offers(group, root_form, model_embedding, n_offerings)
    top_n = sort_offers(offers, n_offerings)
    # convert list of tuples into list
    output = [element for elem in top_n for element in elem]

    return output
