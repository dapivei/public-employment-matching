"""
MODELS TO GENERATE VOCABULARY AND GROUPS OF
KNOWLEDGES SPECIFIED BY DIFFERENT JOB CANDIDATES
"""
import pandas as pd
import numpy as np
import psycopg2
import logging
import boto3
import pickle

from sklearn.cluster import KMeans
from gensim.models import Word2Vec, FastText
from psycopg2 import extras
from datetime import date

from src.utils.general import get_db_conn, get_s3_credentials
from src.utils import constants


def get_clean_conocimientos_habilidades(root_form):
    """
    Retrieve knowledges specified by job applicants.
    :param root_form: Type of normalization performed on knowledges.
    :return: Clean knowledges specified in job applicants.
    """
    q = """
        select
            id_bt,
            conocimientoherramienta_bt
        from
            {}.{}
        where
            conocimientoherramienta_bt != ''
    """.format(constants.SCHEMA_CLEAN, constants.CANDIDATO_CONOCIMIENTO_HABILIDAD + "_text_" + root_form)

    db_conn = get_db_conn("../../conf/local/credentials.yaml")
    cursor = db_conn.cursor()

    try:
        cursor.execute(q)
        conocimientos = cursor.fetchall()
        db_conn.commit()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
        logging.error(error)
    return conocimientos


def format_conocimientos_for_vec(conocimientos):
    """
    Changes the knowledge, abilities, description format into a list of elements, one element per word.
    :param conocimientos: Data Frame of knowledge and abilities.
    :return: List of elements.
    """
    return [element[1].split() for element in conocimientos]


def save_model(model, model_embedding, root_form):
    """
    Save vectorizer model into S3.
    :param model: Model to be persisted in S3.
    :param root_form: Type of normalization.
    :param model_embedding: Type of embedding.
    :return: Nothing.
    """
    s3_creds = get_s3_credentials("../../conf/local/credentials.yaml")

    session = boto3.Session(
        aws_access_key_id=s3_creds['aws_access_key_id'],
        aws_secret_access_key=s3_creds['aws_secret_access_key']
    )
    s3 = session.client('s3')
    if model_embedding == 'fasttext':
        embedding = constants.FASTTEXT_KNOWLEDGE_MODEL
    else:
        embedding = constants.WORD2VEC_KNOWLEDGE_MODEL
    file_name = embedding + "_" + root_form + '.model'
    s3_path = "s3://" + constants.S3_BUCKET + "/" + constants.S3_PATH_KNOWLEDGE_MODEL + root_form + "/" + file_name
    # store in s3 bucket
    model.wv.save_word2vec_format(s3_path)


def run_model(conocimientos, model_embedding, root_form):
    """
    Runs the vectorizer algorithm.
    :param conocimientos: List knowledge, abilities and description, one cleaned word as an element in the list.
    :param root_form: Type of normalization.
    :param model_embedding: Type of embedding.
    :return: Vocabulary and features extracted by vectorizer model.
    """
    np.random.seed(constants.NUMPY_SEED)

    if model_embedding == "fasttext":
        model = FastText(
            sentences=conocimientos, size=constants.VEC_SIZE, window=constants.VEC_WINDOW,
            min_count=constants.VEC_MIN_COUNT, workers=constants.VEC_WORKERS
        )
    else:
        model = Word2Vec(
            sentences=conocimientos, size=constants.VEC_SIZE, window=constants.VEC_WINDOW,
            min_count=constants.VEC_MIN_COUNT, workers=constants.VEC_WORKERS
        )

    vec_keys = list(model.wv.vocab.keys())

    # extract features from vectorizer model
    vec_features = model[model.wv.vocab]

    # save the model in S3
    save_model(model, model_embedding, root_form)

    return vec_keys, vec_features


def save_kmeans(kmeans_vec, model_embedding, root_form):
    """
    Stores kmeans generated from model.
    :param kmeans_vec:
    :param root_form: Type of normalization.
    :param model_embedding: Type of embedding.
    :return: Nothing.
    """
    s3_creds = get_s3_credentials("../../conf/local/credentials.yaml")

    session = boto3.Session(
        aws_access_key_id=s3_creds['aws_access_key_id'],
        aws_secret_access_key=s3_creds['aws_secret_access_key']
    )
    s3 = session.client('s3')
    if model_embedding == "fasttext":
        name_model = constants.FASTTEXT_KNOWLEDGE_MODEL
    else:
        name_model = constants.WORD2VEC_KNOWLEDGE_MODEL

    file_name = "kmeans_" + name_model + "_" + root_form + ".pkl"
    s3_path = constants.S3_PATH_KNOWLEDGE_MODEL + root_form + "/" + file_name
    # store in s3 bucket
    kmeans_pickled = pickle.dumps(kmeans_vec)
    try:
        s3.put_object(Bucket=constants.S3_BUCKET, Key=s3_path, Body=kmeans_pickled)
    except(Exception) as error:
        print(error)

def generate_groups_kmeans(vec_features, model_embedding, root_form, clusters_n):
    """
    Generate the groups for the knowledges with KMeans.
    :param vec_features: Features extracted by model.
    :param root_form: Type of normalization.
    :param model_embedding: Type of embedding.
    :param clusters_n: Number of clusters formed.
    :return: Nothing.
    """
    kmeans_knowledge = KMeans(
    n_clusters=clusters_n,
    max_iter=constants.KMEANS_MAX_ITER,
    random_state=constants.KMEANS_SEED
    )
    kmeans_ = kmeans_knowledge.fit(vec_features)

    save_kmeans(kmeans_, model_embedding, root_form)

    return kmeans_


def get_groups(conocimientos_list, labels_dict):
    """
    Each knowledge can be part of different groups, we only stores the top 3 groups that each knowledge belongs to.
    :param conocimientos_list: List of knowledges.
    :param labels_dict: Dictionary with the vocabulary extracted by vectorizer model and the group where ir belongs.
    :return: Groups for knowledges defined by job applicants.
    """
    aux = [labels_dict[element] for element in conocimientos_list]
    if len(aux) > 1:
        groups_dict = {element: aux.count(element) for element in aux}
    else:
        groups_dict = {aux[0]: 1}
    groups_sorted = sorted(groups_dict.items(), key=lambda x: x[1], reverse=True)
    groups = [element[0] for element in groups_sorted]

    try:
        grupo = groups[0]
    except (Exception) as error:
        logging.error(error)

    return grupo


def save_vocabulary_in_db(vocabulary, model_embedding, root_form):
    """
    Save vocabulary in DB.
    :param vocabulary: Data frame of vocabulary to persist.
    :param root_form: Type of normalization.
    :param model_embedding: Type of word embedding.
    :return: Nothing, except when an error occurs.
    """
    q = "insert into {}.{} values({})".format(
        constants.SCHEMA_GROUPS,
        constants.KNOWLEDGE_VOCABULARY_TABLE + "_" + model_embedding + "_" + root_form,
        ", ".join(['%s'] * 3)
        )

    db_conn = get_db_conn("../../conf/local/credentials.yaml")
    cursor = db_conn.cursor()

    labels = [list(row) for row in vocabulary.itertuples(index=False)]

    try:
        extras.execute_batch(cursor, q, labels)
        db_conn.commit()
    except (Exception, psycopg2.DatabaseError) as error:
        logging.error(error)


def assign_knowledge_to_group(kmeans_vec, vec_keys, lista_conocimientos, conocimientos, model_embedding, root_form):
    """
    Assigns each knowledge to a group generated with KMeans.
    :param kmeans_vec: Groups generated by KMeans.
    :param vec_keys: Vocabulary from model.
    :param lista_conocimientos: List of knowledges.
    :param conocimientos: Cleaned knowledges.
    :param model_embedding: Type of embedding.
    :param root_form: Type of normalization.
    :return: Assigment of groups for knowledges specified by job applicants.
    """
    labels = pd.DataFrame(
        {'vocabulary': vec_keys, 
        'grupo': kmeans_vec.labels_,
        'date_assigned': np.repeat(date.today(),len(vec_keys))}
        )
    # save vocabulary in DB
    save_vocabulary_in_db(labels, model_embedding, root_form)
    labels_dict = dict(zip(labels.vocabulary, labels.grupo))
    text_groups = [get_groups(ele, labels_dict) for ele in lista_conocimientos]
    df_grupos = pd.DataFrame(text_groups, columns=['grupo'])
    conocimientos_ = pd.DataFrame(conocimientos, columns=["id_oferta_empleo", "conocimientos"])
    conocimientos_grupos = conocimientos_.join(df_grupos)

    # convert ot int
    conocimientos_grupos['grupo'] = conocimientos_grupos.grupo.astype(int)
    conocimientos_grupos['date_assigned'] = pd.to_datetime('today').date()
    return conocimientos_grupos


def save_conocimientos_grupo(conocimientos_con_grupo, model_embedding, root_form):
    """
    Persists each existing knowledge in DB with their corresponding group.
    :param ofertas_con_grupo: Data frame with all the knowledges and its corresponding group.
    :param model_embedding: Type of embedding.
    :param root_form: Type of normalization.  
    :return: Nothing.
    """
    # convert offers into list of tuples
    conocimientos_ = conocimientos_con_grupo.drop(['conocimientos'], axis=1)
    conocimientos = [list(row) for row in conocimientos_.itertuples(index=False)]

    # save offers with group from function
    q = "insert into {}.{} values({})".format(
        constants.SCHEMA_GROUPS,
        constants.KNOWLEDGE_CANDIDATE + "_" + model_embedding + "_" + root_form,
        ", ".join(["%s"] * 3)
        )

    db_conn = get_db_conn("../../conf/local/credentials.yaml")
    cursor = db_conn.cursor()

    try:
        extras.execute_batch(cursor, q, conocimientos)
        db_conn.commit()
    except (Exception, psycopg2.DatabaseError) as error:
        logging.error(error)


def groups_from_embedding(lemmatization=True, fasttext=True):
    """
    Pipeline for generating and persisting the groups for knowledges of job applicants
    :param lemmatization: Type of text normalization. Defaults to lemmatization, but could be changed to stemming
    :param fasttext: Type fo embedding. Defaults for fasttext, but can be changed to word2vec
    :return: Nothing.
    """
    # defining name of variable to save the output (depending on the type of text normalization specified)
    if lemmatization and fasttext:
        root_form = "lemmatization"
        model_embedding = "fasttext"
        clusters_n = constants.KMEANS_CONOCIMIENTOS_N_GROUPS_FASTTEXT_LEMMATIZATION
    elif lemmatization == False and fasttext == False:
        model_embedding = "word2vec"
        root_form = "stemming"
        clusters_n = constants.KMEANS_CONOCIMIENTOS_N_GROUPS_WORD2VEC_STEMMING
    elif lemmatization == False and fasttext == True:
        root_form = "stemming"
        model_embedding = "fasttext"
        clusters_n = constants.KMEANS_CONOCIMIENTOS_N_GROUPS_FASTTEXT_STEMMING
    else:
        model_embedding = "word2vec"
        root_form = "lemmatization"
        clusters_n = constants.KMEANS_CONOCIMIENTOS_N_GROUPS_WORD2VEC_LEMMATIZATION

    # knowledges from job applicants
    conocimientos = get_clean_conocimientos_habilidades(root_form)
    lista_conocimientos = format_conocimientos_for_vec(conocimientos)
    vec_keys, vec_features = run_model(lista_conocimientos, model_embedding, root_form)
    kmeans_vec = generate_groups_kmeans(vec_features, model_embedding, root_form, clusters_n)
    conocimientos_con_grupo = assign_knowledge_to_group(kmeans_vec, vec_keys, lista_conocimientos, conocimientos, model_embedding, root_form)
    save_conocimientos_grupo(conocimientos_con_grupo, model_embedding, root_form)
    print('Done!')

if __name__ == '__main__':
    groups_from_embedding()
