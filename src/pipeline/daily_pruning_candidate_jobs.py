import psycopg2
import logging
import time

from psycopg2 import extras
from datetime import date
from concurrent.futures import ProcessPoolExecutor
from collections import defaultdict

from src.utils.general import get_db_conn
from src.utils import constants
from src.pipeline import knowledge_search

TODAY = date.today()
logging.basicConfig(filename='daily_pruning_candidate_jobs_' + str(TODAY) + '.log', level=logging.INFO)


def prune_expired_recommendations():
    """
    Identifies which offers have already been expired and prune them from the results_copy table
    :return: List of offers id pruned
    """
    q = """ 
        with expired_offers as (
            select 
                distinct idoferta 
            from 
                {}.{} a
            join 
                {}.{} b
            on a.idoferta = b.id_oe
            where fecha_vigencia <= '{}'
        )
        
        delete 
        from 
          {}.{}
        where idoferta in (select idoferta from expired_offers)
        returning idoferta
    """.format(constants.SCHEMA_STPS, constants.STPS_RESULTADO_OFERTAS_PARABT,
               constants.SCHEMA_STPS, constants.STPS_OFERTA_EMPLEO, TODAY,
               constants.SCHEMA_STPS, constants.STPS_RESULTADO_OFERTAS_PARABT)

    db_conn = get_db_conn("../../conf/local/credentials.yaml")
    cursor = db_conn.cursor()

    try:
        cursor.execute(q)
        expired_offers = cursor.fetchall()
        db_conn.commit()
    except (Exception, psycopg2.DatabaseError) as error:
        logging.error(error)

    return expired_offers


def pruning_process():
    """
    Main process of pruning expired offers
    :return:
    """
    start_time = time.time()
    expired_offers = prune_expired_recommendations()
    for expired in expired_offers:
        logging.info("deleted expired offer: " + str(expired[0]) + " " + str(TODAY))
    logging.info("total time: " + str(time.time() - start_time))


pruning_process()