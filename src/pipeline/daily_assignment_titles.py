import psycopg2
import logging

from datetime import date
from psycopg2 import extras

from src.utils.general import get_db_conn
from src.pipeline import title_search
from src.utils import constants
from src.pipeline import knowledge_search

TODAY = date.today()


def get_new_job_offers():
    """
    Finds the job offers that doesn't have a group assigned yet

    :return: job offers with no group assigned
    """
    db_conn = get_db_conn("../../conf/local/credentials.yaml")

    q = """
        with ofertas_empleo as(
            select 
                id_oe,
                titulo_oe
            from
                clean.oferta_empleo
            where 
                fecha_ini_vigencia <= {}
            and fecha_vigencia > {}
        ),
        
        ofertas_grupo as(
            select
                id_oferta_empleo
            from 
                groups.titulos_ofertas
        )
        
        select 
            id_oferta_empleo, 
            titulo_oferta
        from 
            ofertas_empleo 
        where
            id_oferta_empleo not in (select id_oferta_empleo from ofertas_grupo)
    """.format(TODAY, TODAY)

    cursor = db_conn.cursor()

    try:
        cursor.execute(q)
        new_job_offers = cursor.fetchall()
        db_conn.commit()
    except (Exception, psycopg2.DatabaseError) as error:
        logging.error(error)

    return new_job_offers


def group_process(job_offer):
    """
    Gets the group where this job offer belongs to

    :param job_offer: tuple of id_oferta_empleo and job offer title
    :return: tuple of id_oferta and group that this job offer belongs to
    """
    id_oferta = job_offer[0]
    title = job_offer[1]
    functions = job_offer[2]

    # groups for titles
    cleaned_text = title_search.clean_text(title)
    model = title_search.load_word2vec_model()
    most_similar_word = title_search.get_most_similar(cleaned_text, model)
    group = title_search.get_group(most_similar_word)

    return id_oferta, group


def assign_group(new_job_offers):
    """
    Assign a group to a job offer that did not have one yet.

    :param new_job_offers: List of job offers that requires to look for their group
    :return: List of job offers with their corresponding group
    """
    offers_groups = [(group_process(job_offer)) for job_offer in new_job_offers]

    return offers_groups


def persist_offer_group(offers_groups):
    """
    Persists the job offers that didn't have a group assigned in DB
    :param offers_groups: List of tuples of the job offers to persist
    :return:
    """

    q = "insert into {}.{} values({});".format(constants.SCHEMA_GROUPS, constants.TITLE_OFFERS,
                                               ", ".join(["%s"] * 2))

    db_conn = get_db_conn("../../conf/local/credentials.yaml")
    cursor = db_conn.cursor()

    try:
        extras.execute_batch(cursor, q, offers_groups)
        db_conn.commit()
    except (Exception, psycopg2.DatabaseError) as error:
        logging.error(error)


def daily_assignment():
    """

    :return:
    """
    new_job_offers = get_new_job_offers()
    offers_groups = assign_group(new_job_offers)
    persist_offer_group(offers_groups)


daily_assignment()

