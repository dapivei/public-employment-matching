import psycopg2
import logging

from src.utils.general import get_db_conn
from src.utils.constants import BASELINE_FUISTE_CONTRATADO, BASELINE_LINEA_TIEMPO


def get_tps(from_date, to_date, table, column, positive_label):

    q = """
        select 
            count(*) as tps
        from 
            clean.{}
        where fecha_alta between '{}' and '{}'
        and compatibilidad > 64 
        and {} = {}
    """.format(table, from_date, to_date, column, positive_label)

    db_conn = get_db_conn("../../conf/local/credentials.yaml")

    try:
        cursor = db_conn.cursor()
        cursor.execute(q)
        tps = cursor.fetchone()[0]
        db_conn.commit()
    except (Exception, psycopg2.DatabaseError) as error:
        logging.error(error)

    return tps


def get_fps(from_date, to_date, table, column, negative_label, baseline):

    if baseline == BASELINE_FUISTE_CONTRATADO:
        q = """
            select 
                count(*) as fps
            from 
                clean.{}
            where fecha_alta between '{}' and '{}'
            and compatibilidad > 64
            and ({} in({}) 
            or {} is NULL)
         """.format(table, from_date, to_date, column, negative_label, column)
    else:
        q = """
             select 
                count(*) as fps
            from 
                clean.{}
            where fecha_alta between '{}' and '{}'
            and compatibilidad > 64
            and {} = {}
         """.format(table, from_date, to_date, column, negative_label)

    db_conn = get_db_conn("../../conf/local/credentials.yaml")

    try:
        cursor = db_conn.cursor()
        cursor.execute(q)
        fps = cursor.fetchone()[0]
        db_conn.commit()
    except (Exception, psycopg2.DatabaseError) as error:
        logging.error(error)

    return fps


def get_tns(from_date, to_date, table, column, negative_label, baseline):

    if baseline == BASELINE_FUISTE_CONTRATADO:
        q = """
            select 
                count(*) as fps
            from 
                clean.{}
            where fecha_alta between '{}' and '{}'
            and compatibilidad < 65
            and ({} in ({}) 
            or {} is NULL)
        """.format(table, from_date, to_date, column, negative_label, column)
    else:
        q = """
            select 
                count(*) as fps
            from 
                clean.{}
            where fecha_alta between '{}' and '{}'
            and compatibilidad < 65
            and {} = {}
            """.format(table, from_date, to_date, column, negative_label)

    db_conn = get_db_conn("../../conf/local/credentials.yaml")

    try:
        cursor = db_conn.cursor()
        cursor.execute(q)
        tns = cursor.fetchone()[0]
        db_conn.commit()
    except (Exception, psycopg2.DatabaseError) as error:
        logging.error(error)

    return tns


def get_fns(from_date, to_date, table, column, positive_label):
    q = """
        select 
            count(*) as fps
        from 
            clean.{}
        where fecha_alta between '{}' and '{}'
        and compatibilidad < 65
        and {} = {} 
    """.format(table, from_date, to_date, column, positive_label)

    db_conn = get_db_conn("../../conf/local/credentials.yaml")

    try:
        cursor = db_conn.cursor()
        cursor.execute(q)
        fns = cursor.fetchone()[0]
        db_conn.commit()
    except (Exception, psycopg2.DatabaseError) as error:
        logging.error(error)

    return fns


def get_precision(tps, fps):
    return tps/(tps + fps)


def get_recall(tps, fns):
    return tps/(tps + fns)


def precision_recall_baseline_as_of_date(from_date, to_date, baseline):
    if baseline == BASELINE_FUISTE_CONTRATADO:
        table = "oferta_candidato"
        column = "fuiste_contratado"
        positive_label = "'Sí'"
        negative_label = "'No' , 'Estoy en espera del resultado del proceso de selección'"
    else:
        table = "ground_truth"
        column = "label"
        positive_label = 1
        negative_label = 0

    tps = get_tps(from_date, to_date, table, column, positive_label)
    fps = get_fps(from_date, to_date, table, column, negative_label, baseline)
    tns = get_tns(from_date, to_date, table, column, negative_label, baseline)
    fns = get_fns(from_date, to_date, table, column, positive_label)

    precision = get_precision(tps, fps)
    recall = get_recall(tps, fns)

    return precision, recall
