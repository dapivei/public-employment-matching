import pandas as pd
import numpy as np
import psycopg2
import logging
import boto3
import pickle
import sys

from sklearn.cluster import KMeans
from gensim.models import Word2Vec
from psycopg2 import extras
from datetime import date

from src.utils.general import get_db_conn, get_s3_credentials
from src.utils import constants

TODAY = date.today()


def get_clean_funciones():
    q = """
        select 
            id_oe,
            funciones_oe
        from 
            stps.oferta_empleo_text
        where 
            funciones_oe != ''
    """
    db_conn = get_db_conn("../../conf/local/credentials.yaml")
    cursor = db_conn.cursor()

    try:
        cursor.execute(q)
        funciones = cursor.fetchall()
        db_conn.commit()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
        logging.error(error)

    return funciones



def get_clean_text_conocimientos(id_candidato):
    """
    Get all existing offer titles from the DB
    :param id_candidato:
    :return:
    """
    # get clean text for conocimientos_habilidades
    q = """
        with conocimiento_habilidad as(
            select
                id_bt
            from 
                stps.bt_conocimiento_habilidad
            where 
                id_bt = {}
            order by 
                fecha_hora_registro_bt
            limit 1    
        )
        
        select
            conocimientoherramienta_bt
        from 
            clean.candidato_conocimiento_habilidad_text
        where id_oe in (
            select id_oe
            from conocimiento_habilidad
        )
    """.format(id_candidato)

    db_conn = get_db_conn("../../conf/local/credentials.yaml")
    cursor = db_conn.cursor()

    try:
        cursor.execute(q)
        conocimientos_habilidades = cursor.fetchall()
        db_conn.commit()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
        logging.error(error)

    return conocimientos_habilidades


def format_conocimiento(conocimientos_habilidades):
    """

    :param conocimientos_habilidades:
    :return:
    """
    return [ele for element in conocimientos_habilidades for ele in element]


def format_funciones_for_word2vec(funciones):
    """
    Changes the knowledge, abilities, description format into a list of elements, one element per word.
    :param funciones: Data Frame of knowledge and abilities
    :return:
    """

    return [element[1].split() for element in funciones]



def save_word2vec_model(model):
    """
    Save word2vec model into S3
    :param model: Model to be persisted in S3
    :return:
    """
    s3_creds = get_s3_credentials("../../conf/local/credentials.yaml")

    session = boto3.Session(
        aws_access_key_id=s3_creds['aws_access_key_id'],
        aws_secret_access_key=s3_creds['aws_secret_access_key']
    )
    s3 = session.client('s3')

    file_name = constants.WORD2VEC_FUNCTIONS_MODEL
    s3_path = "s3://" + constants.S3_BUCKET + "/" + constants.S3_PATH_FUNCTION_MODEL + file_name
    # store in s3 bucket
    model.wv.save_word2vec_format(s3_path)


def run_word2vec(conocimientos):
    """
    Runs the Word2Vec algorithm
    :param conocimientos: List knowledge, abilities and description, one cleaned word as an element in the list.
    :return: Vocabulary and features extracted by Word2Vec
    """
    np.random.seed(constants.NUMPY_SEED)
    model = Word2Vec(sentences=conocimientos, size=constants.WORD2VEC_SIZE, window=constants.WORD2VEC_WINDOW,
                     min_count=constants.WORD2VEC_MIN_COUNT, workers=constants.WORD2VEC_WORKERS)

    word2vec_keys = list(model.wv.vocab.keys())

    # extract features from word2vec
    word2vec_features = model[model.wv.vocab]

    # save the model in S3
    save_word2vec_model(model)

    return word2vec_keys, word2vec_features


def save_kmeans(kmeans_word2vec):
    """
    Stores kmeans generated from word2vec
    :param kmeans_word2vec:
    :return:
    """
    s3_creds = get_s3_credentials("../../conf/local/credentials.yaml")

    session = boto3.Session(
        aws_access_key_id=s3_creds['aws_access_key_id'],
        aws_secret_access_key=s3_creds['aws_secret_access_key']
    )
    s3 = session.client('s3')

    file_name = constants.KMEANS_FUNCTIONS_MODEL
    s3_path = constants.S3_PATH_FUNCTION_MODEL + file_name
    # store in s3 bucket
    kmeans_pickled = pickle.dumps(kmeans_word2vec)
    try:
        s3.put_object(Bucket=constants.S3_BUCKET, Key=s3_path, Body=kmeans_pickled)
    except(Exception) as error:
        print(error)


def generate_groups_kmeans(word2vec_features):
    """
    Generate the groups for the offer titles with KMeans
    :param word2vec_features: Features extracted by Word2Vec
    :return:
    """
    kmeans_conocimientos = KMeans(n_clusters=constants.KMEANS_FUNCIONES_N_GROUPS,
                                  max_iter=constants.KMEANS_MAX_ITER,
                           random_state=constants.KMEANS_SEED)
    kmeans_word2vec = kmeans_conocimientos.fit(word2vec_features)

    save_kmeans(kmeans_word2vec)

    return kmeans_word2vec


def get_groups(titulo_oferta_list, labels_dict):
    """
    Each offer title can be part of different groups, we only stores the top 3 groups that each title belongs to
    :param titulo_oferta_list: List of offer titles
    :param labels_dict: Dictionary with the vocabulary extracted by Word2Vec and the group where ir belongs
    :return:
    """
    aux = [labels_dict[element] for element in titulo_oferta_list]
    if len(aux) > 1:
        groups_dict = {element: aux.count(element) for element in aux}
    else:
        groups_dict = {aux[0]: 1}
    groups_sorted = sorted(groups_dict.items(), key=lambda x: x[1], reverse=True)
    groups = [element[0] for element in groups_sorted]

    try:
        grupo = groups[0]
    except (Exception) as error:
        logging.error(error)

    return grupo


def save_vocabulary_in_db(vocabulary):
    """
    Save vocabulary in DB
    :param vocabulary: Data frame of vocabulary to persist
    :return:
    """
    q = "insert into {}.{} values({})".format(constants.SCHEMA_GROUPS,
                                              constants.FUNCTIONS_VOCABULARY_TABLE, ", ".join(['%s'] * 2))

    db_conn = get_db_conn("../../conf/local/credentials.yaml")
    cursor = db_conn.cursor()

    labels = [list(row) for row in vocabulary.itertuples(index=False)]

    try:
        extras.execute_batch(cursor, q, labels)
        db_conn.commit()
    except (Exception, psycopg2.DatabaseError) as error:
        logging.error(error)


def assign_function_to_group(kmeans_word2vec, word2vec_keys, lista_funciones, funciones):
    """
    Assigns each offer title to a group generated with KMeans
    :param kmeans_word2vec: Groups generated by KMeans
    :param word2vec_keys: Vocabulary from Word2Vec
    :param lista_funciones: Titles of offers
    :param funciones: Cleaned offer titles
    :return:
    """
    labels = pd.DataFrame({'vocabulary': word2vec_keys, 'grupo': kmeans_word2vec.labels_})
    # save vocabulary in DB
    save_vocabulary_in_db(labels)
    labels_dict = dict(zip(labels.vocabulary, labels.grupo))
    text_groups = [get_groups(ele, labels_dict) for ele in lista_funciones]
    df_grupos = pd.DataFrame(text_groups, columns=['grupo'])
    funciones_ = pd.DataFrame(funciones, columns=["id_oferta_empleo", "funcion"])
    ofertas_grupos = funciones_.join(df_grupos)

    # convert ot int
    ofertas_grupos['grupo'] = ofertas_grupos.grupo.astype(int)

    return ofertas_grupos


def save_ofertas_grupo(ofertas_con_grupo):
    """
    Persists each existing offer in DB with their corresponding group
    :param ofertas_con_grupo: Data frame with al the offers and its corresponding group
    :return:
    """
    # convert offers into list of tuples
    ofertas_ = ofertas_con_grupo.drop(['funcion'], axis=1)
    ofertas = [list(row) for row in ofertas_.itertuples(index=False)]

    # save offers with group from function
    q = "insert into {}.{} values({})".format(constants.SCHEMA_GROUPS,
                                              constants.FUNCTIONS_OFFERS, ", ".join(["%s"] * 2))

    db_conn = get_db_conn("../../conf/local/credentials.yaml")
    cursor = db_conn.cursor()

    try:
        extras.execute_batch(cursor, q, ofertas)
        db_conn.commit()
    except (Exception, psycopg2.DatabaseError) as error:
        logging.error(error)


def groups_from_word2vec():
    """
    Pipeline for generating and persisting the groups for job offers functions
    :return:
    """
    # job functions from offers
    funciones = get_clean_funciones()
    lista_funciones = format_funciones_for_word2vec(funciones)

    word2vec_keys, word2vec_features = run_word2vec(lista_funciones)
    kmeans_word2vec = generate_groups_kmeans(word2vec_features)
    funciones_con_grupo = assign_function_to_group(kmeans_word2vec, word2vec_keys, lista_funciones, funciones)
    save_ofertas_grupo(funciones_con_grupo)


if __name__ == '__main__':
    groups_from_word2vec()
