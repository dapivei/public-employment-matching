"""
FOR EACH FIRM, RETRIEVES THE TOP N JOB SEEKERS
THAT FIT BETTER TO THE SKILLS OR KNOWLEDGES THE
JOB OPENNING REQUIRES
"""

import psycopg2
import boto3
import logging
import spacy

from gensim.models import Word2Vec, KeyedVectors
from datetime import date

from src.pipeline.text_preprocessing import preprocess_text
from src.utils import constants
from src.utils.general import get_s3_credentials, get_db_conn


TODAY = date.today()


#logging.basicConfig(filename='daily_functions_search_' + str(TODAY) + '.log', level=logging.INFO)


def load_vec_model(root_form, model_embedding):
    """
    Loads the vectorizer model from S3.
    :param root_form: Type of normalization performed to text.
    :param model_embedding: Type of vectorizer.
    :return: Loaded model.
    """
    s3_creds = get_s3_credentials("../../conf/local/credentials.yaml")

    session = boto3.Session(
        aws_access_key_id=s3_creds['aws_access_key_id'],
        aws_secret_access_key=s3_creds['aws_secret_access_key'],
        region_name=constants.S3_REGION
    )
    s3 = session.resource('s3')
    if model_embedding == 'fasttext':
        embedding = constants.FASTTEXT_KNOWLEDGE_MODEL
    else:
        embedding = constants.WORD2VEC_KNOWLEDGE_MODEL
    file_name = embedding + "_" + root_form + '.model'
    s3_path = "s3://" + constants.S3_BUCKET + "/" + constants.S3_PATH_KNOWLEDGE_MODEL + root_form + "/" + file_name

    # load the model
    model = KeyedVectors.load_word2vec_format(s3_path)

    return model


def get_most_similar(cleaned_text, model):
    """
    Get the most similar word from the vectorizer model
    :param cleaned_text: Cleaned text.
    :param model: Which model to look for similarity.
    :return: Most similar word.
    """
    similar = None
    # check for similarity only with keywords that are in the vocabulary
    word_vectors = model.wv
    valid_vocabulary = [element for element in cleaned_text.split() if element in word_vectors.vocab]
    if len(valid_vocabulary) > 0:
        similar = model.most_similar(positive=valid_vocabulary)
        # top 1
        #logging.info("most similar: ", str(similar))

    return similar


def get_group(most_similar_word, root_form, model_embedding):
    """
    Retrieves group name that contains most similar word.
    :param most_similar_word: Most similar word.
    :param root_form: Type of normalization performed to text.
    :param model_embedding: Type of vectorizer employed.
    :return: Group containing the most similar word.
    """
    ending = '_' +  model_embedding + '_' + root_form 

    q = """
        select 
            grupo,
            grupo_name
        from 
            {}.{}
        join
            {}.{}
        using(grupo)
        where 
            vocabulary = '{}'
    """.format(
        constants.SCHEMA_GROUPS, 
        constants.KNOWLEDGE_VOCABULARY_TABLE + ending,
        constants.SCHEMA_GROUPS,
        constants.STPS_CATALOGO_NOMBRES_GRUPOS_CONOCIMIENTOS + ending,
        most_similar_word
        )

    db_conn = get_db_conn("../../conf/local/credentials.yaml")
    cursor = db_conn.cursor()

    try:
        cursor.execute(q)
        grupo = cursor.fetchall()
        db_conn.commit()
    except (Exception, psycopg2.DatabaseError) as error:
        logging.error(error)

    return grupo[0][0]


def get_candidates(group, root_form, model_embedding):
    """
    Get matching job seekers.
    :param group: Group to look for.
    :param root_form: Type of normalization performed to text.
    :param model_embedding: Type of vectorizer employed.    
    :return: List of job seekers belonging to a specific group.
    """
    ending = '_' + model_embedding + '_' + root_form

    q = """
        with candidatos_grupos as(
            select
                id_candidato::integer
            from 
                {}.{}
            where 
                grupo = {}
        ),

        candidatos as(
            select 
                id_bt as id_candidato,
                max(fecha_hora_registro_bt) as fecha_hora_registro_bt
            from 
                {}.{}
            group by
                id_bt,
                fecha_hora_registro_bt
        )
        select 
            id_candidato,
            fecha_hora_registro_bt
        from 
            candidatos_grupos
        join 
            candidatos
        using (id_candidato)
    """.format(
        constants.SCHEMA_GROUPS,
        constants.KNOWLEDGE_CANDIDATE + ending,
        group, 
        constants.SCHEMA_STPS,
        constants.STPS_CANDIDATO_CONOCIMIENTO_HABILIDAD
        )

    db_conn = get_db_conn("../../conf/local/credentials.yaml")
    cursor = db_conn.cursor()

    try:
        cursor.execute(q)
        candidatos = cursor.fetchall()
        db_conn.commit()
    except (Exception, psycopg2.DatabaseError) as error:
        logging.error(error)

    return candidatos


def get_functions(id_oferta_empleo):
    """
    Gets the functions listed in each job offering.
    :param id_ofert_empleo: Id from the job offering.
    :return: Functions listed.
    """
    q = """
        select 
            funciones_oe
        from 
            {}.{}
        where id__oe = {}
    """.format(
        constants.SCHEMA_STPS, 
        constants.STPS_OFERTA_EMPLEO_FUNCIONES, 
        id_oferta_empleo
        )

    db_conn = get_db_conn("../../conf/local/credentials.yaml")
    cursor = db_conn.cursor()

    try:
        cursor.execute(q)
        funciones = cursor.fetchall()
        db_conn.commit()
    except (Exception, psycopg2.DatabaseError) as error:
        logging.error(error)

    text = " ".join([element for element in funciones[0]])

    return text


def clean_text(text_to_look, root_form, nlp):
    """Preprocess text,
    :param text_to_look: Text to search.
    :param root_form: Type of text normalization performed.
    :param nlp: Spanish language model from spacy.
    :return: Cleaned text.
    """
    steps = ["basic_cleaning", "remove_accents", "remove_punctuation", "remove_numbers",
    "remove_stopwords", root_form, "remove_morewords"]

    cleaned_text = preprocess_text(text_to_look, steps, nlp)

    return cleaned_text


def sort_candidates(candidates, n_candidates):
    """
    Gets top job candidates. Sorted by the most recent date of
    registration in the Matching Motor.
    :param candidates: List of job seekers found by the model.
    :param n_offerings: Number of top job seekers to retrieve. 
    :return: List of top job seekers sorted by registration date.
    """
    q = """
        select 
            id_bt::INTEGER as id_candidato
        from 
            {}.{}
        where 
            id_bt in {}
        order by 
            fecha_hora_registro_bt desc
        limit {}
    """.format(
        constants.SCHEMA_STPS, 
        constants.STPS_BUSCADOR_TRABAJO,
        tuple(candidates),
        n_candidates
        )

    db_conn = get_db_conn("../../conf/local/credentials.yaml")
    cursor = db_conn.cursor()

    try:
        cursor.execute(q)
        top_n = cursor.fetchall()
        db_conn.commit()
    except (Exception, psycopg2.DatabaseError) as error:
        logging.error(error)

    return top_n


def search_group_functions(id_oferta, n_candidates=20, lemmatization=True, fasttext=True):
    """
    Gets the job seekers that are the most similiar to the knowledge and abilities required by the job opening.
    :param id_oferta: Id form the job opening.
    :param n_candidates: Number of job seekers to retrieve per job opening. Defaults to 20.
    :param lemmatization: Type of text normalization. Defaults to lemmatization, but could be changed to stemming.
    :param fasttext: Type fo embedding. Defaults for fasttext, but can be changed to word2vec.
    :return: List of top n job seekers.
    """    
    if lemmatization:
        root_form = "lemmatization"
        nlp = spacy.load('es_core_news_lg')
    else:
        root_form = "stemming"
        nlp = None  
    if fasttext:
        model_embedding = "fasttext"
    else:
        model_embedding = "word2vec"

    functions = get_functions(id_oferta)
    cleaned_text = clean_text(functions, root_form, nlp)
    model = load_vec_model(root_form, model_embedding)
    most_similar_word = get_most_similar(cleaned_text, model)
    group = get_group(most_similar_word, root_form, model_embedding)
    candidates = get_candidates(group, root_form, model_embedding)
    top_n = sort_candidates(candidates, n_candidates)
    # convert list of tuples into list
    output = [element for elem in top_n for element in elem]

    return output
