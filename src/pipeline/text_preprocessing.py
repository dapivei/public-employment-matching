"""
CLEANS AND CREATES NORMALIZED SPANISH TEXT REPRESENTATIONS
"""

import psycopg2
import logging
import re
import string
import unicodedata
import spacy

from tqdm import tqdm
from bs4 import BeautifulSoup
from nltk.tokenize import word_tokenize
from nltk.corpus import stopwords
from nltk import SnowballStemmer
from psycopg2 import sql
from psycopg2.extras import execute_batch

from src.utils.general import get_db_conn


def lemmatize_text(text, nlp):
    """
    Lemmatize text using spacy lemmatizer for spanish
    :param text:
    :param nlp: spanish language model from spacy
    :return:
    """
    return remove_accents(" ".join([token.lemma_ for token in nlp(text, disable = ['ner', 'parser'])]))


def stemm_text(text):
    """
    Stemm text using nltk stemmer (Snowball) for spanish
    :param text:
    :return:
    """
    stemm_sp = SnowballStemmer('spanish')
    tokens = word_tokenize(text, "spanish")
    stems = [stemm_sp.stem(token) for token in tokens]

    return ' '.join(stems)


def strip_html_tags(text):
    """
    Eliminates html tags
    :param text: Text to clean
    :return:
    """
    text_wo_html_tags = BeautifulSoup(text, "html.parser").text

    return text_wo_html_tags


def strip_urls(text):
    """
    Eliminates URLs from text (prefix http or www)
    :param text: Text to clean
    :return:
    """
    text_wo_urls = re.sub(r'(http|www)\S+', '', text)

    return text_wo_urls


def basic_cleaning(text):
    """
    Basic cleaning of the text: Text lowering, removes HTML tags in the text, removes URLs in the text,
    converts unicode to ascii string, removes all special characters and corrects spelling
    :param text: Text to clean
    :param spell: Spanish text correction
    :return:
    """
    lower_text = text.lower()
    strip_text = strip_urls(lower_text)
    ascii_text = unicodedata.normalize('NFKD', strip_text). \
        encode('ascii', 'ignore'). \
        decode("utf-8")
    wo_accent = remove_accents(ascii_text)

    return re.sub('\W+', ' ', wo_accent)


def remove_accents(text):
    """
    Strips accents from text
    :param text: Text to clean
    :return:
    """
    text_wo_accents = text.replace('á', 'a').replace('é', 'e').replace('í', 'i').replace('ó', 'o') \
        .replace('ú', 'u').replace('ü', 'u')

    return text_wo_accents


def remove_numbers(text):
    """
    Remove isolated numbers from text
    :param text: Text to remove numbers for
    :return:
    """
    pattern = r'[0-9]'
    text = re.sub(pattern, '', text)
    tokens = word_tokenize(text, "spanish")
    tokens = ['' if token.isdigit() else token for token in tokens]

    return ' '.join(tokens)


def remove_punctuation(text):
    """
    Remove punctuation from text
    :param text: Text to be cleaned
    :return:
    """
    translator = str.maketrans('', '', string.punctuation)
    stripped = [w.translate(translator) for w in word_tokenize(text)]

    return ' '.join(stripped)


def remove_stopwords(text):
    """
    Remove spanish stopwords from text
    :param text: Text to remove spanish stopwords from
    :return:
    """
    tokens = word_tokenize(text)
    custom_stopwords = stopwords.words("spanish") + ['cv', 'sa', 'ampm', 'pmpm', 'amam', 'pmam', 'indica', 'conocimiento', 'ninguno', 'poner']
    text_wo_stopwords = [word for word in tokens if not word in custom_stopwords]

    return ' '.join(text_wo_stopwords)


def remove_morewords(text):
    """
    Remove words that contain a character being repeated more than 3 times
    and words with less than 3 and more than 20 characters
    :param text: Text to clean
    :return:
    """
    tokens = text.split()
    pattern = re.compile(r'(.)\1{3,}')
    clean_tokens = []
    for token in tokens:
        match = pattern.search(token)
        if match:
            pass
        else:
            clean_tokens.append(token)

    return ' '.join([i for i in clean_tokens if 3 < len(i) <= 20])


def preprocess_text(text, steps, nlp):
    """
    Verifies which steps from the text preprocess to execute
    :param text:
    :param steps:
    :param nlp: spanish language model
    :param spell: spanish text correction
    :return: cleaned text
    """
    for step in steps:
        if step == "basic_cleaning":
            text = basic_cleaning(text)
        elif step == 'remove_accents':
            text = remove_accents(text)
        elif step == "remove_punctuation":
            text = remove_punctuation(text)
        elif step == "remove_numbers":
            text = remove_numbers(text)
        elif step == "remove_stopwords":
            text = remove_stopwords(text)
        elif step == "stemming":
            text = stemm_text(text)
        elif step == "lemmatization":
            text = lemmatize_text(text, nlp)
        elif step == "remove_morewords":
            text = remove_morewords(text)

    return text


def insert_clean_text(rows_cleaned, schema, table, text_columns, id_column_table, root_form):
    """
    Update text columns with cleaned text
    :param rows_cleaned: Cleaned text
    :param schema: Schema of the table
    :param table: Table to update
    :param text_columns: Columns to update
    :param id_column_table: Id from the table
    :param root_form: Type of normalization
    :return:
    """
    db_conn = get_db_conn("../../conf/local/credentials.yaml")
    cursor = db_conn.cursor()

    cols = [id_column_table] + text_columns
    set_cols = ", ".join(cols)
    text_table = table + "_text_" + root_form
    values = ", ".join(['%s'] * len(cols))

    q = """insert into  {}.{} ({}) values({})""".format(schema, text_table, set_cols, values)

    try:
        # cursor.execute(q, (tuple(rows_cleaned), id_col_value))
        execute_batch(cursor, q, rows_cleaned)
        db_conn.commit()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
        logging.error(error)
    return


def clean_table_text(schema, table, text_cols, id_column_table, schema_dest, table_dest, lemmatization=True):
    """
    Clean text columns from a specific table, lemmatization is set to true by default,
    if lemmatization==False, then stemming will be perform
    :param id_col_dest:
    :param table_dest:
    :param schema_dest:
    :param text_cols:
    :param schema:
    :param table:
    :param text_columns:
    :param id_column_table:
    :param lemmatization:
    :return:
    """
    # from constants, text_columns is a tuple with a list, that is why we get the first element in the tuple -the
    # list of cols-
    text_columns = text_cols
    if not isinstance(text_columns, list):
        text_columns = [text_columns]
    cols_to_select = [id_column_table] + text_columns

    q = sql.SQL("""
        select 
            {}
        from 
            {}.{}
    """).format(sql.SQL(",").join(map(sql.Identifier, cols_to_select)), sql.Identifier(schema), sql.Identifier(table))

    db_conn = get_db_conn("../../conf/local/credentials.yaml")
    cursor = db_conn.cursor()

    # definition of type of text normalization
    if lemmatization:
        root_form = "lemmatization"
        # loads previously downloaded spanish model
        nlp = spacy.load('es_core_news_lg')
    else:
        root_form = "stemming"
        nlp = None

    preprocess_steps = ["basic_cleaning", "remove_accents", "remove_punctuation", "remove_numbers",
                        "remove_stopwords", root_form, "remove_morewords"]

    # get indices from the text columns
    col_text_indices = [cols_to_select.index(col) for col in text_columns]
    # get indices from the id columns
    col_ids_indices = cols_to_select.index(id_column_table)

    try:
        cursor.execute(q)
        rows = cursor.fetchall()

        rows_cleaned = []
        for i, element in tqdm(enumerate(rows)):
            row_cols = []
            # id column
            id_col_value = element[col_ids_indices]
            # text columns
            for j in col_text_indices:
                if element[j] is not None:
                    col_cleaned = preprocess_text(element[j], preprocess_steps, nlp)
                    row_cols.append(col_cleaned)
                else:
                    row_cols.append(element[j])
            cleaned_rows = [id_col_value] + row_cols
            rows_cleaned.append(tuple(cleaned_rows))

            if i % 1000 == 0 and i > 0:
                print(i)
                insert_clean_text(rows_cleaned, schema_dest, table_dest, text_columns, id_column_table, root_form)
                rows_cleaned = []

        # last batch
        insert_clean_text(rows_cleaned, schema_dest, table_dest, text_columns, id_column_table, root_form)

    except (Exception, psycopg2.DatabaseError) as error:
        logging.error(error)
    return

