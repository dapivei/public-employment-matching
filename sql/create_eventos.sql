set role sne_readwrite;

set timezone = 'Mexico/General';

-- EVENTOS CANDIDATOS
--- Total Eventos Candidatos: 51,639,319

drop type if exists semantic.evento_candidato cascade;

create type semantic.evento_candidato  as enum (
   'iniciar grado académico',
   'terminar grado académico',
   'actualizar expectativa laboral',
   'actualizar grado académico',
   'actualizar idiomas',
   'ingresar al sistema',
   'postular oferta',
   'monitorear postulación',
   'ser contactado',
   'obtener entrevista',
   'obtener contratacion');

drop table if exists semantic.eventos_candidatos;

create table if not exists semantic.eventos_candidatos(
   evento_candidato serial,
   id_candidato int,
   tipo text,
   fecha date,
   atributos jsonb
  );

create index eventos_candidatos_evento_candidato_ix on semantic.eventos_candidatos(evento_candidato asc);
create index eventos_candidatos_id_candidato_ix on semantic.eventos_candidatos(id_candidato asc);
comment on table semantic.eventos_candidatos is 'Each row is an event associated to a job seeker';

-- EVENTOS OFERTAS
--- Total Eventos Ofertas: 5,016,628

drop type if exists semantic.evento_oferta cascade;

create type semantic.evento_oferta  as enum (
   'dar alta vacante',
   'publicar vacante',
   'modificar vacante',
   'cerrar vacante'
   );

drop table if exists semantic.eventos_ofertas;

create table if not exists semantic.eventos_ofertas(
   evento_oferta serial,
   id_oferta text,
   tipo text,
   fecha date,
   atributos jsonb
   );

create index eventos_ofertas_evento_oferta_ix on semantic.eventos_ofertas(evento_oferta asc);
create index eventos_ofertas_id_oferta_ix on semantic.eventos_ofertas(id_oferta asc);
comment on table semantic.eventos_ofertas is 'Each row is an event associated to a job offer'

-- EVENTOS EMPRESAS
--- Total Eventos Empresas: 1,074,260

drop type if exists semantic.evento_empresa cascade;

create type semantic.evento_empresa  as enum (
   'levantar acta constitutiva',
   'dar alta la empresa',
   'actualizar registro empresa',
   'desactivar la empresa'
   );

drop table if exists semantic.eventos_empresas;

create table if not exists semantic.eventos_empresas(
   evento_empresa serial,
   id_empresa text,
   tipo text,
   fecha date,
   atributos jsonb
  );

