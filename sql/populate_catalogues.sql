--tabla catalogo_tipo_conocimiento_habilidad
insert into raw.catalogo_tipo_conocimiento_habilidad values(1, 'conocimiento');
insert into raw.catalogo_tipo_conocimiento_habilidad values(2, 'habilidad');

--tabla catalogo_tipo_oferta
insert into raw.catalogo_tipo_oferta values(1, 'bolsa de trabajo');
insert into raw.catalogo_tipo_oferta values(2, 'bécate');
insert into raw.catalogo_tipo_oferta values(3, 'abriendo espacios');

--tabla catalogo_tipo_oferta_modalidad
insert into raw.catalogo_tipo_oferta_modalidad values(1, 'capacitación mixta');
insert into raw.catalogo_tipo_oferta_modalidad values(2, 'capacitación en la práctica laboral');
insert into raw.catalogo_tipo_oferta_modalidad values(3, 'capacitación para la certificación de competencias
laborales');

--tabla catalogo_edad_requisito
insert into raw.catalogo_edad_requisito values(1, 'si');
insert into raw.catalogo_edad_requisito values(2, 'no');

--tabla catalogo_habilidades
insert into clean.catalogo_habilidades
select
    cast(id_catalogo_opcion as smallint) as id_habilidad,
    opcion as habilidad
from 
    raw.catalogo_opciones
where
    id_catalogo = '66';
