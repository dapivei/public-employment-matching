set time zone 'America/Mexico_City';
--change role
set role sne_readwrite;

DROP SCHEMA IF EXISTS clean CASCADE;

CREATE SCHEMA clean;

--tabla candidatos
DROP  TABLE IF EXISTS clean.candidatos;

CREATE TABLE clean.candidatos(
    id_candidato integer,
    id_usuario integer,
    id_entidad_nacimiento smallint,
    id_estado_civil smallint,
    id_tipo_discapacidad smallint,
    id_medio_portal smallint,
    confidencialidad_datos smallint,
    veracidad_datos smallint,
    aceptacion_terminos smallint,
    fecha_alta timestamp without time zone,
    estatus smallint,
    fecha_ultima_actualizacion timestamp without time zone,
    estilo_cv smallint,
    evalua_cv smallint,
    aviso_oportuno smallint,
    nss varchar,
    credito_infonavit varchar,
    registro_bumeran smallint,
    fecha_confirma timestamp without time zone,
    postulaciones_actuales smallint,
    fuente smallint,
    id_oficina smallint,
    id_motivo_desactivacion varchar,
    discapacidades varchar,
    fecha_ultimo_acceso timestamp without time zone,
    apoyo_prospera smallint,
    ppc_estatus varchar,
    ppc_fecha_inscripcion varchar,
    ppc_aceptacion_terminos varchar,
    ppc_estatus_imss varchar,
    ppc_fecha_baja_imss varchar,
    ppc_tipo_contrato_imss varchar,
    ppc_idmotivo_fuera varchar,
    folio_prospera varchar,
    estatus_georeferencia smallint,
    folio_integrante_prospera varchar,
    consentimiento varchar
);


--tabla candidato_conocimiento_habilidad
DROP TABLE IF EXISTS clean.candidato_conocimiento_habilidad;

CREATE TABLE clean.candidato_conocimiento_habilidad(
    id_candidato_conocimiento_habilidad integer,
    id_candidato integer,
    id_tipo_conocimiento_habilidad smallint,
    conocimiento_habilidad varchar,
    id_experiencia smallint,
    id_dominio smallint,
    descripcion text,
    fecha_alta timestamp with time zone,
    principal varchar
);


--tabla cempresa
DROP  TABLE IF EXISTS clean.empresas;

CREATE TABLE clean.empresas(
    id_empresa integer,
    rfc varchar,
    id_usuario integer,
    tipo_persona varchar,
    razon_social varchar,
    fecha_acta timestamp with time zone,
    descripcion varchar,
    tipo_empresa varchar,
    id_actividad_economica integer,
    numero_empleados integer,
    medio varchar,
    aceptacion_terminos smallint,
    fecha_alta date,
    estatus smallint,
    fecha_ultima_actualizacion timestamp with time zone,
    asegura_datos smallint,
    nombre_comercial varchar,
    fecha_confirma timestamp with time zone,
    id_oficina varchar,
    cargo_contacto varchar,
    tipo_sociedad varchar,
    ofertas_totales integer,
    ofertas_vigentes integer,
    ofertas_pendientes integer,
    ofertas_eliminadas integer,
    personas_colocadas integer,
    fuente smallint,
    tamaño varchar,
    es_empresa_sne smallint,
    estatus_georef_dom smallint,
    estatus_georef_oferta smallint
);


--tabla candidato_grado_academico
DROP TABLE IF EXISTS clean.candidato_grado_academico;

CREATE TABLE clean.candidato_grado_academico(
    id_candidato_grado_academico integer,
    id_candidato integer,
    id_nivel_estudio smallint,
    id_situacion_academica smallint,
    id_carrera_especialidad integer,
    escuela varchar,
    inicio smallint,
    fin smallint,
    fecha_alta timestamp with time zone,
    principal smallint,
    lugar varchar,
    fecha_inicio timestamp with time zone,
    fecha_fin timestamp with time zone,
    id_documento_aprobatorio smallint
);

--tabla candidato_expectativa_laboral
DROP  TABLE IF EXISTS clean.candidato_expectativa_laboral;

CREATE TABLE clean.candidato_expectativa_laboral(

    id_candidato integer,
    id_sector_deseado integer,
    puesto_deseado varchar,
    id_area_laboral_deseada smallint,
    id_ocupacion_deseada integer,
    salario_pretendido numeric(12,4),
    id_tipo_empleo_deseado smallint,
    id_tipo_contrato smallint,
    fecha_alta timestamp with time zone,
    id_expectativa_laboral integer,
    principal smallint,
    id_experiencia smallint,
    id_ocupacion_noc smallint,
    id_sub_area_laboral_deseada integer,
    funciones varchar

);

--linea tiempo solicitantes
DROP TABLE IF EXISTS clean.linea_tiempo_solicitantes;

CREATE TABLE clean.linea_tiempo_solicitantes(
    id_solicitante integer,
    id_candidato integer,
    rfc varchar,
    nss varchar,
    registro varchar,
    modalidad_contrato smallint,
    tipo_trabajador varchar,
    clave_municipio varchar,
    clave_entidad varchar,
    salario numeric(12,4) default null,
    codigo_postal varchar,
    año smallint,
    mes smallint,
    evento varchar,
    salario_mes numeric(12,4) default null,
    modalidad_mes smallint default null
);

--tabla candidato_otro_estudio
DROP  TABLE IF EXISTS clean.candidato_otro_estudio;

CREATE TABLE clean.candidato_otro_estudio(
    id_candidato_otro_estudio integer,
    id_candidato integer,
    id_otro_estudio smallint,
    id_tipo_estudio smallint,
    id_estatus_academico smallint,
    fecha_inicio timestamp with time zone,
    fecha_fin timestamp with time zone,
    nombre_estudio varchar,
    institucion varchar,
    principal smallint,
    pais varchar,
    id_documento_aprobatorio smallint,
    descripcion varchar
);


--tabla oferta_empleo
DROP TABLE IF EXISTS clean.oferta_empleo;

CREATE TABLE clean.oferta_empleo(
    id_oferta_empleo integer,
    id_empresa integer,
    titulo_oferta varchar,
    id_area_laboral smallint,
    id_ocupacion integer,
    funciones varchar,
    id_tipo_empleo smallint,
    dias_laborales varchar,
    hora_entrada smallint,
    hora_salida smallint,
    rolar_turno smallint,
    empresa_ofrece varchar,
    salario numeric(12,4),
    id_tipo_contrato integer,
    id_jerarquia smallint,
    numero_plazas integer,
    limite_postulantes_plaza integer,
    id_discapacidad smallint,
    id_causa_vacante integer,
    fecha_inicio timestamp with time zone,
    fecha_fin timestamp with time zone,
    disponibilidad_viajar smallint NULL,
    disponibilidad_radicar smallint NULL,
    id_nivel_estudio smallint NULL,
    id_situacion_academica smallint NULL,
    habilidad_general varchar,
    experiencia_anios varchar,
    edad_requisito smallint NULL,
    edad_minima smallint NULL,
    edad_maxima smallint NULL,
    genero smallint NULL,
    observaciones varchar,
    id_duracion_aproximada smallint NULL,
    fuente smallint,
    fecha_alta timestamp with time zone,
    contacto_tel smallint NULL,
    contacto_correo smallint NULL,
    estatus smallint,
    fecha_modificacion timestamp with time zone,
    nombre_empresa varchar,
    id_actividad_economica smallint NULL,
    cargo_contacto varchar,
    contacto_domicilio smallint NULL,
    id_vigencia_oferta smallint NULL,
    id_oficina_registro integer,
    id_usuario integer,
    plazas_cubiertas smallint,
    oferta_capacitacion smallint NULL,
    id_oficina_capacitacion smallint NULL,
    publicar_oferta smallint,
    discapacidades varchar,
    plazas_disponibles smallint NULL,
    pertenece_pet smallint NULL,
    plazas_canceladas smallint NULL,
    tipo_oferta smallint NULL,
    tipo_oferta_modalidad smallint NULL,
    id_subarea  smallint NULL,
    rolar_dias_laborales varchar,
    id_area smallint NULL
);



-- tabla oferta_candidato
DROP TABLE IF EXISTS clean.oferta_candidato;

CREATE TABLE clean.oferta_candidato(
    id_oferta_empleo integer,
    id_candidato integer,
    fecha_alta timestamp with time zone,
    estatus varchar,
    id_motivo smallint default null,
    id_oferta_candidato integer,
    compatibilidad smallint default null,
    id_oficina smallint,
    id_usuario integer,
    postulacion_cartera smallint,
    fecha_seguimiento timestamp with time zone default null,
    fecha_colocacion timestamp with time zone default null,
    fecha_inicio_contrata timestamp with time zone default null,
    fuente varchar,
    motivo_descontratacion varchar default null,
    fecha_modificacion timestamp with time zone default null,
    fecha_contacto timestamp with time zone default null,
    consiguio_entrevista varchar,
    fecha_entrevista timestamp with time zone default null,
    fuiste_contratado varchar,
    id_oficina_seguimiento smallint default null,
    id_usuario_seguimiento integer default null,
    fuente_seguimiento smallint
);

--tabla candidato_evento
DROP TABLE IF EXISTS clean.candidato_evento;

CREATE TABLE clean.candidato_evento(
     id_candidato integer,
     id_evento integer,
     folio_registro integer,
     fecha_registro timestamp with time zone,
     fuente smallint,
     id_usuario integer
);

--tabla catalogo_subarea
DROP TABLE IF EXISTS clean.catalogo_subarea;

CREATE TABLE clean.catalogo_subarea(
    id_subarea smallint,
    descripcion varchar,
    id_area smallint,
    num_cat_area smallint
);

--tabla candidato_idioma
DROP  TABLE IF EXISTS clean.candidato_idioma;

CREATE TABLE clean.candidato_idioma(
    id_candidato_idioma integer,
    id_candidato integer,
    id_idioma smallint,
    id_certificacion smallint,
    id_dominio smallint,
    fecha_alta timestamp with time zone,
    registro_ principal smallint,
    id_dominio_habla smallint,
    id_dominio_escrito smallint,
    id_dominio_lectura smallint,
    examen_id smallint,
    examen_puntos bigint
);

--tabla oficina
DROP TABLE IF EXISTS clean.oficina;

CREATE TABLE clean.oficina(
      id_oficina integer,
      clave_oficina varchar default null,
      descripcion varchar default null,
      estatus smallint,
      fecha_alta timestamp with time zone,
      fecha_modificacion timestamp with time zone default null,
      id_entidad smallint default null,
      id_municipio integer default null, -- el máx es 3232044
      unioperativa_oficina varchar,
      id_motivo_desactivacion smallint default null,
      motivo_desactivacion varchar default null,
      tipo_agenda smallint default null,
      url_localizacion varchar default null,
      oficina_reconocida smallint default null
);
--tabla candidato_otro_lenguaje
DROP TABLE IF EXISTS clean.candidato_otro_lenguaje;

CREATE TABLE clean.candidato_otro_lenguaje(
    id_candidato_otro_lenguaje integer,
    id_candidato integer,
    id_lenguaje smallint
);


--tabla patrones_solicitantes_svl
DROP  TABLE IF EXISTS clean.patrones_solicitantes_svl;

CREATE TABLE clean.patrones_solicitantes_svl(
    clave_patron varchar,
    razon_social varchar,
    modalidad_contrato smallint,
    total_trabajadores integer,
    total_trabajadores_permanentes integer,
    total_trabajadores_eventuales smallint,
    total_trabajadores_campo smallint,
    division_economica smallint,
    grupo_actividad_economica smallint,
    rama_actividad_economica smallint,
    clase_actividad smallint,
    clave_entidad_federativa smallint,
    clave_municipio varchar,
    localidad varchar,
    codigo_postal integer,
    giro varchar
);

--tabla catalogo_habilidades
DROP TABLE IF EXISTS clean.catalogo_habilidades;

CREATE TABLE clean.catalogo_habilidades(
    id_habilidad smallint,
    habilidad varchar
);


--tabla catalogo_area
DROP TABLE IF EXISTS clean.catalogo_area;

CREATE TABLE clean.catalogo_area(
    id_area smallint,
    descripcion varchar
);


--table candidato_conocimiento_habilidad_text
DROP TABLE IF EXISTS clean.candidato_conocimiento_habilidad_text;

CREATE TABLE clean.candidato_conocimiento_habilidad_text(
    id_bt integer,
    conocimiento_habilidad text,
    descripcion text
);

--table oferta_empleo_text
DROP TABLE IF EXISTS clean.oferta_empleo_text;

CREATE TABLE clean.oferta_empleo_text(
    id_oe integer,
    titulo_oe varchar,
    funciones_oe varchar,
    empresa_ofrece varchar,
    habilidad_general varchar,
    observaciones varchar,
    nombre_empresa varchar,
    cargo_contacto varchar
);

--table empresa_text
DROP TABLE IF EXISTS clean.empresa_text;

CREATE TABLE clean.empresa_text(
    id_empresa integer,
    razon_social varchar,
    descripcion varchar,
    nombre_comercial varchar
);

--table candidato_grado_academico_text
DROP TABLE IF EXISTS clean.candidato_grado_academico_text;

CREATE TABLE clean.candidato_grado_academico_text(
    id_candidato_grado_academico integer,
    escuela varchar,
    lugar varchar
);

--table candidato_otro_estudio_text
DROP TABLE IF EXISTS clean.candidato_otro_estudio_text;

CREATE TABLE clean.candidato_otro_estudio_text(
    id_candidato_otro_estudio integer,
    nombre_estudio varchar,
    institucion varchar,
    descripcion varchar
);

--table candidato_expectativa_laboral_clean
DROP TABLE IF EXISTS clean.candidato_expectativa_laboral_text;

CREATE TABLE clean.candidato_expectativa_laboral_text(
    id_candidato integer,
    puesto_deseado varchar,
    funciones varchar
);

--table oferta_candidato_text
DROP TABLE IF EXISTS clean.oferta_candidato_text;

CREATE TABLE clean.oferta_candidato_text(
    id_oferta_candidato integer,
    motivo_descontratacion varchar
);

--tabla patrones_solicitantes_svl
DROP TABLE IF EXISTS clean.patrones_solicitantes_svl_text;

CREATE TABLE clean.patrones_solicitantes_svl_text(
    clave_patron varchar,
    razon_social varchar,
    localidad varchar,
    giro varchar
);


--tabla solicitantes
DROP TABLE IF EXISTS clean.solicitantes;

CREATE TABLE clean.solicitantes(
    id_solicitante integer,
    id_candidato integer,
    id_empresa integer,
    fecha_registro timestamp with time zone,
    id_oficina integer,
    id_fuente integer,
    genero smallint,
    fecha_nacimiento timestamp with time zone,
    edad smallint,
    id_entidad_nacimiento smallint,
    fecha_ultima_modificacion timestamp with time zone,
    persona_ultima_modificacion integer,
    estatus smallint,
    validado_renapo smallint,
    id_motivo_no_validacion integer,
    curp_anonima varchar
);



--table ground truth
DROP TABLE IF EXISTS clean.ground_truth;

create table clean.ground_truth(
    id_candidato integer,
    id_oferta_empleo integer,
    label integer,
    compatibilidad numeric(5,2),
    id_empresa integer,
    rfc varchar,
    fecha_alta timestamp with time zone,
    fecha_imss timestamp with time zone
);


--table ground truth with fuiste contratado
DROP TABLE IF EXISTS clean.groud_truth_fuiste_contratado;

CREATE TABLE clean.ground_truth_fuiste_contratado(
    id_candidato integer,
    id_oferta_empleo integer,
    fecha_alta timestamp with time zone,
    label smallint
);


--table clean.candidato_idioma
DROP TABLE IF EXISTS clean.candidato_idiomma;

CREATE TABLE clean.candidato_idioma(
    id_candidato_idioma integer,
    id_candidato integer,
    id_idioma smallint,
    id_certificacion smallint,
    id_dominio smallint,
    fecha_alta timestamp with time zone,
    registro_principal smallint,
    id_dominio_habla smallint,
    id_dominio_escrito smallint,
    id_dominio_lectura smallint,
    examen_id smallint,
    examen_puntos bigint
);



--table candidato_conocimiento_habilidad with lemmatization
DROP TABLE IF EXISTS clean.candidato_conocimiento_habilidad_text_lemmatization;

CREATE TABLE clean.candidato_conocimiento_habilidad_text_lemmatization(
    id_bt integer,
    conocimientoherramienta_bt varchar
);

--table candidato_conocimiento_habilidad with stemming
DROP TABLE IF EXISTS clean.candidato_conocimiento_habilidad_text_stemming;

CREATE TABLE clean.candidato_conocimiento_habilidad_text_stemming(
    id_bt integer,
    conocimientoherramienta_bt varchar
);


--tabla candidato_habilidad
DROP TABLE IF EXISTS clean.candidato_habilidad;

CREATE TABLE clean.candidato_habilidad(
     id_candidato integer,
     id_habilidad smallint,
     habilidad varchar
);

