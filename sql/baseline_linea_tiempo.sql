insert into clean.ground_truth
with candidato_oferta_empresa as(
  select
    id_candidato,
    id_oferta_empleo,
    id_empresa,
    rfc,
    compatibilidad,
    a.fecha_alta
  from
    clean.oferta_candidato a
  left join
    clean.oferta_empleo b
  using(id_oferta_empleo)
  left join
    clean.empresa c
  using(id_empresa)
),

linea_tiempo as(
  select
    id_candidato,
    rfc,
    to_date(año::varchar||'-'||mes::varchar||'-'||'01', 'YYYY-MM-DD') as fecha_imss,
    evento
  from
    clean.linea_tiempo_solicitantes
  where evento = '3'
)

select
  id_candidato,
  id_oferta_empleo,
  case
       when fecha_imss between fecha_alta and fecha_alta + interval '3 months'
       then 1
       else 0
  end as label,
  compatibilidad,
  id_empresa,
  rfc,
  fecha_alta,
  fecha_imss
from
  candidato_oferta_empresa a
left join
  linea_tiempo b
using(id_candidato, rfc);


