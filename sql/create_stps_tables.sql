SET TIME ZONE 'America/Mexico_City';

set role sne_readwrite;

--create schema
DROP SCHEMA IF EXISTS  stps CASCADE;

CREATE SCHEMA stps;

---create tables
--table stps.resultado_ofertas_parabt
DROP TABLE IF exists stps.resultado_ofertas_parabt;

--table resultado_ofertas_parabt
CREATE TABLE stps.resultado_ofertas_parabt(
    id_tabla serial,
    idbt numeric(18,0),
    idoferta numeric(18,0),
    concepto varchar,
    fechacontrol date,
    PRIMARY KEY (idbt, idoferta, fechacontrol)
);

--table resultado_ofertas_parabt with fasttext and lemmatizaton
DROP TABLE IF exists stps.resultado_ofertas_parabt_fasttext_lemmatization;

CREATE TABLE stps.resultado_ofertas_parabt_fasttext_lemmatization(
    id_tabla serial,
    idbt numeric(18,0),
    idoferta numeric(18,0),
    concepto varchar,
    fechacontrol date,
    PRIMARY KEY (idbt, idoferta, fechacontrol)
);

--copy resultado_ofertas_parabt_copy
DROP TABLE IF exists stps.resultado_ofertas_parabt_copy;

CREATE TABLE stps.resultado_ofertas_parabt_copy(
    id_tabla serial,
    idbt numeric(18,0),
    idoferta numeric(18,0),
    grupo smallint,
    concepto varchar,
    fechacontrol date,
    PRIMARY KEY (idbt, idoferta, fechacontrol)
);

--copy of resultado_ofertas_parabt_fasttext_lemmatization
DROP TABLE IF exists stps.resultado_ofertas_parabt_fasttext_lemmatization_copy;

CREATE TABLE stps.resultado_ofertas_parabt_fasttext_lemmatization_copy(
    id_tabla serial,
    idbt numeric(18,0),
    idoferta numeric(18,0),
    concepto varchar,
    fechacontrol date,
    PRIMARY KEY (idbt, idoferta, fechacontrol)
);

--tabla resultado_candidatos_para_emp
DROP TABLE IF exists stps.resultado_candidatos_paraemp;

CREATE TABLE stps.resultado_candidatos_paraemp(
    id_tabla serial,
    idbt numeric(18,0),
    idoferta numeric(18,0),
    concepto varchar,
    fechacontrol date,
    PRIMARY KEY (idbt, idoferta, fechacontrol)
);

--copy of resultado_candidatos_paraemp
DROP TABLE IF exists stps.resultado_candidatos_paraemp_copy;

CREATE TABLE stps.resultado_candidatos_paraemp_copy(
    id_tabla serial,
    idbt numeric(18,0),
    idoferta numeric(18,0),
    grupo smallint,
    concepto varchar,
    fechacontrol date,
    PRIMARY KEY (idbt, idoferta, fechacontrol)
);

--tabla resultado_candidatos_paraemp with lemmatization and fasttext
DROP TABLE IF exists stps.resultado_candidatos_paraemp_fasttext_lemmatization;

CREATE TABLE stps.resultado_candidatos_paraemp_fasttext_lemmatization(
    id_tabla serial,
    idbt numeric(18,0),
    idoferta numeric(18,0),
    concepto varchar,
    fechacontrol date,
    PRIMARY KEY (idbt, idoferta, fechacontrol)
);

-- copy of tabla resultado_candidatos_paraemp_fasttext_lemmatization
DROP TABLE IF exists stps.resultado_candidatos_paraemp_fasttext_lemmatization_copy;

CREATE TABLE stps.resultado_candidatos_paraemp_fasttext_lemmatization_copy(
    id_tabla serial,
    idbt numeric(18,0),
    idoferta numeric(18,0),
    concepto varchar,
    fechacontrol date,
    PRIMARY KEY (idbt, idoferta, fechacontrol)
);

--table oferta_empleo with funciones oferta, titulo oferta and lemmatization
DROP TABLE IF exists stps.oferta_empleo_text_lemmatization;

CREATE TABLE stps.oferta_empleo_text_lemmatization(
    id_oe integer,
    titulo_oe varchar,
    funciones_oe varchar
);

--table oferta_empleo with funciones oferta, titulo oferta and stemming
DROP TABLE IF exists stps.oferta_empleo_text_stemming;

CREATE TABLE stps.oferta_empleo_text_stemming(
    id_oe integer,
    titulo_oe varchar,
    funciones_oe varchar
);
