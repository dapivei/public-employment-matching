set time zone  'America/Mexico_City';

set role sne_readwrite;

--schema raw
DROP SCHEMA IF EXISTS raw CASCADE;

CREATE SCHEMA raw;

--table candidato
DROP TABLE IF EXISTS raw.candidatos;

CREATE TABLE raw.candidatos(
    id_candidato varchar,
    id_usuario varchar,
    id_entidad_nacimiento varchar,
    id_estado_civil varchar,
    id_tipo_discapacidad varchar,
    id_medio_portal varchar,
    confidencialidad_datos varchar,
    veracidad_datos varchar,
    aceptacion_terminos varchar,
    fecha_alta varchar,
    estatus varchar,
    fecha_ultima_actualizacion varchar,
    estilo_cv varchar,
    evalua_cv varchar,
    aviso_oportuno varchar,
    nss varchar,
    credito_infonativ varchar,
    registro_bumeran varchar,
    fecha_confirma varchar,
    postulaciones_actuales varchar,
    fuente varchar,
    id_oficina varchar,
    id_motivo_desactivacion varchar,
    descapacidades varchar,
    fecha_ultimo_acceso varchar,
    apoyo_prospera varchar,
    ppc_estatus varchar,
    ppc_fecha_inscripcion varchar,
    ppc_aceptacion_terminos varchar,
    ppc_estatus_imss varchar,
    ppc_fecha_baja_imss varchar,
    ppc_tipo_contrato_imss varchar,
    ppc_idmotivo_fuera varchar,
    folio_prospera varchar,
    estatus_georeferencia varchar,
    folio_integrante_prospera varchar,
    consentimiento varchar
);

--table candidato_conocimiento_habilidad
DROP TABLE IF EXISTS raw.candidato_conocimiento_habilidad;

CREATE TABLE raw.candidato_conocimiento_habilidad(
  id_candidato_conocimiento_habilidad varchar,
  id_candidato varchar,
  id_tipo_conocimiento_habilidad varchar,
  conocimiento_habilidad varchar,
  id_experiencia varchar,
  id_dominio varchar,
  descripcion varchar,
  fecha_alta varchar,
  principal varchar
);

--table candidato_documentacion
DROP TABLE IF EXISTS raw.candidato_documentacion;

CREATE TABLE raw.candidato_documentacion(
    id_candidato varchar,
    id_documento varchar
);

--table candidato_evento
DROP TABLE IF EXISTS raw.candidato_evento;

CREATE TABLE raw.candidato_evento(
    id_candidato varchar,
    id_evneto varchar,
    folio_registro varchar,
    fecha_registro varchar,
    fuente varchar,
    id_usuario varchar
);

--table candidato_expectativa_laboral
DROP TABLE IF EXISTS raw.candidato_expectativa_laboral;

CREATE TABLE raw.candidato_expectativa_laboral(
    id_candidato varchar,
    id_sector_deseado varchar,
    puesto_deseado varchar,
    id_area_laboral_deseada varchar,
    id_ocupacion_deseada varchar,
    salario_pretendido varchar,
    id_tipo_empleo_deseado varchar,
    id_tipo_contrato varchar,
    fecha_alta varchar,
    id_expectativa_laboral varchar,
    principal varchar,
    id_experiencia varchar,
    id_ocupacion_noc varchar,
    id_sub_area_laboral_deseada varchar,
    funciones varchar
);


--CANDIDATO GRADO ACADEMICO
DROP TABLE IF EXISTS raw.candidato_grado_academico;

CREATE TABLE raw.candidato_grado_academico(
    id_candidato_grado_academico varchar,
    id_candidato varchar,
    id_nivel_estudio varchar,
    id_situacion_academica varchar,
    id_carrera_especialidad varchar,
    escuela varchar,
    inicio varchar,
    fin varchar,
    fecha_alta varchar,
    principal varchar,
    lugar varchar,
    fecha_inicio varchar,
    fecha_fin varchar,
    id_documento_aprobatorio varchar
);

--candidato_habilidad
DROP TABLE IF EXISTS raw.candidato_habilidad;

CREATE TABLE raw.candidato_habilidad(
    id_candidato varchar,
    id_habilidad varchar
);

--candidato idioma
DROP TABLE IF EXISTS raw.candidato_idioma;

CREATE TABLE raw.candidato_idioma(
    id_candidato_idioma varchar,
    id_candidato varchar,
    id_idioma varchar,
    id_certificacion varchar,
    id_dominio varchar,
    fecha_alta varchar,
    principal varchar,
    id_dominio_habla varchar,
    id_dominio_escrito varchar,
    id_dominio_lectura varchar,
    id_dominio_comprension varchar,
    examen_presentado varchar,
    examen_id varchar,
    examen_puntos varchar,
    examen_fecha varchar,
    nivel_clb varchar
);

--candidato_otro_estudio
DROP TABLE IF EXISTS raw.candidato_otro_estudio;

CREATE TABLE raw.candidato_otro_estudio(
    id_candidato_otro_estudio varchar,
    id_candidato varchar,
    id_otro_estudio varchar,
    id_tipo_estudio varchar,
    id_estatus_academico varchar,
    fecha_inicio varchar,
    fecha_fin varchar,
    nombre_estudio varchar,
    institucion varchar,
    principal varchar,
    pais varchar,
    id_documento_aprobatorio varchar,
    descripcion varchar
);

--catalogo de eventos
DROP TABLE IF EXISTS raw.catalogo_eventos;

CREATE TABLE raw.catalogo_eventos(
    id_evento varchar,
    evento varchar
);

--catalogo opciones
DROP TABLE IF EXISTS raw.catalogo_opciones;

CREATE TABLE raw.catalogo_opciones(
    id_catalogo_opcion varchar,
    id_catalogo varchar,
    opcion varchar,
    fecha_alta varchar,
    estatus varchar,
    fecha_modificacion varchar,
    id_catalogo_asociado varchar,
    id_corto varchar
);

--catalogo area
DROP TABLE IF EXISTS raw.catalogo_areas;

CREATE TABLE raw.catalogo_areas(
    id_area varchar,
    descripcion varchar
);

--catalogo area
DROP TABLE IF EXISTS raw.catalogo_subarea;

CREATE TABLE raw.catalogo_subarea(
    id_subarea varchar,
    descripcion varchar,
    id_area varchar,
    num_cat_area varchar
);

--codigo postal
DROP TABLE IF EXISTS raw.codigo_postal;

CREATE TABLE raw.codigo_postal(
    id_codigo_postal varchar,
    codigo_postal varchar,
    colonia varchar,
    tipo_asentamiento varchar,
    municipio varchar,
    entidad varchar,
    ciudad varchar,
    d_cp varchar,
    id_entidad varchar,
    id_oficina varchar,
    id_tipo_asentamiento varchar,
    id_municipio varchar,
    id_colonia varchar,
    zona varchar,
    id_ciudad varchar,
    c_cp varchar,
    id_asenta_cpcons varchar
);

--tabla empresa
DROP TABLE IF EXISTS raw.empresa;

CREATE TABLE raw.empresa(
    id_empresa varchar,
    rfc varchar,
    id_usuario varchar,
    id_tipo_persona varchar,
    razon_social varchar,
    fecha_acta varchar,
    descripcion varchar,
    id_tipo_empresa varchar,
    id_actividad_economica varchar,
    numero_empleados varchar,
    id_medio varchar,
    aceptacion_terminos varchar,
    fecha_alta varchar,
    estatus varchar,
    fecha_ultima_actualizacion varchar,
    asegura_datos varchar,
    nombre_comercial varchar,
    fecha_confirma varchar,
    id_oficina varchar,
    cargo_contacto varchar,
    id_tipo_sociedad varchar,
    ofertas_totales varchar,
    ofertas_vigentes varchar,
    ofertas_pendientes varchar,
    ofertas_eliminadas varchar,
    personas_colocadas varchar,
    id_motivo_desactivacion varchar,
    fuente varchar,
    id_motivo_no_validacion varchar,
    id_tamaño varchar,
    fecha_ultima_desactivacion varchar,
    detalle_desactivacion varchar,
    es_empresa_sne varchar,
    sucursal varchar,
    genero varchar,
    id_empleador_ext varchar,
    num_empleador_ext varchar,
    codigo_postal_ext varchar,
    id_provincia_ext varchar,
    id_ciudad_ext varchar,
    lineamientos_ext varchar,
    giro_empresa_ext varchar,
    id_pais_ext varchar,
    estatus_georef_dom varchar,
    estatus_georef_oferta varchar,
    mision varchar,
    id_rama varchar,
    id_sector varchar,
    id_subsector varchar,
    mensaje varchar,
    consentimiento varchar
);

--table eventos
DROP TABLE IF EXISTS raw.eventos;

CREATE TABLE raw.eventos(
    id_evento varchar,
    id_usuario varchar,
    evento varchar,
    fuente varchar,
    estatus varchar,
    fecha_alta varchar,
    fecha_modificacion varchar,
    sede varchar,
    participa_feria_nacional varchar,
    id_ambiente varchar,
    id_clasificacion varchar,
    id_tipo_evento varchar,
    otras_poblaciones varchar,
    id_actividad_economica varchar,
    otra_justificacion varchar,
    hora_atencion_inicio varchar,
    hora_atencion_fin varchar,
    estimado_empresas varchar,
    estimado_ofertas varchar,
    estimado_candidatos varchar,
    estimado_candidatos_colocados varchar,
    otras_actividades_comp varchar,
    id_tipo_recurso varchar,
    otro_tipo_recurso varchar,
    id_entidad varchar,
    id_municipio varchar,
    id_colonia varchar,
    codigo_postal varchar,
    localidad varchar,
    id_tipo_zona varchar,
    id_tipo_vialidad varchar,
    calle varchar,
    entre_calle varchar,
    y_calle varchar,
    numero_exterior varchar,
    numero_interior varchar,
    id_tipo_asentamiento varchar,
    asentamiento varchar,
    referencia_domicilio varchar,
    id_motivo_rechazo varchar,
    motivo_rechazo varchar,
    fecha_inicio varchar,
    fecha_fin varchar,
    id_accion_realizada varchar,
    visible varchar
);

--tabla linea_tiempo_solicitantes_svl
DROP TABLE IF EXISTS raw.linea_tiempo_solicitantes_svl;

CREATE TABLE raw.linea_tiempo_solicitantes_svl(
   id_solicitante varchar,
    id_candidato varchar,
    rfc varchar,
    nss varchar,
    registro varchar,
    mod varchar,
    tpt varchar,
    te varchar,
    cve_mun_final varchar,
    cve_ent_final varchar,
    sal_cierre varchar,
    cod_pos varchar,
    enero_2017 varchar,
    febrero_2017 varchar,
    marzo_2017 varchar,
    abril_2017 varchar,
    mayo_2017 varchar,
    junio_2017 varchar,
    julio_2017 varchar,
    agosto_2017 varchar,
    septiembre_2017 varchar,
    octubre_2017 varchar,
    noviembre_2017 varchar,
    diciembre_2017 varchar,
    enero_2018 varchar,
    febrero_2018 varchar,
    marzo_2018 varchar,
    abril_2018 varchar,
    mayo_2018 varchar,
    junio_2018 varchar,
    julio_2018 varchar,
    agosto_2018 varchar,
    septiembre_2018 varchar,
    octubre_2018 varchar,
    noviembre_2018 varchar,
    diciembre_2018 varchar,
    enero_2019 varchar,
    febrero_2019 varchar,
    marzo_2019 varchar,
    abril_2019 varchar,
    mayo_2019 varchar,
    junio_2019 varchar,
    julio_2019 varchar,
    agosto_2019 varchar,
    septiembre_2019 varchar,
    octubre_2019 varchar,
    noviembre_2019 varchar,
    diciembre_2019 varchar,
    enero_2020 varchar,
    febrero_2020 varchar,
    marzo_2020 varchar,
    abril_2020 varchar,
    mayo_2020 varchar,
    junio_2020 varchar,
    julio_2020 varchar,
    agosto_2020 varchar,
    septiembre_2020 varchar,
    octubre_2020 varchar,
    noviembre_2020 varchar,
    diciembre_2020 varchar,
    sal_enero_2017 varchar,
    sal_febrero_2017 varchar,
    sal_marzo_2017 varchar,
    sal_abril_2017 varchar,
    sal_mayo_2017 varchar,
    sal_junio_2017 varchar,
    sal_julio_2017 varchar,
    sal_agosto_2017 varchar,
    sal_octubre_2017 varchar,
    sal_septiembre_2017 varchar,
    sal_noviembre_2017 varchar,
    sal_diciembre_2017 varchar,
    sal_enero_2018 varchar,
    sal_febrero_2018 varchar,
    sal_marzo_2018 varchar,
    sal_abril_2018 varchar,
    sal_mayo_2018 varchar,
    sal_junio_2018 varchar,
    sal_julio_2018 varchar,
    sal_agosto_2018 varchar,
    sal_septiembre_2018 varchar,
    sal_octubre_2018 varchar,
    sal_noviembre_2018 varchar,
    sal_diciembre_2018 varchar,
    sal_enero_2019 varchar,
    sal_febrero_2019 varchar,
    sal_marzo_2019 varchar,
    sal_abril_2019 varchar,
    sal_mayo_2019 varchar,
    sal_junio_2019 varchar,
    sal_julio_2019 varchar,
    sal_agosto_2019 varchar,
    sal_septiembre_2019 varchar,
    sal_octubre_2019 varchar,
    sal_noviembre_2019 varchar,
    sal_diciembre_2019 varchar,
    sal_enero_2020 varchar,
    sal_febrero_2020 varchar,
    sal_marzo_2020 varchar,
    sal_abril_2020 varchar,
    sal_mayo_2020 varchar,
    sal_junio_2020 varchar,
    sal_julio_2020 varchar,
    sal_agosto_2020 varchar,
    sal_septiembre_2020 varchar,
    sal_octubre_2020 varchar,
    sal_noviembre_2020 varchar,
    sal_diciembre_2020 varchar,
    mod_enero_2017 varchar,
    mod_febrero_2017 varchar,
    mod_marzo_2017 varchar,
    mod_abril_2017 varchar,
    mod_mayo_2017 varchar,
    mod_junio_2017 varchar,
    mod_julio_2017 varchar,
    mod_agosto_2017 varchar,
    mod_octubre_2017 varchar,
    mod_septiembre_2017 varchar,
    mod_noviembre_2017 varchar,
    mod_diciembre_2017 varchar,
    mod_enero_2018 varchar,
    mod_febrero_2018 varchar,
    mod_marzo_2018 varchar,
    mod_abril_2018 varchar,
    mod_mayo_2018 varchar,
    mod_junio_2018 varchar,
    mod_julio_2018 varchar,
    mod_agosto_2018 varchar,
    mod_septiembre_2018 varchar,
    mod_octubre_2018 varchar,
    mod_noviembre_2018 varchar,
    mod_diciembre_2018 varchar,
    mod_enero_2019 varchar,
    mod_febrero_2019 varchar,
    mod_marzo_2019 varchar,
    mod_abril_2019 varchar,
    mod_mayo_2019 varchar,
    mod_junio_2019 varchar,
    mod_julio_2019 varchar,
    mod_agosto_2019 varchar,
    mod_septiembre_2019 varchar,
    mod_octubre_2019 varchar,
    mod_noviembre_2019 varchar,
    mod_diciembre_2019 varchar,
    mod_enero_2020 varchar,
    mod_febrero_2020 varchar,
    mod_marzo_2020 varchar,
    mod_abril_2020 varchar,
    mod_mayo_2020 varchar,
    mod_junio_2020 varchar,
    mod_julio_2020 varchar,
    mod_agosto_2020 varchar,
    mod_septiembre_2020 varchar,
    mod_octubre_2020 varchar,
    mod_noviembre_2020 varchar,
    mod_diciembre_2020 varchar
);

--tabla localidad_inegi
DROP TABLE IF EXISTS raw.localidad_inegi;

CREATE TABLE raw.localidad_inegi(
    cloc_id varchar,
    cloc_nombre varchar,
    cmun_id varchar
);

--municipio
DROP TABLE IF EXISTS raw.municipio;

CREATE TABLE raw.municipio(
    id_entidad varchar,
    id_municipio varchar,
    municipio varchar,
    identificador varchar,
    cmun_id varchar
);


--table oferta_candidato
DROP IF EXISTS raw.oferta_candidato;

CREATE TABLE raw.oferta_candidato(
    id_oferta_empleo varchar,
    id_candidato varchar,
    fecha_alta varchar,
    estatus varchar,
    id_motivo varchar,
    id_oferta_candidato varchar,
    compatibilidad varchar,
    id_oficina varchar,
    id_usuario varchar,
    postulacion_cartera varchar,
    fecha_seguimiento varchar,
    fecha_colocacion varchar,
    fecha_inicio_contrata varchar,
    fuente varchar,
    motivo_desc varchar,
    fecha_modificacion varchar,
    fecha_contacto varchar,
    consiguio_entrevista varchar,
    fecha_entrevista varchar,
    motivo_no_entrevista varchar,
    fuiste_contratado varchar,
    id_oficina_seguimiento varchar,
    id_usuario_seguimiento varchar,
    fuente_seguimiento varchar,
    compatibilidad_ocupacion varchar,
    compatibilidad_educacion varchar,
    compatibilidad_habilidades varchar,
    compatibilidad_experiencia varchar,
    compatibilidad_experiencia varchar,
    compatibilidad_idioma varchar,
    compatibilidad_salario varchar,
    compatibilidad_edad varchar,
    compatibilidad_horario varchar,
    compatibilidad_viajar varchar,
    compatibilidad_radicar_fuera varchar,
    compatibilidad_modalidad varchar,
    preselect_en varchar
);

--tabla oficina
DROP TABLE IF EXISTS raw.oficina;

CREATE TABLE raw.oficina(
   id_oficina varchar,
   clave_oficina varchar,
   descripcion varchar,
   estatus varchar,
   fecha_alta varchar,
   fecha_modificacion varchar,
   id_entidad varchar,
   id_municipio varchar,
   unioperativa_oficina varchar,
   id_motivo_desactivacion varchar,
   motivo_desactivacion varchar,
   tipo_agenda varchar,
   url_localizacion varchar,
   oficina_reconocida varchar
);


--tabla patrones_solicitantes_svl
DROP TABLE IF EXISTS raw.patrones_solicitantes_svl;

CREATE TABLE raw.patrones_solicitanes_svl(
    cve_patron varchar,
    razon varchar,
    cv_modalidad varchar,
    ta varchar,
    tp varchar,
    teu varchar,
    tec varchar,
    div_final varchar,
    gpo_final varchar,
    act_final varchar,
    clase varchar,
    cve_ent_final varchar,
    cve_mun_final varchar,
    localidad varchar,
    cod_pos varchar,
    giro varchar
);

--tabla solicitantes
DROP TABLE IF EXISTS raw.solicitantes;

CREATE TABLE raw.solicitantes(
    id_solicitante varchar,
    id_candidato varchar,
    id_empresa varchar,
    fecha_registro varchar,
    id_oficina varchar,
    id_fuente varchar,
    genero varchar,
    fecha_nacimiento varchar,
    edad varchar,
    id_entidad_nacimiento varchar,
    fecha_ultima_modificacion varchar,
    persona_ultima_modificacion varchar,
    estatus varchar,
    validado_renapo varchar,
    id_motivo_no_validacion varchar,
    es_fraudulento varchar,
    id_usuario_registro varchar,
    curp_anonima varchar
);

--tabla usuarios
DROP TABLE IF EXISTS raw.usuarios;

CREATE TABLE raw.usuarios(
    id_candidato varchar,
    id_usuario varchar,
    id_entidad_nacimiento varchar,
    id_estado_civil varchar,
    id_tipo_discapacidad varchar,
    id_medio_portal varchar,
    confidencialidad_datos varchar,
    veracidad_datos varchar,
    aceptacion_terminos varchar,
    fecha_alta varchar,
    estatus varchar,
    fecha_ultima_actualizacion varchar,
    estilo_cv varchar,
    evalua_cv varchar,
    aviso_oportuno varchar,
    nss varchar,
    credito_infonavit varchar,
    registro_bumeran varchar,
    fecha_confirma varchar,
    postulaciones_actuales varchar,
    fuente varchar,
    id_oficina varchar,
    id_motivo_desactivacion varchar,
    discapacidades varchar,
    fecha_ultimo_acceso varchar,
    apoyo_prospera varchar,
    ppc_estatus varchar,
    ppc_fecha_inscripcion varchar,
    ppc_aceptacion_terminos varchar,
    ppc_estatus_imss varchar,
    ppc_fecha_baja_imss varchar,
    ppc_tipo_contrato_imss varchar,
    ppc_idmotivo_fuera varchar,
    folio_prospera varchar,
    estatus_georeferencia varchar,
    folio_integrante_prospera varchar,
    consentimiento varchar
);

--table act_division
DROP TABLE IF EXISTS raw.act_division;

CREATE TABLE raw.act_division(
    id_division varchar,
    cve_division varchar,
    descripcion_division varchar
);

--table act_gpo
DROP TABLE IF EXISTS raw.act_gpo;

CREATE TABLE raw.act_gpo(
    id_gpo varchar,
    cve_division varchar,
    cve_gpo varchar,
    gpo_descripcion varchar
);

--table catalogo_modalidad (c_modalidad)
DROP TABLE IF EXISTS raw.catalogo_modalidad;

CREATE TABLE raw.catalogo_modalidad(
    id_mod varchar,
    descripcion_modalidad varchar
);

--table catalogo_actividad
DROP TABLE IF EXISTS raw.catalogo_actividad;

CREATE TABLE raw.catalogo_actividad(
    id_actividad varchar,
    cve_actividad varchar,
    cve_division varchar,
    cve_gpo_actividad varchar,
    cve_actividad_economica varchar,
    descripcion_actividad_economica varchar
);

--table entidad_municipio_imss
DROP TABLE IF EXISTS raw.entidad_municipio_imss;

CREATE TABLE raw.entidad_municipio_imss(
    cve_municipio varchar,
    cve_delegacion varchar,
    cve_entidad varchar,
    descripcion_entidad varchar,
    descripcion_municipio varchar
);

--table candidato_otro_lenguaje
DROP TABLE IF EXISTS raw.candidato_otro_lenguaje;

CREATE TABLE raw.candidato_otro_lenguaje(
    id_candidato_otro_lenguaje varchar,
    id_candidato varchar,
    id_lenguaje varchar
);

--table oferta_empleo
DROP TABLE IF EXISTS raw.oferta_empleo;

CREATE TABLE raw.oferta_empleo(
    id_oferta_empleo varchar,
    id_empresa varchar,
    titulo_oferta varchar,
    id_area_laboral varchar,
    id_ocupacion varchar,
    funciones varchar,
    id_tipo_empleo varchar,
    dias_laborales varchar,
    hora_entrada varchar,
    hora_salida varchar,
    rolar_turno varchar,
    empresa_ofrece varchar,
    salario varchar,
    id_tipo_contrato varchar,
    id_jerarquia varchar,
    numero_plazas varchar,
    limite_postulantes_plaza varchar,
    id_discapacidad varchar,
    id_causa_vacante varchar,
    fecha_inicio varchar,
    fecha_fin varchar,
    disponibilidad_viajar varchar,
    disponibilidad_radicar varchar,
    id_nivel_estudio varchar,
    id_situacion_academica varchar,
    habilidad_general varchar,
    experiencia_anios varchar,
    edad_requisito varchar,
    edad_minima varchar,
    edad_maxima varchar,
    genero varchar,
    mapa_ubicacion varchar,
    observaciones varchar,
    id_horario_de varchar,
    id_horario_a varchar,
    id_duracion_aproximada varchar,
    dias_entrevista varchar,
    fuente varchar,
    fecha_alta varchar,
    contacto_tel varchar,
    contacto_correo varchar,
    estatus varchar,
    fecha_modificacion varchar,
    nombre_empresa varchar,
    id_actividad_economica varchar,
    cargo_contacto varchar,
    contacto_domicilio varchar,
    id_vigencia_oferta varchar,
    id_oficina_registro varchar,
    id_usuario varchar,
    plazas_cubiertas varchar,
    oferta_capacitacion varchar,
    id_oficina_capacitacion varchar,
    publicar_oferta varchar,
    id_prestacion varchar,
    id_motivo_eliminacion varchar,
    discapacidades varchar,
    codigo_universal_de_puesto_sfp varchar,
    plazas_disponibles varchar,
    pertenece_pet varchar,
    nombre_programa_proyecto varchar,
    plazas_canceladas varchar,
    url_video varchar,
    tipo_oferta varchar,
    tipo_oferta_modalidad varchar,
    id_subarea varchar,
    rolar_dias_laborales varchar,
    rolar_hora_entrada varchar,
    rolar_hora_salida varchar,
    path_imagen varchar,
    id_area varchar,
    id_perfil_tipo varchar,
    medio_contacto varchar
);

--table catalogo_oficinas
DROP TABLE IF EXISTS raw.catalogo_oficinas;

CREATE TABLE raw.catalogo_oficinas(
    id_oficina varchar,
    clave_oficina varchar,
    descripcion varchar,
    estatus varchar,
    fecha_alta varchar,
    fecha_modificacion varchar,
    id_entidad varchar,
    id_municipio varchar,
    unioperativa_oficina varchar,
    id_motivo_desactivacion varchar,
    motivo_desactivacion varchar,
    tipo_agenda varchar,
    oficina_reconocida varchar
);

--tabla catalogo_tipo_conocimiento_habilidad
DROP TABLE IF EXISTS raw.catalogo_tipo_conocimiento_habilidad;

CREATE TABLE raw.catalogo_tipo_conocimiento_habilidad(
    id varchar,
    descripcion varchar
);

--tabla catalogo_tipo_oferta
DROP TABLE IF EXISTS raw.catalogo_tipo_oferta;

CREATE TABLE raw.catalogo_tipo_oferta(
    id varchar,
    descripcion varchar
);


--tabla catalogo_tipo_oferta_modalidad
DROP TABLE IF EXISTS raw.catalogo_tipo_oferta_modalidad;

CREATE TABLE raw.catalogo_tipo_oferta_modalidad(
    id varchar,
    descripcion varchar
);


--tabla catalogo_edad_requisito
DROP TABLE IF EXISTS raw.catalogo_edad_requisito;

CREATE TABLE raw.catalogo_edad_requisito(
    id varchar,
    descripcion varchar
);
