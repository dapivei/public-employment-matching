--tabla candidatos
insert into clean.candidatos
select
    cast(id_candidato as integer),
    cast(id_usuario as integer),
    cast(id_entidad_nacimiento as smallint),
    cast(id_estado_civil as smallint),
    cast(id_tipo_discapacidad as smallint),
    cast(id_medio_portal as smallint),
    cast(confidencialidad_datos as smallint),
    cast(veracidad_datos as smallint),
    cast(aceptacion_terminos as smallint),
    cast(fecha_alta as timestamp without time zone),
    cast(estatus as smallint),
    cast(fecha_ultima_actualizacion as timestamp without time zone),
    cast(estilo_cv as smallint),
    cast(evalua_cv as smallint),
    cast(aviso_oportuno as smallint),
    nss,
    credito_infonavit,
    cast(registro_bumeran as smallint),
    cast(fecha_confirma as timestamp without time zone),
    cast(postulaciones_actuales as smallint),
    cast(fuente as smallint),
    cast(btrim(id_oficina) as smallint),
    id_motivo_desactivacion,
    discapacidades,
    cast(fecha_ultimo_acceso as timestamp without time zone),
    cast(apoyo_prospera as smallint),
    ppc_estatus,
    ppc_fecha_inscripcion,
    ppc_aceptacion_terminos,
    ppc_estatus_imss,
    ppc_fecha_baja_imss,
    ppc_tipo_contrato_imss,
    ppc_idmotivo_fuera,
    folio_prospera,
    cast(estatus_georeferencia as smallint),
    folio_integrante_prospera,
    consentimiento
from raw.candidatos;

--tabla candidato_conocimiento_habilidad
insert into clean.candidato_conocimiento_habilidad
select
    cast(id_candidato_conocimiento_habilidad as integer),
    cast(id_candidato as integer),
    cast(id_tipo_conocimiento_habilidad as smallint),
    conocimiento_habilidad,
    cast(id_experiencia as smallint),
    cast(id_dominio as smallint),
    cast(descripcion as text),
    cast(fecha_alta as timestamp with time zone),
    principal
from raw.candidato_conocimiento_habilidad;

--tabla candidato_grado_academico
insert into clean.candidato_grado_academico
select
     cast(id_candidato_grado_academico as integer),
     cast(id_candidato as integer),
     cast(id_nivel_estudio as smallint),
     cast(id_situacion_academica as smallint),
     cast(id_carrera_especialidad as integer),
     cast(escuela as varchar),
     cast(inicio as smallint),
     cast(fin as smallint),
     cast(fecha_alta as timestamp with time zone),
     cast(principal as smallint),
     cast(lugar as varchar),
     cast(fecha_inicio as timestamp with time zone),
     cast(fecha_fin as timestamp with time zone),
     cast(id_documento_aprobatorio as smallint)
from raw.candidato_grado_academico;


--tabla candidato_expectativa_laboral
insert into clean.candidato_expectativa_laboral
select
  cast(id_candidato as integer),
  cast(id_sector_deseado as integer),
  puesto_deseado,
  cast(id_area_laboral_deseada as smallint),
  cast(id_ocupacion_deseada as integer),
  cast(salario_pretendido as numeric(12,4)),
  cast(id_tipo_empleo_deseado as smallint),
  cast(id_tipo_contrato as smallint),
  cast(fecha_alta as timestamp with time zone),
  cast(id_expectativa_laboral as integer),
  cast(principal as smallint),
  cast(id_experiencia as smallint),
  cast(id_ocupacion_noc as smallint),
  cast(id_sub_area_laboral_deseada as integer),
  funciones
from raw.candidato_expectativa_laboral;


--tabla linea_tiempo_solicitantes_svg
insert into clean.linea_tiempo_solicitantes
with eventos as(
select
  cast(id_solicitante as integer),
  cast(id_candidato as integer),
  rfc,
  nss,
  registro,
  cast(mod as smallint) as modalidad_contrato,
  case
    when tpt = '1' then 'permanente'
    when te = '1' then 'eventual'
  end tipo_trabajador,
  cve_mun_final as clave_municipio,
  cve_ent_final as clave_entidad,
  cast(sal_cierre as numeric(12,4)) as salario,
  cod_pos as codigo_postal,
  b.año,
  b.mes,
  b.evento
  from
    raw.linea_tiempo_solicitantes_svl a,
  lateral (
    values
        (2017, 1, a.enero_2017),
        (2017, 2, a.febrero_2017),
        (2017, 3, a.marzo_2017),
        (2017, 4, a.abril_2017),
        (2017, 5, a.mayo_2017),
        (2017, 6, a.junio_2017),
        (2017, 7, a.julio_2017),
        (2017, 8, a.agosto_2017),
        (2017, 9, a.septiembre_2017),
        (2017, 10, a.octubre_2017),
        (2017, 11, a.noviembre_2017),
        (2017, 12, a.diciembre_2017),
        (2018, 1, a.enero_2018),
        (2018, 2, a.febrero_2018),
        (2018, 3, a.marzo_2018),
        (2018, 4, a.abril_2018),
        (2018, 5, a.mayo_2018),
        (2018, 6, a.junio_2018),
        (2018, 7, a.julio_2018),
        (2018, 8, a.agosto_2018),
        (2018, 9, a.septiembre_2018),
        (2018, 10, a.octubre_2018),
        (2018, 11, a.noviembre_2018),
        (2018, 12, a.diciembre_2018),
        (2019, 1, a.enero_2019),
        (2019, 2, a.febrero_2019),
        (2019, 3, a.marzo_2019),
        (2019, 4, a.abril_2019),
        (2019, 5, a.mayo_2019),
        (2019, 6, a.junio_2019),
        (2019, 7, a.julio_2019),
        (2019, 8, a.agosto_2019),
        (2019, 9, a.septiembre_2019),
        (2019, 10, a.octubre_2019),
        (2019, 11, a.noviembre_2019),
        (2019, 12, a.diciembre_2019),
        (2020, 1, a.enero_2020),
        (2020, 2, a.febrero_2020),
        (2020, 3, a.marzo_2020),
        (2020, 4, a.abril_2020),
        (2020, 5, a.mayo_2020),
        (2020, 6, a.junio_2020),
        (2020, 7, a.julio_2020),
        (2020, 8, a.agosto_2020),
        (2020, 9, a.septiembre_2020),
        (2020, 10, a.octubre_2020),
        (2020, 11, a.noviembre_2020),
        (2020, 12, a.diciembre_2020)
  ) b (año, mes, evento)
  where evento in('1','3','4','5','6')
),

salario_mes as(
select
    cast(id_solicitante as integer),
    cast(id_candidato as integer),
    rfc,
    nss,
    registro,
    b.año,
    b.mes,
    cast(b.salario_mes as numeric(12,4))
from
    raw.linea_tiempo_solicitantes_svl a,
    lateral (
        values
        (2017, 1, a.sal_enero_2017),
        (2017, 2, a.sal_febrero_2017),
        (2017, 3, a.sal_marzo_2017),
        (2017, 4, a.sal_abril_2017),
        (2017, 5, a.sal_mayo_2017),
        (2017, 6, a.sal_junio_2017),
        (2017, 7, a.sal_julio_2017),
        (2017, 8, a.sal_agosto_2017),
        (2017, 9, a.sal_septiembre_2017),
        (2017, 10, a.sal_octubre_2017),
        (2017, 11, a.sal_noviembre_2017),
        (2017, 12, a.sal_diciembre_2017),
        (2018, 1, a.sal_enero_2018),
        (2018, 2, a.sal_febrero_2018),
        (2018, 3, a.sal_marzo_2018),
        (2018, 4, a.sal_abril_2018),
        (2018, 5, a.sal_mayo_2018),
        (2018, 6, a.sal_junio_2018),
        (2018, 7, a.sal_julio_2018),
        (2018, 8, a.sal_agosto_2018),
        (2018, 9, a.sal_septiembre_2018),
        (2018, 10, a.sal_octubre_2018),
        (2018, 11, a.sal_noviembre_2018),
        (2018, 12, a.sal_diciembre_2018),
        (2019, 1, a.sal_enero_2019),
        (2019, 2, a.sal_febrero_2019),
        (2019, 3, a.sal_marzo_2019),
        (2019, 4, a.sal_abril_2019),
        (2019, 5, a.sal_mayo_2019),
        (2019, 6, a.sal_junio_2019),
        (2019, 7, a.sal_julio_2019),
        (2019, 8, a.sal_agosto_2019),
        (2019, 9, a.sal_septiembre_2019),
        (2019, 10, a.sal_octubre_2019),
        (2019, 11, a.sal_noviembre_2019),
        (2019, 12, a.sal_diciembre_2019),
        (2020, 1, a.sal_enero_2020),
        (2020, 2, a.sal_febrero_2020),
        (2020, 3, a.sal_marzo_2020),
        (2020, 4, a.sal_abril_2020),
        (2020, 5, a.sal_mayo_2020),
        (2020, 6, a.sal_junio_2020),
        (2020, 7, a.sal_julio_2020),
        (2020, 8, a.sal_agosto_2020),
        (2020, 9, a.sal_septiembre_2020),
        (2020, 10, a.sal_octubre_2020),
        (2020, 11, a.sal_noviembre_2020),
        (2020, 12, a.sal_diciembre_2020)
    ) b (año, mes, salario_mes)
    where salario_mes != ''
),

modalidad_mes as(
select
    cast(id_solicitante as integer),
    cast(id_candidato as integer),
    rfc,
    nss,
    registro,
    b.año,
    b.mes,
    cast(b.modalidad_mes as smallint)
from
    raw.linea_tiempo_solicitantes_svl a,
    lateral (
        values
        (2017, 1, a.mod_enero_2017),
        (2017, 2, a.mod_febrero_2017),
        (2017, 3, a.mod_marzo_2017),
        (2017, 4, a.mod_abril_2017),
        (2017, 5, a.mod_mayo_2017),
        (2017, 6, a.mod_junio_2017),
        (2017, 7, a.mod_julio_2017),
        (2017, 8, a.mod_agosto_2017),
        (2017, 9, a.mod_septiembre_2017),
        (2017, 10, a.mod_octubre_2017),
        (2017, 11, a.mod_noviembre_2017),
        (2017, 12, a.mod_diciembre_2017),
        (2018, 1, a.mod_enero_2018),
        (2018, 2, a.mod_febrero_2018),
        (2018, 3, a.mod_marzo_2018),
        (2018, 4, a.mod_abril_2018),
        (2018, 5, a.mod_mayo_2018),
        (2018, 6, a.mod_junio_2018),
        (2018, 7, a.mod_julio_2018),
        (2018, 8, a.mod_agosto_2018),
        (2018, 9, a.mod_septiembre_2018),
        (2018, 10, a.mod_octubre_2018),
        (2018, 11, a.mod_noviembre_2018),
        (2018, 12, a.mod_diciembre_2018),
        (2019, 1, a.mod_enero_2019),
        (2019, 2, a.mod_febrero_2019),
        (2019, 3, a.mod_marzo_2019),
        (2019, 4, a.mod_abril_2019),
        (2019, 5, a.mod_mayo_2019),
        (2019, 6, a.mod_junio_2019),
        (2019, 7, a.mod_julio_2019),
        (2019, 8, a.mod_agosto_2019),
        (2019, 9, a.mod_septiembre_2019),
        (2019, 10, a.mod_octubre_2019),
        (2019, 11, a.mod_noviembre_2019),
        (2019, 12, a.mod_diciembre_2019),
        (2020, 1, a.mod_enero_2020),
        (2020, 2, a.mod_febrero_2020),
        (2020, 3, a.mod_marzo_2020),
        (2020, 4, a.mod_abril_2020),
        (2020, 5, a.mod_mayo_2020),
        (2020, 6, a.mod_junio_2020),
        (2020, 7, a.mod_julio_2020),
        (2020, 8, a.mod_agosto_2020),
        (2020, 9, a.mod_septiembre_2020),
        (2020, 10, a.mod_octubre_2020),
        (2020, 11, a.mod_noviembre_2020),
        (2020, 12, a.mod_diciembre_2020)
    ) b (año, mes, modalidad_mes)
    where modalidad_mes != ''
)

select
    id_solicitante,
    id_candidato,
    rfc,
    nss,
    registro,
    modalidad_contrato,
    tipo_trabajador,
    clave_municipio,
    clave_entidad,
    salario,
    codigo_postal,
    año,
    mes,
    evento,
    salario_mes,
    modalidad_mes
from
    eventos
join
    salario_mes using(id_solicitante, id_candidato, rfc, nss, registro, año, mes)
join
    modalidad_mes using(id_solicitante, id_candidato, rfc, nss, registro, año, mes);


--tabla candidato_conocimiento_habilidad
insert into clean.candidato_conocimiento_habilidad
select
    cast(id_candidato_conocimiento_habilidad as integer),
    cast(id_candidato as integer),
    cast(id_tipo_conocimiento_habilidad as smallint),
    conocimiento_habilidad,
    cast(id_experiencia as smallint),
    cast(id_dominio as smallint),
    cast(descripcion as text),
    cast(fecha_alta as timestamp with time zone),
    principal
from raw.candidato_conocimiento_habilidad;

--tabla empresa
insert into clean.empresas
select
    cast(cast(id_empresa as numeric) as integer),
    rfc,
    cast(id_usuario as integer),
    case
    	 when btrim(id_tipo_persona) = '' then NULL
    	 when btrim(id_tipo_persona) = '1' then 'física'
    	 when btrim(id_tipo_persona) = '2' then 'moral'
         else 'desconocido'
    end tipo_persona,
    razon_social,
    case
         when btrim(fecha_acta) =  ''  then NULL
         else cast(fecha_acta as timestamp with time zone)
    end fecha_acta,
    descripcion,
    case
        when id_tipo_empresa = '1' then 'publica'
        when id_tipo_empresa = '2' then 'privada'
        when id_tipo_empresa = '3' then 'organismo no gubernamental'
        when btrim(id_tipo_empresa) = '' then 'no declarado'
        else 'desconocido'
    end tipo_empresa ,
    cast(id_actividad_economica  as smallint),
    cast(numero_empleados as integer),
    case
         when btrim(id_medio) = '' then NULL
         when btrim(id_medio) = '1' then 'anuncio en radio'
         when btrim(id_medio) = '2' then 'anuncio en televisión'
         when btrim(id_medio) = '3' then 'cartel'
         when btrim(id_medio) = '4' then 'tríptico o díptico'
         when btrim(id_medio) = '5' then 'periódico de ofertas de empleo'
         when btrim(id_medio) = '6' then 'familiares o amigos'
         when btrim(id_medio) = '7' then 'buscador en la web'
         when btrim(id_medio) = '9' then 'anuncio en revista'
         when btrim(id_medio) = '10' then 'banner'
         when btrim(id_medio) = '11' then 'link en la web'
         when btrim(id_medio) = '12' then 'kioscos cibernéticos'
         when btrim(id_medio) = '13' then 'nota de prensa'
         when btrim(id_medio) = '14' then 'tarjetas telefónicas'
         when btrim(id_medio) = '15' then 'feria de empleo'
         when btrim(id_medio) = '16' then 'servicio nacional de empleo'
         when btrim(id_medio) = '17' then 'instituto mexicano de la juventud'
         when btrim(id_medio) = '18' then 'instituto del fondo nacional de la vivienda para los trabajadores'
         when btrim(id_medio) = '19' then 'portal del empleo'
         when btrim(id_medio) = '20' then 'redes sociales'
         else 'desconocido'
    end medio,
    case
         when btrim(aceptacion_terminos) =  ''  then NULL
         else cast(aceptacion_terminos as smallint)
    end aceptacion_terminos,
    cast(fecha_alta as date),
    cast(estatus as smallint),
    cast(fecha_ultima_actualizacion as timestamp with time zone),
    case
         when btrim(asegura_datos) =  ''  then NULL
         else cast(asegura_datos as smallint)
    end asegura_datos,
    nombre_comercial,
    case
        when btrim(fecha_confirma) = '' then NULL
        else cast(fecha_confirma  as timestamp with time zone)
    end fecha_confirma,
    id_oficina,
    cargo_contacto,
    case
         when btrim(id_tipo_sociedad) =  '' then NULL
         when btrim(id_tipo_sociedad) = '1' then 'sociedad anónima (s.a.)'
         when btrim(id_tipo_sociedad) = '2' then 'sociedad civil (s.c.)'
         when btrim(id_tipo_sociedad) = '3' then 'asociación civil (a.c.)'
         when btrim(id_tipo_sociedad) = '4' then 'sociedad en nombre colectivo'
         when btrim(id_tipo_sociedad) = '5' then 'comandita simple s. en c.'
         when btrim(id_tipo_sociedad) = '6' then 'comandita por acciones s. en c. por a.'
         when btrim(id_tipo_sociedad) = '7' then 'sociedad de responsabilidad limitada (s. de r.l.)'
         when btrim(id_tipo_sociedad) = '8' then 'sociedad cooperativa limitada (s.c.l.)'
         when btrim(id_tipo_sociedad) = '9' then 'sociedad cooperativa suplementada (s.c.s.)'
         when btrim(id_tipo_sociedad) = '10' then 'asociación en participación (a.p.)'
         when btrim(id_tipo_sociedad) = '11' then 'sociedad mutualista de seguros de vida o daños'
         when btrim(id_tipo_sociedad) = '12' then 'sociedad de responsabilidad limitada de interés público (s. de r.l
         . de i.p.)'
         when btrim(id_tipo_sociedad) = '13' then 'sociedad nacional de crédito y/o institución de banca de
         desarrollo (s.n.c.)'
         when btrim(id_tipo_sociedad) = '14' then 'institución de banca múltiple'
         when btrim(id_tipo_sociedad) = '15' then 'sociedad de inversión común (s.i.c.)'
         when btrim(id_tipo_sociedad) = '16' then 'sociedad de inversión en instrumentos de deuda (s.i.i.d.)'
         when btrim(id_tipo_sociedad) = '17' then 'sociedad de inversión en capitales de riesgo (s.i.c.r.)'
         when btrim(id_tipo_sociedad) = '18' then 'agrupaciones financieras (a.f.)'
         when btrim(id_tipo_sociedad) = '19' then 'sociedad financiera de objetivo limitado (sofol)'
         when btrim(id_tipo_sociedad) = '20' then 'administradoras de fondos para el retiro (afore)'
         when btrim(id_tipo_sociedad) = '21' then 'sociedad de inversión especializada de fondos para el retiro
         (siefore)'
         when btrim(id_tipo_sociedad) = '22' then 'sociedad colectiva'
         when btrim(id_tipo_sociedad) = '23' then 'sociedad de responsabilidad limitada microindustrial (s. de r.l. mi)'
         when btrim(id_tipo_sociedad) = '24' then 'sociedad de solidaridad social (s. de s.s.)'
         when btrim(id_tipo_sociedad) = '25' then 'sociedad anónima de capital variable (s.a. de c.v.)'
         else 'desconocido'
    end tipo_sociedad,
    cast(ofertas_totales as integer),
    cast(ofertas_vigentes  as integer),
    cast(ofertas_pendientes as integer),
    cast(ofertas_eliminadas as integer),
    cast(personas_colocadas as integer),
    cast(fuente as smallint),
    case
        when id_tamaño = '1' then 'micro'
        when id_tamaño = '2' then 'pequeña'
        when id_tamaño = '3' then 'mediana'
        when id_tamaño = '4' then 'grande'
        else 'no determinado'
    end tamaño,
    case
    	 when btrim(es_empresa_sne) =  ''  then NULL
         else cast(es_empresa_sne as smallint)
    end es_empresa_sne,
    case
         when btrim(estatus_georef_dom) =  ''  then NULL
         else cast(estatus_georef_dom as smallint)
    end estatus_georef_dom,
    case
         when btrim(estatus_georef_oferta) =  ''  then NULL
         else cast(estatus_georef_oferta as smallint)
    end estatus_georef_oferta

from raw.empresa;


--tabla candidato_otro_estudio
insert into clean.candidato_otro_estudio
select
    cast(id_candidato_otro_estudio as integer),
    cast(id_candidato as integer),
    cast(id_otro_estudio as smallint),
    cast(id_tipo_estudio as smallint),
    cast(id_estatus_academico as smallint),
    cast(fecha_inicio as timestamp with time zone),
    cast(fecha_fin as timestamp with time zone),
    nombre_estudio,
    institucion,
    case
    	when btrim(principal) = '' then NULL
    	else cast(principal as smallint)
    end principal,
    pais,
    case
    	when btrim(id_documento_aprobatorio) = '' then NULL
    	else cast(id_documento_aprobatorio as smallint)
    end id_documento_aprobatorio,
    descripcion
from raw.candidato_otro_estudio;

-- tabla oferta_candidato
insert into clean.oferta_candidato
with catalogo_estatus as(
    select
        id_catalogo_opcion,
        opcion as estatus
    from
        raw.catalogo_opciones
     where
        id_catalogo = '1'
),

catalogo_fuiste_contratado as(
    select
        id_catalogo_opcion,
        opcion as fuiste_contratado
    from
        raw.catalogo_opciones
    where
        id_catalogo = '117'
),

catalogo_fuente as(
    select
        id_catalogo_opcion,
        opcion as fuente
    from
        raw.catalogo_opciones
    where
        id_catalogo = '65'
),

cast_oferta_empleo as(
    select
        cast(id_oferta_empleo as integer), --no nulo
        cast(id_candidato as integer), --no nulo
        cast(fecha_alta as timestamp with time zone), --no nulo
        case
            when btrim(a.estatus) = '' then 'desconocido'
            else b.estatus
        end estatus,
        case
            when btrim(id_motivo) = '' then NULL
            else cast(id_motivo  as smallint)
        end id_motivo,
        cast(id_oferta_candidato as integer),  --no nulo
        case
            when btrim(compatibilidad) = '' then NULL
            else cast(compatibilidad  as smallint)
        end compatibilidad,
        case
            when btrim(id_oficina) = '' then NULL
            else cast(id_oficina  as smallint)
        end id_oficina,
        cast(id_usuario as integer), --no nulo
        case
            when btrim(postulacion_cartera) = '' then NULL
            else cast(postulacion_cartera  as smallint)
        end postulacion_cartera,
         case
            when btrim(fecha_seguimiento) = '' then NULL
            else cast(fecha_seguimiento  as timestamp with time zone)
        end fecha_seguimiento,
        case
            when btrim(fecha_colocacion) = '' then NULL
            else cast(fecha_colocacion  as timestamp with time zone)
        end fecha_colocacion,
        case
            when btrim(fecha_inicio_contrata) = '' then NULL
            else cast(fecha_inicio_contrata  as timestamp with time zone)
        end fecha_inicio_contrata,
        case
            when btrim(a.fuente) = '' then 'desconocido'
            else c.fuente
        end fuente,
        case
            when btrim(motivo_desc) = '' then NULL
            else motivo_desc
        end motivo_descontratacion,
        case
            when btrim(fecha_modificacion) = '' then NULL
            else cast(fecha_modificacion  as timestamp with time zone)
        end fecha_modificacion,
        case
            when btrim(fecha_contacto) = '' then NULL
            else cast(fecha_contacto  as timestamp with time zone)
        end fecha_contacto,
        case
            when btrim(consiguio_entrevista) = '' then 'desconocido'
            when btrim(consiguio_entrevista) = '1' then 'no'
            when btrim(consiguio_entrevista) = '2' then 'si'
        end consiguio_entrevista,
        case
            when btrim(fecha_entrevista) = '' then NULL
            else cast(fecha_entrevista  as timestamp with time zone)
        end fecha_entrevista,
        case
            when btrim(fuiste_contratado) = '' then 'desconocido'
            else cast(fuiste_contratado::numeric::smallint as varchar)
        end fuiste_contratado,
        case
            when btrim(id_oficina_seguimiento) = '' then NULL
            else cast(id_oficina_seguimiento  as smallint)
        end id_oficina_seguimiento,
        case
            when btrim(id_usuario_seguimiento) = '' then NULL
            else cast(id_usuario_seguimiento  as integer)
        end id_usuario_seguimiento,
        case
            when btrim(fuente_seguimiento) = '' then NULL
            when btrim(fuente_seguimiento) = '' then NULL
            else cast(fuente_seguimiento  as smallint)
        end fuente_seguimiento
    from
        raw.oferta_candidato a
    left join
        catalogo_estatus b
    on
        a.estatus = b.id_catalogo_opcion
    left join
        catalogo_fuente c
    on
        a.fuente = c.id_catalogo_opcion
)

select
    id_oferta_empleo,
    id_candidato,
    fecha_alta,
    estatus,
    id_motivo,
    id_oferta_candidato,
    compatibilidad,
    id_oficina,
    id_usuario,
    postulacion_cartera,
    fecha_seguimiento,
    fecha_colocacion,
    fecha_inicio_contrata,
    fuente,
    motivo_descontratacion,
    fecha_modificacion,
    fecha_contacto,
    consiguio_entrevista,
    fecha_entrevista,
    b.fuiste_contratado,
    id_oficina_seguimiento,
    id_usuario_seguimiento,
    fuente_seguimiento
from
    cast_oferta_empleo a
left join
    catalogo_fuiste_contratado b
on
    a.fuiste_contratado = b.id_catalogo_opcion;


--tabla oferta_empleo
insert into clean.oferta_empleo
select
    cast(id_oferta_empleo as integer),
    cast(id_empresa::numeric as integer),
    titulo_oferta,
    cast(id_area_laboral as smallint),
    cast(id_ocupacion as integer),
    funciones,
    cast(id_tipo_empleo as smallint),
    dias_laborales,
    cast(hora_entrada as smallint),
    cast(hora_salida as smallint),
    cast(rolar_turno as smallint),
    empresa_ofrece,
    cast(salario as numeric(12,4)),
    cast(id_tipo_contrato as integer),
    cast(id_jerarquia as smallint),
    cast(numero_plazas as integer),
    cast(limite_postulantes_plaza as integer),
    cast(id_discapacidad as smallint),
    cast(id_causa_vacante as integer),
    cast(fecha_inicio as timestamp with time zone),
    cast(fecha_fin as timestamp with time zone),
    case
        when disponibilidad_viajar = ''
            then NULL
        else cast(disponibilidad_viajar as smallint)
    end,
    case
        when disponibilidad_radicar = ''
            then NULL
        else cast(disponibilidad_radicar as smallint)
    end,
    case
        when id_nivel_estudio = ''
            then NULL
        else cast(id_nivel_estudio as smallint)
    end,
    case
        when id_situacion_academica = ''
            then NULL
        else cast(id_situacion_academica as smallint)
    end,
    habilidad_general,
    experiencia_anios,
    case
        when edad_requisito = ''
            then NULL
        else cast(edad_requisito as smallint)
    end,
    case
        when edad_minima = ''
            then NULL
        else cast(edad_minima as smallint)
    end,
    case
        when edad_maxima = ''
            then NULL
        else cast(edad_maxima as smallint)
    end,
    case
        when genero = ''
            then NULL
        else cast(genero as smallint)
    end,
    observaciones,
    case
        when id_duracion_aproximada = ''
            then NULL
        else cast(id_duracion_aproximada as smallint)
    end,
    cast(fuente as smallint),
    cast(fecha_alta as timestamp with time zone),
    case
        when contacto_tel = ''
            then NULL
        else cast(contacto_tel as smallint)
    end,
    case
        when contacto_correo = ''
            then NULL
        else cast(contacto_correo as smallint)
    end,
    cast(estatus as smallint),
    cast(fecha_modificacion as timestamp with time zone),
    nombre_empresa,
    case
        when id_actividad_economica = ''
            then NULL
        else cast(id_actividad_economica as smallint)
    end,
    cargo_contacto,
    case
        when contacto_domicilio = ''
            then NULL
        else cast(contacto_domicilio as smallint)
    end,
    case
        when id_vigencia_oferta = ''
            then NULL
        else cast(id_vigencia_oferta as smallint)
    end,
    cast(id_oficina_registro as integer),
    cast(id_usuario as integer),
    cast(plazas_cubiertas as smallint),
    case
        when oferta_capacitacion = ''
            then NULL
        else cast(oferta_capacitacion as smallint)
    end,
    case
        when id_oficina_capacitacion = ''
            then NULL
        else cast(id_oficina_capacitacion as smallint)
    end,
    cast(publicar_oferta as smallint),
    discapacidades,
    case
        when plazas_disponibles = ''
            then NULL
        else cast(plazas_disponibles as smallint)
    end,
    case
        when pertenece_pet = ''
            then NULL
        else cast(pertenece_pet as smallint)
    end,
    case
        when plazas_canceladas = ''
            then NULL
    else cast(plazas_canceladas as smallint)
    end,
    case
        when tipo_oferta = ''
            then NULL
        else cast(tipo_oferta as smallint)
    end,
    case
        when tipo_oferta_modalidad = ''
            then NULL
        else cast(tipo_oferta_modalidad as smallint)
    end,
    case
        when id_subarea = ''
            then NULL
        else cast(id_subarea as smallint)
    end,
    rolar_dias_laborales,
    case
        when id_area = ''
            then NULL
        else cast(id_area as smallint)
    end
from raw.oferta_empleo;

--tabla candidato_evento
insert into clean.candidato_evento
select
     cast(id_candidato as integer),
     cast(id_evneto as integer) as id_evento,
     cast(folio_registro as integer),
     cast(fecha_registro as timestamp with time zone),
     cast(fuente as smallint),
     cast(id_usuario as integer)
from raw.candidato_evento;

--tabla catalogo_subarea
insert into clean.catalogo_subarea
select
     cast(id_subarea as smallint),
     cast(descripcion as varchar),
     cast(id_area as smallint),
     cast(num_cat_area as smallint)
from raw.catalogo_subarea;


--tabla candidato_idioma
insert into clean.candidato_idioma
select
    cast(id_candidato_idioma as integer),
    cast(id_candidato as integer),
    cast(id_idioma as smallint),
    cast(id_certificacion as smallint),
    cast(id_dominio as smallint),
    cast(fecha_alta as timestamp with time zone),
    cast(principal as smallint) as registro_principal,
    cast(id_dominio_habla as smallint),
    cast(id_dominio_escrito as smallint),
    cast(id_dominio_lectura as smallint),
    cast(examen_id as smallint),
    cast(examen_puntos as bigint)
from raw.candidato_idioma;


--tabla oficina
insert into clean.oficina
select
    cast(id_oficina as integer),

    case
        when clave_oficina = ''
            then NULL
        else clave_oficina
    end,

    case
        when descripcion = ''
            then NULL
        else descripcion
    end,

    cast(estatus as smallint),

    cast(fecha_alta as timestamp with time zone),

    case
        when fecha_modificacion = ''
            then NULL
        else cast(fecha_modificacion as timestamp with time zone)
    end,

    case
        when id_entidad = ''
            then NULL
        else cast(id_entidad as smallint)
    end,

    case
        when id_municipio = ''
            then NULL
        else cast(id_municipio as integer)
    end,

    unioperativa_oficina,-- varchar,

    case
        when id_motivo_desactivacion = ''
            then NULL
        else cast(id_motivo_desactivacion as smallint)
    end,

    case
        when motivo_desactivacion = ''
            then NULL
        else motivo_desactivacion
    end,

    case
        when tipo_agenda = ''
            then NULL
        else cast(tipo_agenda::numeric as smallint)
    end,

    case
        when url_localizacion = ''
            then NULL
        else url_localizacion
    end,

    case
        when oficina_reconocida = ''
            then NULL
        else cast(oficina_reconocida as smallint)
    end
from raw.oficina;


--tabla candidato_otro_lenguaje
insert into clean.candidato_otro_lenguaje
select
     cast(id_candidato_otro_lenguaje as integer),
     cast(id_candidato as integer),
     cast(id_lenguaje as smallint)
from raw.candidato_otro_lenguaje;


--tabla patrones_solicitantes_svl
insert into clean.patrones_solicitantes_svl
select
    cve_patron as clave_patron,
    razon as razon_social,
    cast(cv_modalidad as smallint) as modalidad_contrato,
    case
      when ta like '%,%'
        then cast(replace(ta, ',', '') as integer)
      else cast(ta as integer)
    end total_trabajadores,
    cast(tp as integer) as total_trabajadores_permanentes,
    cast(teu as smallint) as total_trabajadores_eventuales,
    cast(tec as smallint) as total_trabajadores_campo,
    cast(div_final as smallint) as division_economica,
    cast(gpo_final as smallint) as grupo_actividad_economica,
    cast(act_final as smallint) as rama_actividad_economica,
    cast(clase as smallint) as clase_actividad,
    cast(cve_ent_final as smallint) as clave_entidad_federativa,
    cve_mun_final as clave_municipio,
    localidad,
    cast(cod_pos as integer) as codigo_postal,
    giro
from raw.patrones_solicitantes_svl;


--tabla catalogo_area
insert into clean.catalogo_area
select
     cast(id_area as smallint),
     descripcion
from raw.catalogo_area;

--tabla solicitantes
insert into clean.solicitantes
select
     cast(id_solicitante::numeric as integer),
     case
        when id_candidato = ''
            then NULL
        else cast(id_candidato::numeric as integer)
     end,
     case
        when id_empresa = ''
            then NULL
        else cast(id_empresa::numeric as integer)
     end,
     cast(fecha_registro as timestamp with time zone),
     cast(id_oficina::numeric as integer),
     case
        when id_fuente = ''
            then NULL
        else cast(id_fuente::numeric as integer)
     end,
     cast(genero as smallint),
     case
        when fecha_nacimiento = ''
            then NULL
        else cast(fecha_nacimiento as timestamp with time zone)
     end,
     cast(edad as smallint),
     cast(id_entidad_nacimiento as smallint),
     case
        when fecha_ultima_modificacion = ''
            then NULL
        else cast(fecha_ultima_modificacion as timestamp with time zone)
     end,
     case
        when persona_ultima_modificacion = ''
            then NULL
        else cast(persona_ultima_modificacion::numeric as integer)
     end,
     case
        when estatus = ''
            then NULL
        else cast(estatus as smallint)
     end,
     cast(validado_renapo as smallint),
     case
        when id_motivo_no_validacion = ''
            then NULL
        else cast(id_motivo_no_validacion::numeric as integer)
     end,
     curp_anonima
from raw.solicitantes;


-- tabla oferta_candidato
insert into clean.oferta_candidato
select
    cast(id_oferta_empleo as integer), --no nulo
    cast(id_candidato as integer), --no nulo
    cast(fecha_alta as timestamp with time zone), --no nulo
    cast(estatus as smallint), --no nulo

    case
        when btrim(id_motivo) = '' then NULL
        else cast(id_motivo  as smallint)
    end id_motivo,

    cast(id_oferta_candidato as integer),  --no nulo

    case
        when btrim(compatibilidad) = '' then NULL
        else cast(compatibilidad  as smallint)
    end compatibilidad,

    case
        when btrim(id_oficina) = '' then NULL
        else cast(id_oficina  as smallint)
    end id_oficina,

    cast(id_usuario as integer), --no nulo

    case
        when btrim(postulacion_cartera) = '' then NULL
        else cast(postulacion_cartera  as smallint)
    end postulacion_cartera,
     case
        when btrim(fecha_seguimiento) = '' then NULL
        else cast(fecha_seguimiento  as timestamp with time zone)
    end fecha_seguimiento,

    case
        when btrim(fecha_colocacion) = '' then NULL
        else cast(fecha_colocacion  as timestamp with time zone)
    end fecha_colocacion,

    case
        when btrim(fecha_inicio_contrata) = '' then NULL
        else cast(fecha_inicio_contrata  as timestamp with time zone)
    end fecha_inicio_contrata,

    case
        when btrim(fuente) = '' then NULL
        else cast(fuente  as smallint)
    end fuente,

    case
        when btrim(motivo_desc) = '' then NULL
        else motivo_desc
    end motivo_desc,

    case
        when btrim(fecha_modificacion) = '' then NULL
        else cast(fecha_modificacion  as timestamp with time zone)
    end fecha_modificacion,

    case
        when btrim(fecha_contacto) = '' then NULL
        else cast(fecha_contacto  as timestamp with time zone)
    end fecha_contacto,

    case
        when btrim(consiguio_entrevista) = '' then NULL
        else cast(consiguio_entrevista::numeric  as smallint)
    end consiguio_entrevista,

    case
        when btrim(fecha_entrevista) = '' then NULL
        else cast(fecha_entrevista  as timestamp with time zone)
    end fecha_entrevista,

    case
        when btrim(fuiste_contratado) = '' then NULL
        else cast(fuiste_contratado::numeric  as smallint)
    end fuiste_contratado,

    case
        when btrim(id_oficina_seguimiento) = '' then NULL
        else cast(id_oficina_seguimiento  as smallint)
    end id_oficina_seguimiento,

    case
        when btrim(id_usuario_seguimiento) = '' then NULL
        else cast(id_usuario_seguimiento  as integer)
    end id_usuario_seguimiento,

    case
        when btrim(fuente_seguimiento) = '' then NULL
        else cast(fuente_seguimiento  as smallint)
    end fuente_seguimiento

from raw.oferta_candidato;


insert into clean.ground_truth_fuiste_contratado
select
    id_candidato,
    id_oferta_empleo,
    fecha_alta,
    case
       when fuiste_contratado = 'Sí' then 1
       when fuiste_contratado = '' and estatus = 'Contratado' then 1
       when fuiste_contratado is null and estatus = 'Contratado' then 1
       when fuiste_contratado ilike '%en espera%' and estatus = 'Contratado' then 1
       else 0
    end as label
from clean.oferta_candidato;

--tabla candidato_habilidad
insert into clean.candidato_habilidad
with candidato_habilidad as(
  select
    id_candidato,
    id_habilidad
  from
    raw.candidato_habilidad
),

catalogo_habilidad as(
  select
    id_catalogo_opcion as id_habilidad,
    opcion as habilidad
  from
    raw.catalogo_opciones
  where
    id_catalogo = '66'
)

select
  cast(id_candidato as integer),
  cast(id_habilidad as smallint),
  habilidad
from
  candidato_habilidad
left join
  catalogo_habilidad
using(id_habilidad);


