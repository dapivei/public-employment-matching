set role sne_readwrite;

DROP SCHEMA IF EXISTS groups cascade;

CREATE SCHEMA groups;

--############ Titulos
--table titulos_ofertas
DROP TABLE IF EXISTS groups.titulos_ofertas;

CREATE TABLE groups.titulos_ofertas(
    id_oferta_empleo integer,
    grupo smallint
);

CREATE INDEX idx_grps_to_id_oferta_empleo on groups.titulos_ofertas(id_oferta_empleo);

--table titulos_ofertas fasttext lemma
DROP TABLE IF EXISTS groups.titulos_ofertas_fasttext_lemmatization;

CREATE TABLE groups.titulos_ofertas_fasttext_lemmatization(
    id_oferta_empleo integer,
    grupo smallint,
    date_assigned timestamp without time zone
);

CREATE INDEX idx_grps_to_id_fasttxt_lemma_oferta_empleo on groups.titulos_ofertas_fasttext_lemmatization
(id_oferta_empleo);

--table titulos_ofertas fasttext stem
DROP TABLE IF EXISTS groups.titulos_ofertas_fasttext_stemming;

CREATE TABLE groups.titulos_ofertas_fasttext_stemming(
    id_oferta_empleo integer,
    grupo smallint,
    date_assigned timestamp without time zone
);

CREATE INDEX idx_grps_to_fasttxt_stem_id_oferta_empleo on groups.titulos_ofertas_fasttext_stemming(id_oferta_empleo);

--table titulos_ofertas word2vec lemma
DROP TABLE IF EXISTS groups.titulos_ofertas_word2vec_lemmatization;

CREATE TABLE groups.titulos_ofertas_word2vec_lemmatization(
    id_oferta_empleo integer,
    grupo smallint,
    date_assigned timestamp without time zone
);

CREATE INDEX idx_grps_to_w2v_lemma_id_oferta_empleo on groups.titulos_ofertas_word2vec_lemmatization(id_oferta_empleo);

--table titulos_ofertas word2vec stem
DROP TABLE IF EXISTS groups.titulos_ofertas_word2vec_stemming;

CREATE TABLE groups.titulos_ofertas_word2vec_stemming(
    id_oferta_empleo integer,
    grupo smallint,
    date_assigned timestamp without time zone
);

CREATE INDEX idx_grps_to_w2v_stem_id_oferta_empleo on groups.titulos_ofertas_word2vec_stemming(id_oferta_empleo);


--########## Funciones
--table funciones_ofertas
DROP TABLE IF EXISTS groups.funciones_ofertas;

CREATE TABLE groups.funciones_ofertas(
    id_oferta_empleo integer,
    grupo smallint
);

CREATE INDEX idx_grps_of_id_oferta_empleo on groups.funciones_ofertas(id_oferta_empleo);

--table funciones_ofertas fasttext lemma
DROP TABLE IF EXISTS groups.funciones_ofertas_fasttext_lemmatization;

CREATE TABLE groups.funciones_ofertas_fasttext_lemmatization(
    id_oferta_empleo integer,
    grupo smallint,
    date_assigned timestamp without time zone
);

CREATE INDEX idx_grps_of_fasttxt_lemma_id_oferta_empleo on groups.funciones_ofertas_fasttext_lemmatization
(id_oferta_empleo);

--table funciones_ofertas fasttext stem
DROP TABLE IF EXISTS groups.funciones_ofertas_fasttext_stemming;

CREATE TABLE groups.funciones_ofertas_fasttext_stemming(
    id_oferta_empleo integer,
    grupo smallint,
    date_assigned timestamp without time zone
);

CREATE INDEX idx_grps_of_fasttxt_stem_id_oferta_empleo on groups.funciones_ofertas_fasttext_stemming(id_oferta_empleo);

--table funciones_ofertas word2vec lemma
DROP TABLE IF EXISTS groups.funciones_ofertas_word2vec_lemmatization;

CREATE TABLE groups.funciones_ofertas_word2vec_lemmatization(
    id_oferta_empleo integer,
    grupo smallint,
    date_assigned timestamp without time zone
);

CREATE INDEX idx_grps_of_w2v_lemma_id_oferta_empleo on groups.funciones_ofertas_word2vec_lemmatization
(id_oferta_empleo);

--table funciones_ofertas word2vec stem
DROP TABLE IF EXISTS groups.funciones_ofertas_word2vec_stemming;

CREATE TABLE groups.funciones_ofertas_word2vec_stemming(
    id_oferta_empleo integer,
    grupo smallint,
    date_assigned timestamp without time zone
);

CREATE INDEX idx_grps_of_w2v_stem_id_oferta_empleo on groups.funciones_ofertas_word2vec_stemming(id_oferta_empleo);


--####### conocimientos candidatos
--table conocimientos_candidatos
DROP TABLE IF EXISTS groups.conocimientos_candidatos;

CREATE TABLE groups.conocimientos_candidatos(
    id_candidato varchar,
    grupo smallint
);

CREATE INDEX idx_grps_of_id_candidato on groups.conocimientos_candidatos(id_candidato);


--table conocimientos_candidatos fasttext lemma
DROP TABLE IF EXISTS groups.conocimientos_candidatos_fasttext_lemmatization;

CREATE TABLE groups.conocimientos_candidatos_fasttext_lemmatization(
    id_candidato varchar,
    grupo smallint,
    date_assigned timestamp without time zone
);

CREATE INDEX idx_grps_of_fasttxt_lemma_id_candidato on groups.conocimientos_candidatos_fasttext_lemmatization(id_candidato);

--table conocimientos_candidatos word2vec lemma
DROP TABLE IF EXISTS groups.conocimientos_candidatos_word2vec_lemmatization;

CREATE TABLE groups.conocimientos_candidatos_word2vec_lemmatization(
    id_candidato varchar,
    grupo smallint,
    date_assigned timestamp without time zone
);

CREATE INDEX idx_grps_of_w2v_lemma_id_candidato on groups.conocimientos_candidatos_word2vec_lemmatization(id_candidato);


--table conocimientos_candidatos fasttext stemming
DROP TABLE IF EXISTS groups.conocimientos_candidatos_fasttext_stemming;

CREATE TABLE groups.conocimientos_candidatos_fasttext_stemming(
    id_candidato varchar,
    grupo smallint,
    date_assigned timestamp without time zone
);

CREATE INDEX idx_grps_of_fasttxt_stem_id_candidato on groups.conocimientos_candidatos_fasttext_stemming(id_candidato);

--table conocimientos_candidatos word2vec stemming
DROP TABLE IF EXISTS groups.conocimientos_candidatos_word2vec_stemming;

CREATE TABLE groups.conocimientos_candidatos_word2vec_stemming(
    id_candidato varchar,
    grupo smallint,
    date_assigned timestamp without time zone
);

CREATE INDEX idx_grps_of_w2v_stem_id_candidato on groups.conocimientos_candidatos_word2vec_stemming(id_candidato);

--########### vocabularios

--### vocabulario titulo
--table vocabulary_title
DROP TABLE IF EXISTS groups.vocabulary_title;

CREATE TABLE groups.vocabulary_title(
    vocabulary varchar,
    grupo smallint
);

--table vocabulary_title fasttext lemma
DROP TABLE IF EXISTS groups.vocabulary_title_fasttext_lemmatization;

CREATE TABLE groups.vocabulary_title_fasttext_lemmatization(
    vocabulary varchar,
    grupo smallint,
    date_assigned timestamp without time zone
);

--table vocabulary_title fasttext stemming
DROP TABLE IF EXISTS groups.vocabulary_title_fasttext_stemming;

CREATE TABLE groups.vocabulary_title_fasttext_stemming(
    vocabulary varchar,
    grupo smallint,
    date_assigned timestamp without time zone
);

--table vocabulary_title word2vec lemma
DROP TABLE IF EXISTS groups.vocabulary_title_word2vec_lemmatization;

CREATE TABLE groups.vocabulary_title_word2vec_lemmatization(
    vocabulary varchar,
    grupo smallint,
    date_assigned timestamp without time zone
);

--table vocabulary_title word2vec stemming
DROP TABLE IF EXISTS groups.vocabulary_title_word2vec_stemming;

CREATE TABLE groups.vocabulary_title_word2vec_stemming(
    vocabulary varchar,
    grupo smallint,
    date_assigned timestamp without time zone
);


--### vocabulario conocimientos
--table vocabulary_knowledge
DROP TABLE IF EXISTS groups.vocabulary_knowledge;

CREATE TABLE groups.vocabulary_knowledge(
    vocabulary varchar,
    grupo smallint
);

--table vocabulary_knowledge fasttext lemma
DROP TABLE IF EXISTS groups.vocabulary_knowledge_fasttext_lemmatization;

CREATE TABLE groups.vocabulary_knowledge_fasttext_lemmatization(
    vocabulary varchar,
    grupo smallint,
    date_assigned timestamp without time zone
);

--table vocabulary_knowledge fasttext stemming
DROP TABLE IF EXISTS groups.vocabulary_knowledge_fasttext_stemming;

CREATE TABLE groups.vocabulary_knowledge_fasttext_stemming(
    vocabulary varchar,
    grupo smallint,
    date_assigned timestamp without time zone
);

--table vocabulary_knowledge word2vec lemma
DROP TABLE IF EXISTS groups.vocabulary_knowledge_word2vec_lemmatization;

CREATE TABLE groups.vocabulary_knowledge_word2vec_lemmatization(
    vocabulary varchar,
    grupo smallint,
    date_assigned timestamp without time zone
);

--table vocabulary_knowledge word2vec stemming
DROP TABLE IF EXISTS groups.vocabulary_knowledge_word2vec_stemming;

CREATE TABLE groups.vocabulary_knowledge_word2vec_stemming(
    vocabulary varchar,
    grupo smallint,
    date_assigned timestamp without time zone
);


--### vocabulario funciones
--table vocabulary functions
DROP TABLE IF EXISTS groups.vocabulary_functions;

CREATE TABLE groups.vocabulary_functions(
    vocabulary varchar,
    grupo smallint
);

--table vocabulary functions fasttext lemma
DROP TABLE IF EXISTS groups.vocabulary_functions_fasttext_lemmatization;

CREATE TABLE groups.vocabulary_functions_fasttext_lemmatization(
    vocabulary varchar,
    grupo smallint,
    date_assigned timestamp without time zone
);

--table vocabulary functions fasttext stemming
DROP TABLE IF EXISTS groups.vocabulary_functions_fasttext_stemming;

CREATE TABLE groups.vocabulary_functions_fasttext_stemming(
    vocabulary varchar,
    grupo smallint,
    date_assigned timestamp without time zone
);

--table vocabulary functions word2vec lemma
DROP TABLE IF EXISTS groups.vocabulary_functions_word2vec_lemmatization;

CREATE TABLE groups.vocabulary_functions_word2vec_lemmatization(
    vocabulary varchar,
    grupo smallint,
    date_assigned timestamp without time zone
);

--table vocabulary functions word2vec stemming
DROP TABLE IF EXISTS groups.vocabulary_functions_word2vec_stemming;

CREATE TABLE groups.vocabulary_functions_word2vec_stemming(
    vocabulary varchar,
    grupo smallint,
    date_assigned timestamp without time zone
);



--################### Catalogos

--### catalogo funciones
--catalogue groups names functions
DROP TABLE IF EXISTS groups.catalogue_group_name_functions;

CREATE TABLE groups.catalogue_group_name_functions(
   grupo smallint,
   group_name varchar
);

--catalogue groups names knowledge
DROP TABLE IF EXISTS groups.catalogue_group_name_knowledge;

CREATE TABLE groups.catalogue_group_name_knowledge(
   grupo smallint,
   group_name varchar
);

--catalogue groups names titles
DROP TABLE IF EXISTS groups.catalogue_group_name_titles;

CREATE TABLE groups.catalogue_group_name_titles(
  grupo smallint,
  group_name varchar
);

--catalogue groups names functions fasttext lemma
DROP TABLE IF EXISTS groups.catalogue_group_name_functions_fasttext_lemmatization;

CREATE TABLE groups.catalogue_group_name_functions_fasttext_lemmatization(
   grupo smallint,
   group_name varchar
);

--catalogue groups names knowledge fasttext lemma
DROP TABLE IF EXISTS groups.catalogue_group_name_knowledge_fasttext_lemmatization;

CREATE TABLE groups.catalogue_group_name_knowledge_fasttext_lemmatization(
   grupo smallint,
   group_name varchar
);

--catalogue groups names titles fasttext lemma
DROP TABLE IF EXISTS groups.catalogue_group_name_titles_fasttext_lemmatization;

CREATE TABLE groups.catalogue_group_name_titles_fasttext_lemmatization(
   grupo smallint,
   group_name varchar
);

--catalogue groups names titles fasttext stemming
DROP TABLE IF EXISTS groups.catalogue_group_name_title_fasttext_stemming;

CREATE TABLE groups.catalogue_group_name_title_fasttext_stemming(
   grupo smallint,
   group_name varchar
);
