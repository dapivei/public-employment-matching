insert into semantic.eventos_candidatos (id_candidato, tipo, fecha, atributos)
   with

   eventos_postulacion as (
      select
         id_candidato,
         'postular oferta'::semantic.evento_candidato,
         fecha_alta,
         jsonb_build_object(
            'id_oferta_empleo', id_oferta_empleo,
            'estatus', estatus,
            'fuiste_contratado',
            case
                 when fuiste_contratado = 'Sí' then 1
                 when fuiste_contratado = '' and estatus = 'Contratado' then 1
                 when fuiste_contratado is null and estatus = 'Contratado' then 1
                 when fuiste_contratado ilike '%en espera%' and estatus = 'Contratado' then 1
                 else 0
            end
         )::jsonb  as atributos
      from clean.oferta_candidato
   ),

   eventos_inicio_sesion as (
      select
         distinct on (id_bt)
         id_bt::int,
         'ingresar al sistema'::semantic.evento_candidato,
         fecha_ultimo_acceso,
         jsonb_build_object(
            'id_tabla', id_tabla,
            'fecha_control', fecha_control
         )::jsonb  as atributos
      from stps.inicio_sesion_bt
      where id_bt is not null
   ),

   eventos_contratacion as (
      select
         id_candidato,
         'obtener contratacion'::semantic.evento_candidato,
         fecha_alta + interval '2 months' as fecha_colocacion,
         jsonb_build_object(
            'id_oferta_empleo', id_oferta_empleo,
            'estatus', estatus,
            'fuiste_contratado',
            case
                 when fuiste_contratado = 'Sí' then 1
                 when fuiste_contratado = '' and estatus = 'Contratado' then 1
                 when fuiste_contratado is null and estatus = 'Contratado' then 1
                 when fuiste_contratado ilike '%en espera%' and estatus = 'Contratado' then 1
                 else 0
            end
         )::jsonb  as atributos
      from clean.oferta_candidato
   ),

   eventos_entrevista as (
      select
         id_candidato,
         'obtener entrevista'::semantic.evento_candidato,
         fecha_entrevista,
         jsonb_build_object(
            'id_oferta_empleo', id_oferta_empleo,
            'estatus', estatus
         )::jsonb  as atributos
      from clean.oferta_candidato
   ),

   eventos_contacto as (
      select
         id_candidato,
         'ser contactado'::semantic.evento_candidato,
         fecha_contacto,
         jsonb_build_object(
            'id_oferta_empleo', id_oferta_empleo,
            'estatus', estatus
         )::jsonb  as atributos
      from clean.oferta_candidato
   ),

   eventos_monitoreo as (
      select
         id_candidato,
         'monitorear postulación'::semantic.evento_candidato,
         fecha_seguimiento,
         jsonb_build_object(
            'id_oferta_empleo', id_oferta_empleo,
            'estatus', estatus
         )::jsonb  as atributos
      from clean.oferta_candidato
   ),

   eventos_iniciar_grado as (
      select
         id_candidato,
         'iniciar grado académico'::semantic.evento_candidato,
         inicio,
         jsonb_build_object(
            'id_carrera_especialidad', id_carrera_especialidad,
            'id_nivel_estudio', id_nivel_estudio,
            'id_situacion_academica', id_situacion_academica,
            'id_candidato_grado_academico', id_candidato_grado_academico
         )::jsonb  as atributos
      from clean.candidato_grado_academico
   ),

   eventos_terminar_grado as (
      select
         id_candidato,
         'terminar grado académico'::semantic.evento_candidato,
         fin,
         jsonb_build_object(
            'id_carrera_especialidad', id_carrera_especialidad,
            'id_nivel_estudio', id_nivel_estudio,
            'id_situacion_academica', id_situacion_academica,
            'id_candidato_grado_academico', id_candidato_grado_academico
         )::jsonb  as atributos
      from clean.candidato_grado_academico
   ),

   eventos_expectativa as (
      select
         id_candidato,
         'actualizar expectativa laboral'::semantic.evento_candidato,
         fecha_alta,
         jsonb_build_object(
            'id_tipo_empleo_deseado', id_tipo_empleo_deseado,
            'id_ocupacion_deseada', id_ocupacion_deseada,
            'id_area_laboral_deseada', id_area_laboral_deseada,
            'id_sub_area_laboral_deseada', id_sub_area_laboral_deseada,
            'id_tipo_contrato', id_tipo_contrato,
            'salario_pretendido', salario_pretendido,
            'id_experiencia', id_experiencia,
            'id_expectativa_laboral', id_expectativa_laboral
         )::jsonb  as atributos
      from clean.candidato_expectativa_laboral
   ),

   eventos_grado_academico as (
      select
         id_candidato,
         'actualizar grado académico'::semantic.evento_candidato,
         fecha_alta,
         jsonb_build_object(
            'id_carrera_especialidad', id_carrera_especialidad,
            'id_nivel_estudio', id_nivel_estudio,
            'año_inicio_grado_academico',
            case
               when inicio = 0 then null else inicio
            end,
            'año_fin_grado_academico',
            case
               when fin = 0 then null
               else fin
            end,
            'id_situacion_academica', id_situacion_academica,
            'id_candidato_grado_academico', id_candidato_grado_academico
         )::jsonb  as atributos
      from clean.candidato_grado_academico
   ),

   eventos_idiomas as (
      select
         id_candidato,
         'actualizar idiomas'::semantic.evento_candidato,
         fecha_alta,
         jsonb_build_object(
            'id_certificacion', id_certificacion,
            'id_dominio', id_dominio,
            'id_idioma', id_idioma,
            'id_candidato_idioma', id_candidato_idioma
         )::jsonb  as atributos
      from clean.candidato_idioma
   )

   select
      id_candidato::int,
      evento_candidato as tipo,
      fecha_alta::date as fecha,
      atributos
   from eventos_idiomas
      where fecha_alta is not NULL
   union
   select
      id_candidato::int,
      evento_candidato as tipo,
      fecha_alta::date as fecha,
      atributos
   from eventos_grado_academico
      where fecha_alta is not NULL
   union
   select
      id_candidato::int,
      evento_candidato as tipo,
      fecha_alta::date as fecha,
      atributos
   from eventos_expectativa
      where fecha_alta is not NULL
   union
   select
      id_candidato::int,
      evento_candidato as tipo,
      fecha_colocacion::date as fecha,
      atributos
   from eventos_contratacion
      where fecha_colocacion is not NULL
   union
   select
      id_candidato::int,
      evento_candidato as tipo,
      make_date(inicio, 01, 01)::date as fecha,
      atributos
   from eventos_iniciar_grado
      where inicio != 0
   union
   select
      id_candidato::int,
      evento_candidato as tipo,
      make_date(fin, 12, 31)::date as fecha,
      atributos
   from eventos_terminar_grado
      where fin != 0
   union
   select
      id_candidato::int,
      evento_candidato as tipo,
      fecha_alta::date as fecha,
      atributos
   from eventos_postulacion
   union
   select
      id_candidato::int,
      evento_candidato as tipo,
      fecha_entrevista::date as fecha,
      atributos
   from eventos_entrevista
      where fecha_entrevista is not null
   union
   select
      id_candidato::int,
      evento_candidato as tipo,
      fecha_contacto::date as fecha,
      atributos
   from eventos_contacto
      where fecha_contacto is not NULL
   union
   select
      id_candidato::int,
      evento_candidato as tipo,
      fecha_seguimiento::date as fecha,
      atributos
   from eventos_monitoreo
      where fecha_seguimiento is not null
   union
   select
      id_bt::int as id_candidato,
      evento_candidato as tipo,
      fecha_ultimo_acceso::date as fecha,
      atributos
   from eventos_inicio_sesion
      where fecha_ultimo_acceso is not null
   order by id_candidato, fecha;

create index eventos_candidatos_evento_candidato_ix on semantic.eventos_candidatos(evento_candidato asc);
create index eventos_candidatos_id_candidato_ix on semantic.eventos_candidatos(id_candidato asc);
create index eventos_candidatos_tipo on semantic.eventos_candidatos(tipo);
comment on table semantic.eventos_candidatos is 'Each row is an event associated to a job seeker';

--tabla semantic.eventos_ofertas
insert into semantic.eventos_ofertas (id_oferta, tipo, fecha, atributos)
   with

   cat_jerarquia as (
      select
         cast(id_catalogo_opcion as smallint) as id_jerarquia,
         opcion as jerarquia
      from
         raw.catalogo_opciones
      where id_catalogo = '22'
   ),

   eventos_alta as (
      select
         id_oferta_empleo as id_oferta,
         'dar alta vacante'::semantic.evento_oferta,
         fecha_alta,
         jsonb_build_object(
            'id_empresa', id_empresa,
            'salario', salario,
            'jerarquia', jerarquia
         )::jsonb  as atributos
      from clean.oferta_empleo
      left join cat_jerarquia
  	     using(id_jerarquia)
    ),

   eventos_publicacion as (
      select
         id_oferta_empleo as id_oferta,
         'publicar vacante'::semantic.evento_oferta,
         fecha_inicio,
         jsonb_build_object(
            'id_empresa', id_empresa,
            'salario', salario,
            'jerarquia', jerarquia
         )::jsonb  as atributos
      from clean.oferta_empleo
      left join cat_jerarquia
  	      using(id_jerarquia)
  	),

   eventos_modificacion as (
      select
         id_oferta_empleo as id_oferta,
         'modificar vacante'::semantic.evento_oferta,
         fecha_modificacion,
         jsonb_build_object(
            'id_empresa', id_empresa,
            'salario', salario,
            'jerarquia', jerarquia
         )::jsonb  as atributos
      from clean.oferta_empleo
      left join cat_jerarquia
  	      using(id_jerarquia)
  	),

   eventos_cierre as (
      select
         id_oferta_empleo as id_oferta,
         'cerrar vacante'::semantic.evento_oferta,
         fecha_fin,
         id_jerarquia,
         jsonb_build_object(
            'id_empresa', id_empresa,
            'salario', salario,
            'jerarquia', jerarquia
         )::jsonb  as atributos
      from clean.oferta_empleo
      left join cat_jerarquia
  	      using(id_jerarquia)
   )

   select distinct
      id_oferta::text,
      evento_oferta::text as tipo,
      fecha_alta ::date as fecha,
      atributos
   from eventos_alta
      where fecha_alta is not null
   union
   select distinct
      id_oferta::text,
      evento_oferta::text as tipo,
      fecha_inicio::date as fecha,
      atributos
   from eventos_publicacion
      where fecha_inicio is not null
   union
   select distinct
      id_oferta::text,
      evento_oferta::text as tipo,
      fecha_modificacion::date as fecha,
      atributos
   from eventos_modificacion
      where fecha_modificacion is not null
   union
   select distinct
      id_oferta::text,
      evento_oferta::text as tipo,
      fecha_fin as fecha,
      atributos
   from eventos_cierre
      where fecha_fin is not null
   order by id_oferta, fecha
   ;

create index eventos_ofertas_evento_oferta_ix on semantic.eventos_ofertas(evento_oferta asc);
create index eventos_ofertas_id_oferta_ix on semantic.eventos_ofertas(id_oferta asc);
comment on table semantic.eventos_ofertas is 'Each row is an event associated to a job offer'

--semantic.eventos_empresas
insert into semantic.eventos_empresas (id_empresa, tipo, fecha, atributos)
with
   eventos_alta as (
      select
         id_empresa,
         'dar alta la empresa'::semantic.evento_empresa,
         fecha_alta,
         jsonb_build_object(
         )::jsonb  as atributos
      from clean.empresas
    ),

   eventos_acta_constitutiva as (
      select
         id_empresa,
         'levantar acta constitutiva'::semantic.evento_empresa,
         fecha_acta,
         jsonb_build_object(
               )::jsonb  as atributos
      from clean.empresas
   ),

   eventos_modificacion as (
      select
         id_empresa,
         'actualizar registro empresa'::semantic.evento_empresa,
         fecha_ultima_actualizacion,
         jsonb_build_object(
         )::jsonb  as atributos
      from clean.empresas
  	),

   eventos_desactivar as (
      select
         id_empresa,
         'desactivar la empresa'::semantic.evento_empresa,
         case
     	      when fecha_ultima_desactivacion='' then null
    	      else fecha_ultima_desactivacion::date
         end,
         jsonb_build_object(
         )::jsonb  as atributos
      from raw.empresa
   )

   select distinct
      id_empresa::text,
      evento_empresa::text as tipo,
      fecha_alta::date as fecha,
      atributos
   from eventos_alta
      where fecha_alta is not null
   union
   select distinct
      id_empresa::text,
      evento_empresa::text as tipo,
      fecha_acta::date as fecha,
      atributos
   from eventos_acta_constitutiva
      where fecha_acta is not null
   union
   select distinct
      id_empresa::text,
      evento_empresa::text as tipo,
      fecha_ultima_actualizacion::date as fecha,
      atributos
   from eventos_modificacion
      where fecha_ultima_actualizacion is not null
   union
   select distinct
      id_empresa::text,
      evento_empresa::text as tipo,
      fecha_ultima_desactivacion as fecha,
      atributos
   from eventos_desactivar
      where fecha_ultima_desactivacion is not null
   order by id_empresa, fecha
   ;
