SET TIME ZONE 'America/Mexico_City';

SET ROLE sne_readwrite;

DROP SCHEMA IF EXISTS  deploy CASCADE;

CREATE SCHEMA deploy;

--tabla mockup_match_api
DROP TABLE IF EXISTS deploy.mockup_match_api;

CREATE TABLE deploy.mockup_match_api(
    id_candidato integer,
    id_oferta integer,
    match_score numeric(5, 2),
    match varchar,
    fecha_prediccion timestamp with time zone
);