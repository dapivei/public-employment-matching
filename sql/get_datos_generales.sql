--contratados y en el imss
with candidatos_2019 as(
  select
    id_candidato
  from
    clean.candidatos
  where date_part('year', fecha_alta) = 2020
  or date_part('year', fecha_ultima_actualizacion) = 2020
),

imss as(
  select
    count(id_candidato)
  from
    clean.linea_tiempo_solicitantes
  where año = 2020
  and evento = '3'
),

contratados as(
  select
    id_candidato
  from
    clean.oferta_candidato
  where fuiste_contratado = 'Sí'
)

select
    count(distinct id_candidato),
    count(id_candidato)
from
    candidatos_2019
join
    imss using(id_candidato)
join
    contratados using(id_candidato);


--candidatos contratados
with candidatos_2019 as(
  select
    distinct id_candidato
  from
    clean.candidatos
  where date_part('year', fecha_alta) = 2020
  or date_part('year', fecha_ultima_actualizacion) = 2020
)

select
    count(distinct(id_candidato)),
    count(id_candidato)
from
    clean.oferta_candidato
join
    candidatos_2019 using(id_candidato)
where fuiste_contratado = 'Sí';


--candidatos en imss
with candidatos_2019 as(
  select
    distinct id_candidato
  from
    clean.candidatos
  where date_part('year', fecha_alta) = 2020
  or date_part('year', fecha_ultima_actualizacion) = 2020
)

  select
    count(distinct(id_candidato)),
    count(id_candidato)
  from
    clean.linea_tiempo_solicitantes
  join
    candidatos_2019 using(id_candidato)
  where año = 2020
  and evento = '3' ;


  --candidatos 2020
select
    count(*)
from
    clean.candidatos
where date_part('year', fecha_alta) = 2020
or date_part('year', fecha_ultima_actualizacion) = 2020;
