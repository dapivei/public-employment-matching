from flask import Flask
from flask import jsonify
from flask_sqlalchemy import SQLAlchemy
from flask_restplus import Api, Resource, fields

from src.utils.general import get_db_conn_sql_alchemy
from src.pipeline.title_search import search_group_title
from src.pipeline.knowledge_search import search_group_knowledge
from src.pipeline.assignment_candidate_jobs_by_endpoint import populate_stps_offers_for_candidate
from src.pipeline.assignment_company_candidates_by_endpoint import populate_stps_candidates_for_offer

# create Flask app
app = Flask(__name__)
api = Api(app)


# endpoints
@api.route('/')
class HelloWorld(Resource):
    def get(self):
        return {'Hello': 'Hello World!'}


@api.route('/search/title/<keywords>')
class JobOffersByTitle(Resource):
    def get(self, keywords):
        top_20 = search_group_title(keywords)

        return jsonify(top_20)


@api.route('/search/knowledge/<int:id_candidato>')
class JobOffersByKnowledge(Resource):
    def get(self, id_candidato):
        top_20 = search_group_knowledge(id_candidato)

        return jsonify(top_20)


@api.route('/search/employment_fair/knowledge/<int:id_candidato>')
class JobOffersByKnowledgeEmploymentFair(Resource):
    def get(self, id_candidato):
        ok = populate_stps_offers_for_candidate(id_candidato)

        return jsonify(ok)


@api.route('/search/employment_fair/company/<int:id_oferta>')
class JobOffersByFunctionEmploymentFair(Resource):
    def get(self, id_oferta):
        ok = populate_stps_candidates_for_offer(id_oferta)

        return jsonify(ok)


if __name__ == '__main__':
    app.run(debug=True)
