#!/bin/bash

aws s3 cp s3://sne-match/ --recursive

# ziped files
for element in *.zip
do
  #echo "$element.txt"
  unzip $element
  aws s3 cp "{$element%%.*}.txt" s3://sne-match/raw/
  rm "{$element%%.*}.txt"
  rm $element
done