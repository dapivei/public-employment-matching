#!/bin/bash

aws s3 cp s3://sne-match/EMPRESA.txt .

iconv -f ISO-8859-1 -t UTF-8//TRANSLIT EMPRESA.txt -o empresa_utf8.txt

# empresa tiene 52 columnas, la columna 2 es la que tiene el problema de id_portal_empleo como si fuera una razón
# social.
awk -F'|' '{if (length($2) > 1) print NR, $0}' empresa_utf8.txt > empresa_to_fix.txt

zip empresa_to_fix.zip empresa_to_fix.txt

aws s3 cp empresa_to_fix.zip s3://sne-match/verify/