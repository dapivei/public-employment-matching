#!/bin/bash

aws s3 cp s3://sne-match/raw . --recursive

for element in *.txt
# change to utf8
iconv -f ISO-8859-1 -t UTF-8//TRANSLIT $element -o ${element/\.txt/_utf8\.txt}

do
  table_name=$(echo ${element_utf8%%.} | tr '[:upper:]' '[:lower:]')
  psql -c "\copy raw.${element} FROM $element WITH CSV HEADER DELIMITER '|';" service=$1  && rm $element
done