import csv
import logging
import pickle

from psycopg2 import DatabaseError
from psycopg2.extras import execute_batch

from src.utils.general import get_db_conn
from src.utils import constants


def _insert_into_db(data, table, fields, num_fields):
    """
    Insert rows into their respective table
    :param data:
    :param table:
    :param fields:
    :param num_fields:
    :return:
    """
    db_conn = get_db_conn("../conf/local/credentials.yaml")
    cursor = db_conn.cursor()

    rows = [(tuple(element.split('|'))) for element in data]

    q = "insert into raw.{} {} values ({});".format(table, fields, ', '.join(['%s'] * num_fields))

    try:
        logging.info("writing to db {}".format(table))
        execute_batch(cursor, q, rows)
        db_conn.commit()
    except (Exception, DatabaseError) as error:
        logging.error(error)


def fix_oferta_candidato(content, table):
    """
    Fixing data for table oferta_candidato
    :param content:
    :param table:
    :return:
    """
    data = []
    i = 0

    while i < len(content) - 2:
        line = []
        if len(content[i].split('|')) == 15:
            line.append(content[i].strip())
            i += 1
        while len(content[i].split('|')) != 15:
            line.append(content[i].strip())
            i += 1
        complete_line = " ".join(line)
        data.append(complete_line)

    num_fields = len(constants.OFERTA_CANDIDATO_COLS.split(','))
    _insert_into_db(data, table, constants.OFERTA_CANDIDATO_COLS, num_fields)


def fix_oficina(content, table):
    """
    Fixing data for table oficina
    :param content:
    :param table:
    :return:
    """
    data = []
    for i in range(0, len(content), 2):
        first_part = content[i].strip()
        second_part = content[i + 1].strip()
        correct = " ".join([first_part, second_part])
        data.append(correct)

    num_fields = len(constants.OFICINA_COLS.split(','))
    _insert_into_db(data, table, constants.OFICINA_COLS, num_fields)


def fix_table(content, table, column_names, correct_fields):
    """
    Function to make observations by joining return lines into the same field, and quoting each field
    :param content: observations to fix
    :param table: name of the table in the data base -without schema-
    :param column_names: List with column names
    :param correct_fields: number of correct columns to have
    :return:
    """
    data = []
    i = 0
    to_fix = []

    while i < len(content):
        # correct length
        element = content[i]
        if len(element.split('|')) == correct_fields:
            # poner en quotes
            #quoted_data = ['"' + str(elem) + '"' for elem in element.split('|')]
            #data.append("|".join(quoted_data))
            data.append(element)
            i += 1
        else:
            element = content[i]
            if len(element.split('|')) < correct_fields:
                fields = 0
                record = []
                while fields < correct_fields:
                    element = content[i]
                    record.append(element)
                    fields += len(element.split('|'))
                    i += 1
                line = " ".join(record)
                #quoted_data = ['"' + str(elem) + '"' for elem in line.split('|')]
                data.append("|".join(line))
            else:
                to_fix.append(element)
                i += 1

    data_ok = []
    for element in data:
        if len(element.split('|')) != correct_fields:
            to_fix.append(element)
        else:
            data_ok.append(element)

    file_name = "./to_fix_" + table + ".pkl"
    pickle.dump(to_fix, open(file_name, 'wb'))

    num_fields = len(column_names.split(","))
    for i in range(0, len(content), 1000):
        _insert_into_db(data_ok[i:i+1000], table, column_names, num_fields)

    #fix_table_bugs(table, correct_fields + 1, column_names, file_name)


def fix_table_bugs(table, possible_fields, column_names, file_name):
    """
    Try to fix minor bugs from tables with format problems
    :param table: Name of the table in the db -without schema-
    :param possible_fields: Correct fields plus 1, we eliminate the last occurrence
    :param column_names: List with column names
    :param file_name: Name of the file with the pickle to fix
    :return:
    """
    content = pickle.load(open(file_name, 'rb'))
    errors = []
    i = 0
    data = []

    while i < len(content):
        element = content[i]
        if len(element.split('|')) == possible_fields:
            line = element.split('|')[:-1]
            #quoted_data = ['"' + str(elem) + '"' for elem in line]
            data.append("|".join(line))
            i += 1
        else:
            errors.append(element)
            i += 1

    with open("./errors" + str(table) + ".csv", "w", newline="") as f:
        w = csv.writer(f, quoting=csv.QUOTE_ALL)
        w.writerow(errors)

    # insert into db
    num_fields = len(column_names.split(","))
    _insert_into_db(data, table, column_names, num_fields)


def read_data(file):
    """
    Read file to fix
    :param file:
    :return:
    """
    with open(file, 'r') as f:
        content = f.readlines()
    f.close()

    return content


def fix_data(file, table):
    """
    Calls the method to fix the respective table
    :param file: Path of file to fix
    :param table: Name of the table in DB
    :return:
    """
    content = read_data(file)
    if table == "oficina":
        fix_oficina(content, table)
    elif table == "oferta_candidato":
        fix_oferta_candidato(content, table)
    elif table == "empresa":
        fix_table(content, table, constants.EMPRESA_COLS, len(constants.EMPRESA_COLS.split(',')))
    elif table == "oferta_empleo":
        fix_table(content, "oferta_empleo_2", constants.OFERTA_EMPLEO_COLS, len(constants.OFERTA_EMPLEO_COLS.split(','
                                                                                                                   '')))
    elif table == "candidato_conocimiento_habilidad":
        fix_table(content, table, constants.CANDIDATO_CONOCIMIENTO_HABILIDAD_COLS,
                  len(constants.CANDIDATO_CONOCIMIENTO_HABILIDAD_COLS.split(',')))
    elif table == "candidato_otro_estudio":
        fix_table(content, table, constants.CANDIDATO_OTRO_ESTUDIO, len(constants.CANDIDATO_OTRO_ESTUDIO.split(',')))


# TODO grab files to fix from s3 bucket
def get_file_from_s3(file):
    pass


# fix_data("/home/silil/Documents/itam/ccdatos/SNE/sne_remoto/oficina_to_fix.txt", "oficina")
# fix_data("/home/silil/Documents/itam/ccdatos/SNE/sne_remoto/oferta_candidato_to_fix.txt", "oferta_candidato")
# fix_data("/home/silil/Documents/itam/ccdatos/SNE/sne_remoto/oferta_empleo_to_fix.txt", "oferta_empleo")
fix_data("/home/silil/Documents/itam/ccdatos/SNE/sne_remoto/empresa_to_fix.txt", "empresa")
# fix_data("/home/silil/Documents/itam/ccdatos/SNE/sne_remoto/candidato_conocimiento_habilidad_to_fix.txt",
#         "candidato_conocimiento_habilidad")
# fix_data("/home/silil/Documents/itam/ccdatos/SNE/sne_remoto/candidato_conocimiento_habilidad_ok.txt",
#          "candidato_conocimiento_habilidad")
#fix_data("/home/silil/Documents/itam/ccdatos/SNE/sne_remoto/candidato_otro_estudio_utf8.txt", "candidato_otro_estudio")
#fix_data("/home/silil/Documents/itam/ccdatos/SNE/sne_remoto/empresa_utf8.txt", "empresa")
#fix_data("/home/silil/Documents/itam/ccdatos/SNE/sne_remoto/oferta_empleo_utf8.txt", "oferta_empleo")