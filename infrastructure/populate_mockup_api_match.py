import numpy as np
import logging
from datetime import datetime

from psycopg2 import DatabaseError
from psycopg2.extras import execute_batch

from src.utils.general import get_db_conn
from src.utils.constants import NUMPY_SEED, MOCKUP_MATCH_TABLE


def get_candidatos():
    """
    Get list of unique id candidates from candidatos table
    :return:
    """
    db_conn = get_db_conn("../conf/local/credentials.yaml")

    q = """
        select 
            id_candidato,
            id_oferta_empleo
        from
            clean.oferta_candidato
    """

    try:
        cursor = db_conn.cursor()
        cursor.itersize = 10000
        try:
            cursor.execute(q)
            candidatos_ofertas = cursor.fetchall()
            match_scores = generate_random_match_score(len(candidatos_ofertas))
            match_labels = generate_random_match_labels(match_scores)
            mockup_data = [
                (candidato[0], candidato[1], match_scores[i], match_labels[i], datetime.today().strftime('%Y-%m-%d'))
                for i, candidato in enumerate(candidatos_ofertas)]

            insert_mockup_data(mockup_data)

        except (Exception, DatabaseError) as error:
            logging.error(error)
    except (Exception, DatabaseError) as error:
        logging.error(error)

    return candidatos_ofertas


def generate_random_match_score(size):
    """
    Generates mockup match score
    :param size: Number of unique candidates
    :return: List of random match scores
    """
    match_score = np.random.random(size)

    return match_score


def generate_random_match_labels(scores):
    """
    Generates mockup match labes using a specific threshold
    :param scores: List of match scores
    :return: List of binary match labels
    """
    match_labels = ['0' if element < 0.8 else '1' for element in scores]

    return match_labels


def insert_mockup_data(mockup_data):
    db_conn = get_db_conn("../conf/local/credentials.yaml")
    cursor = db_conn.cursor()

    q = "insert into deploy.{} (id_candidato, id_oferta, match_score, match, fecha_prediccion) values ({});".format(
        MOCKUP_MATCH_TABLE, ", ".join(['%s'] * 5))

    try:
        #for i in range(0, len(mockup_data), 5000):
        execute_batch(cursor, q, mockup_data)
        db_conn.commit()
    except (Exception, DatabaseError) as error:
        logging.error(error)


def populate_mockup_match_table():
    """

    :return:
    """
    np.random.seed(NUMPY_SEED)

    get_candidatos()
    '''
    match_scores = generate_random_match_score(len(candidatos_ofertas))
    match_labels = generate_random_match_labels(match_scores)
    mockup_data = [(candidato[0], candidato[1], match_scores[i], match_labels[i], datetime.today().strftime('%Y-%m-%d'))
                   for i, candidato in enumerate(candidatos_ofertas)]

    db_conn = get_db_conn("../conf/local/credentials.yaml")
    cursor = db_conn.cursor()

    q = "insert into deploy.{} (id_candidato, id_oferta, match_score, match, fecha_prediccion) values ({});".format(
        MOCKUP_MATCH_TABLE, ", ".join(['%s'] * 5))

    try:
        for i in range(0, len(mockup_data), 5000):
            execute_batch(cursor, q, mockup_data[i: i+5000])
            db_conn.commit()
    except (Exception, DatabaseError) as error:
        logging.error(error)
    '''

if __name__ == '__main__':
    populate_mockup_match_table()