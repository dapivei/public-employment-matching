# SKILLS2CHAMBA
***
<div align="justify">

Data and algorithm-driven decisions are becoming increasingly prevalent in the public policy arena.
Public employment services represent a potential use case where algorithmic decisions can be of notorious utility in facilitating skills matching. This work exploits machine learning techniques to upgrade Mexico's employment matching engine, from a plain ruled-based appliance to **SKILLS2CHAMBA** ––a job recommendation system trained on a large Spanish corpus that provides each job seeker with a selection of semantically suitable job offerings. Concretely, it first draws on *Word2Vec*, and *FastText* to learn semantically meaningful vocabulary: numerical contextual vector representations for the words listed in over one million job
postings. Then, it relies on the $k$-means algorithm to partition the vocabulary into clusters of semantically similar words. Finally, it deploys a pipeline to provide job seekers with a daily selection of available and semantically suitable job offerings. Overall, this work exemplifies how to formulate, implement, and deploy data-driven decisions to support public labor intermediation systems, despite poor-quality data.

<div align="center">

![alt text](<./images/algoritmos_sne_modulo1.drawio.png>) 

##### Previous Portal versus Current Portal
![alt text](<./images/comparison_before_after.drawio.png>) 

</div>
This job recommendation system forms part of Mexico's Public Employment System, available at [*Portal del Empleo*](https://www.empleo.gob.mx/PortalDigital).



![alt text](<./images/sne_portal.png>) 


### Sites content:



    .

    ├── conf                       
    ├── docs.                      
    |   └..
    ├── images                    
    ├── infraestructure                      
    ├── sql                  
    ├── src
    |   ├── etl                    
    |   ├── pipeline              
    |   ├── utils.                
    ├── README.md                    
    └── requirements                

</div>
